var dataskumanager;
$(document).one("ready", function(){
	dataskumanager = new DataSkuManager(".topic.Variacao");
});

function DataSkuManager(selectorGroup)
{
	var selector = selectorGroup;
	var _owner = this;
	var pathDataSku = "/produto/sku/";
	this.objSkusInfo = {skuList: []};

	// Inicia
	initManager();

	function initManager()
	{
		var textMl = $(".selectSku label").text();
		var textCopoBanho = $(".bread-crumb ul li:first-child + li a").html();

		// CASO TENHA "ml" NO SKU ELE REMOVE A CLASSE QUE FAZ O QUADRADINHO DE COR
		if(textMl.match(/ml/) || textMl.match(/kit/) || textMl.match(/Kit/) || textMl.match(/0g/) || textMl.match(/1g/) || textMl.match(/2g/) || textMl.match(/3g/) || textMl.match(/4g/) || textMl.match(/5g/) || textMl.match(/6g/) || textMl.match(/7g/) || textMl.match(/8g/) || textMl.match(/9g/) || textCopoBanho == "Corpo e Banho"){
			$("body").removeClass("thumb-color");
			$(".produto .cont_princ_prod .dir.com_sku .cont_sku h5").hide();
			$(".load_sku").fadeOut(500);
		}else
		{
			if($(selector).length > 0)
			{	

				$("body").addClass("thumb-color");
				var lengthSkus;
				lengthSkus = $(selector).find("label").length;

				// Coleta infos iniciais
				$.each(skuJson.skus, function(index, value){
					_owner.objSkusInfo.skuList[index] = new Object();
				    _owner.objSkusInfo.skuList[index].id = value.sku;
				    _owner.objSkusInfo.skuList[index].name = value.values[0];
				    _owner.objSkusInfo.skuList[index].thumb = "";
				    _owner.objSkusInfo.skuList[index].texture = "";

					getJsonObjectFromSkuId(_owner.objSkusInfo.skuList[index].id, _owner.objSkusInfo.skuList[index].name);
				});
						
			}			
		}
	}

	function getJsonObjectFromSkuId(intIdSku, skuName)
	{
		var urlJSONSkuInfos = pathDataSku+intIdSku;
		var objSucess;
		var _skuName = skuName;
		
		$.getJSON(urlJSONSkuInfos, function(data) 
		{
			objSucess = data;

			try
			{
				var totalImages = objSucess[0]["Images"][1].length;
				if(typeof totalImages != "undefined")
				{

					for(var i = 0; i < totalImages; i++)
					{
						var archiveTypeId = objSucess[0]["Images"][1][i]["ArchiveTypeId"];

						if(archiveTypeId == 3)
						{
							var urlThumbSku = objSucess[0]["Images"][1][i]["Path"];				
							var urlTexture = objSucess[0]["Images"][2][i]["Path"];

							$.each(_owner.objSkusInfo.skuList, function(index, value){
								if(value.id == intIdSku)
								{
									_owner.objSkusInfo.skuList[index].thumb = urlThumbSku;
									_owner.objSkusInfo.skuList[index].texture = urlTexture;
								}
							});

							$(selector).find("label").each(function(){
								if(skuName == $(this).text())
								{
									$(this).css("background", "url('"+urlThumbSku+"') center center no-repeat scroll transparent");	
									$(this).wrap("<div class='sku-option-wrap'></div>");
									$(this).parent(".sku-option-wrap").append("<div class='thumb-texture'> <img src='"+urlTexture+"' /> </div>");
								}
							});

							$("#include ul.thumbs li").each(function(){
								var thisThumb = $(this);
								$.each(_owner.objSkusInfo.skuList, function(i, val){

									if(thisThumb.find("img").attr("src") == val.thumb || thisThumb.find("img").attr("src") == val.texture)
									{
										console.log("Esconde");
										thisThumb.hide();
									}
								});
							});

							$(".load_sku").fadeOut(500);

							$(selector).find("input").live("change", function(){
								$("#include ul.thumbs li").each(function(){
									var thisThumb = $(this);
									$.each(_owner.objSkusInfo.skuList, function(i, val){
										if(thisThumb.find("img").attr("src") == val.thumb || thisThumb.find("img").attr("src") == val.texture)
										{
											thisThumb.hide();
										}
									});
								});
							});

						}
					}
				}
			}catch(error)
			{
				console.warn(error);
			}

	  	});
	}
}



