$(document).ready(function(){

	$.ajax({
	    url: "/app-marcas",
	    dataType: "html",
	    success:function(data){
        	var dataHTML = data.substring(data.indexOf("<!--initmarcasapp-->"), data.indexOf("<!--endmarcasapp-->"));
	        $(".m_marcas ul, #marcas-lateral").html(dataHTML);
	        
	        afterLoadAppMarcas();
	        $(".dif.m_marcas .sub").removeClass("fix");
	    }
	});

	function afterLoadAppMarcas()
	{
		
		$(window).bind("scroll",function(){
			var $this=$(this);
			if($this.scrollTop()>180){
				$(".btn_marca").stop(true, true).animate({right: "0px"}, 200);
			}else{
				$(".btn_marca").stop(true, true).animate({right: "-70px"}, 200);
				$('.fix').animate({right: "-300px"}, 200);
			}
		});

		$(".btn_marca").click(function(){
			$(this).animate({right: "-70px"}, 200);
			$('.fix').animate({right: "0px"}, 200);
			return false
		});

		$('.fix').mouseleave(function(){
			$(this).stop(true, true).animate({right: "-300px"}, 200);
			$(".btn_marca").stop(true, true).animate({right: "0px"}, 200);
		});


		// $('.cont_marcas_fix .cont_resu .brandFilter ul').append('<li><a href="#">A marca 01</a></li><li><a href="#">A marca 02</a></li><li><a href="#">A marca 03</a></li><li><a href="#">A marca 04</a></li><li><a href="#">A marca 05</a></li><li><a href="#">A marca 06</a></li><li><a href="#">A marca 07</a></li><li><a href="#">A marca 08</a></li><li><a href="#">A marca 09</a></li><li><a href="#">A marca 10</a></li> <li><a href="#">B marca 01</a></li><li><a href="#">B marca 02</a></li><li><a href="#">B marca 03</a></li><li><a href="#">B marca 04</a></li><li><a href="#">B marca 05</a></li><li><a href="#">B marca 06</a></li><li><a href="#">B marca 07</a></li><li><a href="#">B marca 08</a></li>');

		
		$(".cont_marcas_fix .cont_resu .brandFilter ul li a").attr("target", "_parent");

		var listaTotasMarcas = $(".cont_marcas_fix .cont_resu .brandFilter ul").html();

		// INICIALMENTE COLOCA TODAS AS MARCAS
		$(".cont_marcas_fix .cont_resu .marcas_resu ul").html(listaTotasMarcas);


		// MARCAR A ULTIMA LETRA ESCOLHIDA
		$(".cont_marcas_fix .header_marcas_fix .inicial li a").click(function(){
			$(".cont_marcas_fix .header_marcas_fix .inicial li a").removeClass("ativo");
			$(this).addClass("ativo");
		});


		$(".cont_marcas_fix .header_marcas_fix .inicial li").click(function(){

			var container = $(this).parent().parent().parent();

			container.find(".cont_resu .marcas_resu ul").html(" ");
			var letraEscolhida = $(this).attr("class");

			
			container.find(".cont_resu .brandFilter ul li a").each(function(){
				
				var nomeMarca = $(this).text().toString();
				var inicialMarca = nomeMarca.charAt(0).toLowerCase();

				if(letraEscolhida == inicialMarca){
					var marcasFiltradas = $(this).parent("li").html();

					container.find(".cont_resu .marcas_resu ul").append("<li>" + marcasFiltradas + "</li>");		
				}

			});
			
			if(container.find(".cont_resu .marcas_resu li").length < 1){
				container.find(".cont_resu .marcas_resu ul").html("<p>Nenhuma marca com a letra <span>" + letraEscolhida + "</span></p>");
			}


			// SCROLL
			if(container.find(".cont_resu .marcas_resu li").length > 9){			
				container.find(".cont_resu .marcas_resu ul").mCustomScrollbar();
				// $(".m_marcas .cont_marcas_fix .cont_resu .marcas_resu ul").mCustomScrollbar();
			}


			return false

		});


		// ver todas
		$("a.vertodas").click(function(){
			$(".cont_marcas_fix .cont_resu .marcas_resu ul").html(listaTotasMarcas);
			$(".cont_marcas_fix .header_marcas_fix .inicial li a").removeClass("ativo");

			// SCROLL
			if($(".cont_marcas_fix .cont_resu .marcas_resu li").length > 9){			
				$(".cont_marcas_fix .cont_resu .marcas_resu ul").mCustomScrollbar();
				// $(".m_marcas .cont_marcas_fix .cont_resu .marcas_resu ul").mCustomScrollbar();
			}

			return false
		});



		// SCROLL
		if($(".cont_marcas_fix .cont_resu .marcas_resu li").length > 9){			
				$(".cont_marcas_fix .cont_resu .marcas_resu ul").mCustomScrollbar();
				// $(".m_marcas .cont_marcas_fix .cont_resu .marcas_resu ul").mCustomScrollbar();
		}
	}
	

});