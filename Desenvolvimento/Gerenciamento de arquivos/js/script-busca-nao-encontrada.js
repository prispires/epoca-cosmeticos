$(document).ready(function() {

    //Nome do produto Buscado
    $('.produto-nao-encontrado').text(window.location.pathname.substring(1));

    // Exibe as prateleiras
    if($('.busca-vazio').length){
        $($('.cont100')[0]).css('display', 'none');
        $($('.cont100')[1]).css('display', 'block');
        console.log('exibi prateleiras');
    }

    // Monta mapa de departamentos
    $(".cont100.cont100.busca-nao-encontrada .cont").append("<ul class='columns'></ul>");
    var contclass = 0;

    $(".cont100.busca-nao-encontrada .cont .menu-departamento h3").each(function(){
        $('.columns').append('<li class="colmap mapcol'+contclass+'"></li>');
        $(this).next().appendTo('.columns li.mapcol'+contclass+'');
        $(this).prependTo('.columns li.mapcol'+contclass+'');
        if(contclass == 6){$(".menu-departamento").show();}
        contclass++;
    });



    if(localStorage.getItem('produto') !== null) {
        console.log('tenho localstorage');
        $('.carrega-produtos').show();

        produtoVisitado = JSON.parse(localStorage.getItem("produto"));
        $("body").append("<div class='divHidden' style='display: none'></div>");
        $('.carrega-produtos').load('/buscapagina?sl=ef3fcb99-de72-4251-aa57-71fe5b6e149f&PS=1&cc=10&sm=0&PageNumber=1&ft='+encodeURI(produtoVisitado.nome));
        $('.divHidden').load('/buscapagina?sl=ef3fcb99-de72-4251-aa57-71fe5b6e149f&PS=50&cc=50&sm=0&PageNumber=1&ft='+encodeURI(produtoVisitado.categoria));

        $(document).ajaxStop(function(){
            $('.helperComplement').remove();

            $('.divHidden li').each(function(){
                $(this).appendTo(".carrega-produtos .prateleira.vitrine.n1colunas ul");
            });

            if ($(".carrega-produtos .prateleira ul li").length > 4) {
                $(".carrega-produtos .prateleira ul").jcarousel({
                    scroll: 1
                });
            }
        });

    } else {

        $('.prod-sem-localstorage').show();

        $('.helperComplement').remove();

        if ($(".prod-sem-localstorage .prateleira ul li").length > 4) {
            $(".prod-sem-localstorage .prateleira ul").jcarousel({
                scroll: 1
            });
        }
    }
        
});








//http://cea.vtexcommercestable.com.br/buscapagina?sl=9a856f41-5f7a-4739-92e1-e5d2150be0e7&PS=5&cc=10&sm=0&PageNumber=1&ft=camiseta%20polo