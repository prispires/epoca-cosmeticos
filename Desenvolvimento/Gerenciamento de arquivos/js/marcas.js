// Documento de script JS em jQuery
// Autor: Profite
// Ultima data de alteração: 19/09/2013


/* Função que retorna a primeira letra da string*/
function verificaLetra(nameMarca){
	// Pegando primeira letra da marca
	var letraInicial = nameMarca.charAt(0);
	// Transformando a letra em minuscula
	var letra = letraInicial.toLowerCase();
	// Retorna primeira letra
	return letra;
};

// Função verifica se o primeiro caracter é um numero
function verificaNum(str){ 
	var er = /^[0-9]+$/; 
	return (er.test(str));
}

/* Função que adiciona a marca a div certa quando a letra inicial é um caracter do alfabeto*/
function appendMarcasLetra(letra, marca){
	// Monta id para colocar a marca na categoria de letra certa
	var transformId = '#' + letra + ' ul';
	// Montando html para fazer o append
	var htmlMarca = '<li>' + marca + '</li>';
	// Append na categoria certa
	$(transformId).append(htmlMarca);
};

/* Função que adiciona a marca a div certa quando a letra inicial é um numero*/
function appendMarcasNum(marca){
	// Montando html para fazer o append
	var htmlMarca = '<li>' + marca + '</li>';
	// Append na categoria certa
	$('#123 ul').append(htmlMarca);
};

$(document).ready(function(){

	// Varre todas as marcas trazidas
	$('.menu-departamento .brandFilter ul li').each(function(){

		// Resgata a marca e a trasforma em uma string
		var nameMarca = $(this).find('a').text().toString();
		// Chama a função verificaLetra enviando a marca como parametro
		var letra = verificaLetra(nameMarca);
		// Variavel recebe marca com já com o link
		var marcaLink = $(this).html();
		// Verifica se é um numero
		var enumero = verificaNum(letra);
		// Se a variavel letra for diferente de um numero
		if (enumero == false){
			// Chama a função para fazer o append na marca
			appendMarcasLetra(letra, marcaLink);
		}
		else{
			// Chama a função para fazer o append na marca se o nome começar com um numero
			appendMarcasNum(marcaLink);
		}
	});

	escondeDivsVazia();

	// Evento de clique para filtro
	// Clique no a (Aparecendo de divs de A a E)
	$('#content #marcas #title_filtro div#filtro ul li a.a').click(function(){
		$('#content #marcas #title_filtro div#filtro ul li a').removeClass('active');
		$('#content #marcas #content_marcas p.mensagem').remove();
		filtroAE();
		$(this).addClass('active');
		return false;
	});
	// Clique no f (Aparecendo de divs de F a J)
	$('#content #marcas #title_filtro div#filtro ul li a.f').click(function(){
		$('#content #marcas #title_filtro div#filtro ul li a').removeClass('active');
		$('#content #marcas #content_marcas p.mensagem').remove();
		filtroFJ();
		$(this).addClass('active');
		return false;
	});
	// Clique no k (Aparecendo de divs de K a O)
	$('#content #marcas #title_filtro div#filtro ul li a.k').click(function(){
		$('#content #marcas #title_filtro div#filtro ul li a').removeClass('active');
		$('#content #marcas #content_marcas p.mensagem').remove();
		filtroKO();
		$(this).addClass('active');
		return false;
	});
	// Clique no p (Aparecendo de divs de P a T)
	$('#content #marcas #title_filtro div#filtro ul li a.p').click(function(){
		$('#content #marcas #title_filtro div#filtro ul li a').removeClass('active');
		$('#content #marcas #content_marcas p.mensagem').remove();
		filtroPT();
		$(this).addClass('active');
		return false;
	});
	// Clique no u (Aparecendo de divs de U a Y)
	$('#content #marcas #title_filtro div#filtro ul li a.u').click(function(){
		$('#content #marcas #title_filtro div#filtro ul li a').removeClass('active');
		$('#content #marcas #content_marcas p.mensagem').remove();
		filtroUY();
		$(this).addClass('active');
		return false;
	});
	// Clique no z (Aparecendo de divs de Z a 123)
	$('#content #marcas #title_filtro div#filtro ul li a.z').click(function(){
		$('#content #marcas #title_filtro div#filtro ul li a').removeClass('active');
		$('#content #marcas #content_marcas p.mensagem').remove();
		filtroZ123();
		$(this).addClass('active');
		return false;
	});

	// Evento de clique para remover o filtro
	$('#content #marcas #title_filtro').click(function(){
		$('#content #marcas #content_marcas p.mensagem').remove();
		$('#content #marcas #title_filtro div#filtro ul li a').removeClass('active');
		removeFiltro();
	});
});




// Função de filtro (Aparecendo de divs de A a E)
function filtroAE(){
	$('#content #marcas #content_marcas div').hide();
	$('#content #marcas #content_marcas div#a').fadeIn('fast');
	$('#content #marcas #content_marcas div#b').fadeIn('fast');
	$('#content #marcas #content_marcas div#c').fadeIn('fast');
	$('#content #marcas #content_marcas div#d').fadeIn('fast');
	$('#content #marcas #content_marcas div#e').fadeIn('fast');
	escondeDivsVazia();
};

// Função de filtro (Aparecendo de divs de F a J)
function filtroFJ(){
	$('#content #marcas #content_marcas div').hide();
	$('#content #marcas #content_marcas div#f').fadeIn('fast');
	$('#content #marcas #content_marcas div#g').fadeIn('fast');
	$('#content #marcas #content_marcas div#h').fadeIn('fast');
	$('#content #marcas #content_marcas div#i').fadeIn('fast');
	$('#content #marcas #content_marcas div#j').fadeIn('fast');
	escondeDivsVazia();
};

// Função de filtro (Aparecendo de divs de K a O)
function filtroKO(){
	$('#content #marcas #content_marcas div').hide();
	$('#content #marcas #content_marcas div#k').fadeIn('fast');
	$('#content #marcas #content_marcas div#l').fadeIn('fast');
	$('#content #marcas #content_marcas div#m').fadeIn('fast');
	$('#content #marcas #content_marcas div#n').fadeIn('fast');
	$('#content #marcas #content_marcas div#o').fadeIn('fast');
	escondeDivsVazia();
};

// Função de filtro (Aparecendo de divs de P a T)
function filtroPT(){
	$('#content #marcas #content_marcas div').hide();
	$('#content #marcas #content_marcas div#p').fadeIn('fast');
	$('#content #marcas #content_marcas div#q').fadeIn('fast');
	$('#content #marcas #content_marcas div#r').fadeIn('fast');
	$('#content #marcas #content_marcas div#s').fadeIn('fast');
	$('#content #marcas #content_marcas div#t').fadeIn('fast');
	escondeDivsVazia();
};

// Função de filtro (Aparecendo de divs de U a Y)
function filtroUY(){
	$('#content #marcas #content_marcas div').hide();
	$('#content #marcas #content_marcas div#u').fadeIn('fast');
	$('#content #marcas #content_marcas div#v').fadeIn('fast');
	$('#content #marcas #content_marcas div#x').fadeIn('fast');
	$('#content #marcas #content_marcas div#y').fadeIn('fast');
	escondeDivsVazia();
};

// Função de filtro (Aparecendo de divs de Z a 123) 
function filtroZ123(){
	$('#content #marcas #content_marcas div').hide();
	$('#content #marcas #content_marcas div#z').fadeIn('fast');
	$('#content #marcas #content_marcas div#w').fadeIn('fast');
	$('#content #marcas #content_marcas div#123').fadeIn('fast');
	escondeDivsVazia();
};

// Remove o filtro
function removeFiltro(){
	$('#content #marcas #content_marcas div').fadeIn('fast');
	escondeDivsVazia();
};

// Função que checa divs das letras que estão vazias
function escondeDivsVazia(){
	$('#content #marcas #content_marcas div').each(function(){
		var conteudo = $(this).find('ul').html();
		if (conteudo == ''){
			$(this).hide();
		};
	});
};

/* Função que checa se o filtro trouxe alguma li, 
senão aparece uma mensagem. 
function semMarcaMensagem(){
	var contadorDivs = 0;
	$('#content #marcas #content_marcas div').each(function(){
		var styleContent = $(this).attr('style');
		console.log(styleContent);
		if (styleContent == 'display: none;') {
			console.log(styleContent);
			contadorDivs++;
		}
	});
	console.log(contadorDivs);
	if (contadorDivs == 26){
		var mensagem = '<p class="mensagem"><span>Ops</span>,não encontramos nenhuma marca com essas letras</span><br/>Selecione outro filtro!</p>';
		$('#content #marcas #content_marcas').append(mensagem);
	};	
};*/




// COLOCA A MESMA ALTURA PARA TODAS AS LETRAS PRA NÃO QUEBRAR O LAYOUT
$(document).ready(function(){
	if($("body.nossasmarcas").length > 0){
		var totalBox = $("body.nossasmarcas #content #marcas #content_marcas div").length;
		var heightToCompare,  heightToSetBoxs = 0;

		for(var i = 0; i < totalBox; i++)
		{
		heightToCompare = $("body.nossasmarcas #content #marcas #content_marcas div").eq(i).height();

		if(heightToCompare >= heightToSetBoxs)
		{
		heightToCompare = $("body.nossasmarcas #content #marcas #content_marcas div").eq(i).height();
		heightToSetBoxs = heightToCompare;
		}

		   if(i == totalBox-1) $("body.nossasmarcas #content #marcas #content_marcas div").css("height", heightToSetBoxs+"px");

		}
	}
});