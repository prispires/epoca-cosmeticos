/* SCRIPT BOX SMART */
$(document).ready(function() {

 var date = new Date();
 var minutes = 180;
 date.setTime(date.getTime() + (minutes * 60 * 1000));




    $('a[name=modal]').click(function(e) {
        e.preventDefault();

        var id = $(this).attr('href');

        var maskHeight = $(document).height();
        var maskWidth = $(window).width();

        $('.bg-boxsmart').css({'width':maskWidth,'height':maskHeight});

        $('.bg-boxsmart').fadeIn(1000);
        $('.bg-boxsmart').fadeTo("slow",0.8);

        //Get the window height and width
        var winH = $(window).height();
        var winW = $(window).width();

        $(id).css('top',  winH/2-$(id).height()/2);
        $(id).css('left', winW/2-$(id).width()/2);

        $(id).fadeIn(2000);
    });

    var trocaBanner = $('.boxsmart .box-banner img').attr('alt');
    var trocaHtml = $('.boxsmart div').attr('data-banner');

    $('.fechar-boxsmart').click(function (e) {
        e.preventDefault();

        $('.bg-boxsmart, .boxsmart').hide();
        $.cookie('Cookie-banner-boxsmart', trocaBanner, { expires: 1 });
        $.cookie('Cookie-html-boxsmart', trocaHtml, { expires: 1 });
        $.cookie('close-boxsmart', 'fecha por 3 horas', { expires: date });
    });

    $('.bg-boxsmart').click(function () {
        $(this).hide();
        $('.boxsmart').hide();
        $.cookie('Cookie-banner-boxsmart', trocaBanner, { expires: 1 });
        $.cookie('Cookie-html-boxsmart', trocaHtml, { expires: 1 });
        $.cookie('close-boxsmart', 'fecha por 3 horas', { expires: date });
    });


});

$(window).load(function(){
    if(!$.cookie('close-boxsmart')){

        if($('.boxsmart .box-banner img').length > '0' || $('.boxsmart div').length > '0') {
            $('a[name=modal]').click();
        }

        if($.cookie('Cookie-banner-boxsmart')){
            if($('.boxsmart .box-banner img').attr('alt') != $.cookie('Cookie-banner-boxsmart')) {
                console.log($('.boxsmart .box-banner img').attr('alt') + ' - ' + $.cookie('Cookie-banner-boxsmart'));
                $.cookie('Cookie-banner-boxsmart', trocaBanner, { expires: -1 });
                $.cookie('Cookie-html-boxsmart', trocaHtml, { expires: -1 });
            } else {
                $('.bg-boxsmart').hide();
                $('.boxsmart').hide();
            }
        }

        if($.cookie('Cookie-html-boxsmart')){
            if($('.boxsmart div').attr('data-banner') != $.cookie('Cookie-html-boxsmart')) {
                console.log($('.boxsmart div').attr('data-banner') + ' - ' + $.cookie('Cookie-html-boxsmart'));
                $.cookie('Cookie-banner-boxsmart', trocaBanner, { expires: -1 });
                $.cookie('Cookie-html-boxsmart', trocaHtml, { expires: -1 });
            } else {
                $('.bg-boxsmart').hide();
                $('.boxsmart').hide();
            }
        }

    }

});