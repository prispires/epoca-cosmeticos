// Documento de script JS em jQuery
// Autor: Profite
// Ultima data de alteração: 19/09/2013

$(document).ready(function(){


// Mapa Lojas

    // Definindo posições dos marcadores e texto do tooltip default
        
        var Leblon = new google.maps.LatLng(-22.983652,-43.219283);
        
        var Barra = new google.maps.LatLng(-23.001044,-43.386009); 
        
        initializeMap();

        criarMarcador(Leblon);
        criarMarcador(Barra);

     //Função de Inicialização
        function initializeMap() {
          geocoder = new google.maps.Geocoder();
          var id = document.getElementById("map");
          var latlng = new google.maps.LatLng(-22.984313,-43.29065);
          var myOptions = {
             zoom: 10,
             center: latlng,
             mapTypeId: google.maps.MapTypeId.ROADMAP
          }
          map = new google.maps.Map(id, myOptions);
        }

        //Função para criar marcador e tooltip
        function criarMarcador(localizacao) { 
          // criar o marcador
          var marcador = new google.maps.Marker({
              position: localizacao,
              map: map,
              icon: new google.maps.MarkerImage('http://develop-epocacosmeticos.vtexcommercebeta.com.br/arquivos/pin_epoca.png')
          });
          // criar uma tooltip para informações
          /*var infowindow = new google.maps.InfoWindow({
                                  content: info,
                                  size: new google.maps.Size(50, 50)
                            });
          //No evento click abrir uma infowindow
          google.maps.event.addListener(marcador, 'click', function() {
                          infowindow.open(map, marcador);
          });*/

        }

        // Definindo click e zoom para cada loja

        $('body.nossaslojas #content #content_map #info #loja_leblon button.button_map').click(function(){
            //centralizando o mapa
            map.setCenter( new google.maps.LatLng(-22.983652,-43.219283) );
            //mudando o zoom do mapa
            map.setZoom(15);
        }); 

        $('body.nossaslojas #content #content_map #info #loja_barra button.button_map').click(function(){
            //centralizando o mapa
            map.setCenter( new google.maps.LatLng(-23.001044,-43.386009) );
            //mudando o zoom do mapa
            map.setZoom(15);
        });


});     

