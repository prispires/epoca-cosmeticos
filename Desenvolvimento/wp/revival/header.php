<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
    <title><?php if( ! is_home() ): wp_title( '|', true, 'right' ); endif; bloginfo( 'name' ); ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Favicon and Feed -->
    <?php $favicon = get_option( 'revivaltheme_favicon_upload' );
    if ($favicon !== '') { ?>
        <link rel="shortcut icon" type="image/png" href="<?php echo $favicon; ?>">
    <?php }?>

	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">

<?php wp_head(); ?>

<!-- Script Header Flutuante -->
<script type="text/javascript">
jQuery(document).ready(function($){
    $(window).bind("scroll", function() {
        var $this = $(this);
        if ($this.scrollTop() > 180) {
            $(".header_flutuante").stop(true, true).fadeIn(500);
            $(".ui-autocomplete").addClass("fixo");
        } else {
            $(".header_flutuante").stop(true, true).fadeOut(500);
            $(".ui-autocomplete").removeClass("fixo");
        }
    });
    $(".header_flutuante ul.menu_dept li.item").mouseenter(function() {
        $(".header_flutuante ul.menu_dept li ul.sub_dept").stop(true, true).slideDown(300);
    });

    $(".header_flutuante ul.menu_dept li.item").mouseleave(function() {
        $(".header_flutuante ul.menu_dept li ul.sub_dept").stop(true, true).slideUp(300);
    });

    $(".header_flutuante ul.menu_dept li ul.sub_dept").mouseenter(function() {
        $(".header_flutuante ul.menu_dept li.item").addClass("ativo");
    });

    $(".header_flutuante ul.menu_dept li ul.sub_dept").mouseleave(function() {
        $(".header_flutuante ul.menu_dept li.item").removeClass("ativo");
    });
    // BANNER TOPO
    // clicando no botao a.close ele fecha o bannerTop
    $(".topbanner a.close").click(function() {
        $(".topbanner").slideUp(300);
    });
});

</script>
<!-- Responsivo -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
jQuery(document).ready(function($){
      $("#hide").click(function(){
        $("#subMenuMobile").hide(900);
      });
      $("#show").click(function(){
        $("#subMenuMobile").show(900);
      });
});
</script>
  </head>


<body <?php body_class(); ?>>

<div class="off-canvas-wrap" data-offcanvas>

    <div class="inner-wrap">
<!-- Responsive -->

<?php include 'inc/1024.php';?>

<!-- Logo topo -->


            
            <header id="header" class="light" role="banner">
            <div class="logoPost"><img src="http://blog.epocacosmeticos.com.br/wp-content/themes/revival/img/logo.jpg" /></div>
                <?php //get_template_part( 'inc/header', 'left'); ?>

                <?php if ( function_exists('is_active_sidebar') && is_active_sidebar('secondary-sidebar') ) { ?>
                    <div class="sidebar-icon"><a class="right-off-canvas-toggle"><i class="i-menu"></i></a></div>

                <?php } ?>


            </header>

