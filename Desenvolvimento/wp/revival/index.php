<?php
$homepage_layout = get_option('revivaltheme_homepage_layout' );
$homepage_sidebar = get_option('revivaltheme_homepage_sidebar' );
?>

<?php get_header(); ?>

<?php dynamic_sidebar("Homepage Top"); ?>


    <section id="content" class="animated fadeIn" role="main">

        <div class="row collapse gray">

            <?php if ($homepage_sidebar == 'fullwidth') { ?>

            <div class="<?php if ($homepage_layout == 'blog') { ?>large-10 large-centered<?php } else { ?>large-12<?php } ?> columns">

                <?php } else { ?>

                <div class="large-9 medium-12 small-12 columns">

                    <?php } ?>
                    <?php if ( have_posts() ) : ?>


                        <?php // Layout

                        if ($homepage_layout == 'magazine_2') { the_content(); }
                        elseif ($homepage_layout == 'magazine_3') { get_template_part('layout/loop', 'magazine_3');}
                        elseif ($homepage_layout == 'magazine_4') { get_template_part('layout/loop', 'magazine_4'); }
                        else { get_template_part('layout/loop', 'blog');  } ?>

                    <?php else : ?>

                        <?php get_template_part( 'content', 'none' ); ?>


                    <?php endif; // end have_posts() check ?>


                </div><!--end main column-->



                <?php if ( $homepage_sidebar == 'fullwidth') { } else { ?>

                    <?php get_sidebar(); ?>

                <?php } ?>


            </div><!--End Row-->

    </section>

<?php
if ($_GET['res'] == true){
    $resolucao = '1024';
                        }else{
    $resolucao = "<script>document.write(+screen.width)</script>";
                                }
    if ($resolucao == '320' || $resolucao == '480') {
        include 'footerMobile.php';
    } else {
        include 'footer.php';
    }
?>
