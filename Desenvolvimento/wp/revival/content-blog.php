<?php $captionbg = get_field('caption_background'); ?>

<div class="<?php if ( $captionbg ) { echo $captionbg; } else { echo 'white'; } ?>">

    <article <?php post_class(''); ?>>

        <div class="row large-block">

            <?php if ( has_post_format( 'status' ) ) { ?>

                <div class="large-10 large-centered columns">

                    <?php get_template_part( 'inc/featured'); ?>
                    <?php get_template_part( 'inc/blog', 'text'); ?>

                </div><!--end column-->


            <?php } elseif ( has_post_format( 'quote' ) || has_post_format( 'aside' ) || ('' == get_the_post_thumbnail() && !has_post_format( 'video' && !has_post_format( 'audio' ) && !has_post_format( 'gallery' ) ))) { ?>

                <div class="large-10 large-centered columns">

                    <?php get_template_part( 'inc/blog', 'text'); ?>

                </div><!--end column-->
            <?php } else { ?>

                <div class="large-6 columns">


                    <?php get_template_part( 'inc/featured'); ?>


                </div><!--end column-->


                <div class="large-6 columns">


                    <?php get_template_part( 'inc/blog', 'text'); ?>

                </div><!--end column-->

            <?php } ?>

        </div><!--end row-->

    </article>

</div>