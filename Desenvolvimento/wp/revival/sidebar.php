<div class="large-3 columns end right hide-for-medium-down">

    <aside id="sidebar" class="white animated fadeInDown" role="complementary">


        <?php if (is_front_page()) {

            if ( (function_exists('is_active_sidebar') && is_active_sidebar('primary-sidebar')) ) {

                dynamic_sidebar( 'primary-sidebar' );

            } else {

                echo 'Place any widget here via <a href="'.home_url().'/wp-admin/widgets.php">Appearance -> Widget</a> section.';

            }

        } elseif ( is_page() ) {


            if ( function_exists('is_active_sidebar') && is_active_sidebar('page-sidebar') ) {
                dynamic_sidebar( 'page-sidebar' );

            } elseif ( function_exists('is_active_sidebar') && is_active_sidebar('primary-sidebar') ) {

                dynamic_sidebar( 'primary-sidebar' );

            } else {

                echo 'Place any widget here via <a href="'.home_url().'/wp-admin/widgets.php">Appearance -> Widget</a> section.';

            }

        } elseif ( is_single() ) {


            if ( function_exists('is_active_sidebar') && is_active_sidebar('post-sidebar') ) {
                dynamic_sidebar( 'post-sidebar' );

            } elseif ( function_exists('is_active_sidebar') && is_active_sidebar('primary-sidebar') ) {

                dynamic_sidebar( 'primary-sidebar' );

            } else {

                echo 'Place any widget here via <a href="'.home_url().'/wp-admin/widgets.php">Appearance -> Widget</a> section.';

            }

        } elseif ( is_archive() ) {

            if ( function_exists('is_active_sidebar') && is_active_sidebar('archives-sidebar') ) {
                dynamic_sidebar( 'archives-sidebar' );

            } elseif ( function_exists('is_active_sidebar') && is_active_sidebar('primary-sidebar') ) {

                dynamic_sidebar( 'primary-sidebar' );

            } else {

                echo 'Place any widget here via <a href="'.home_url().'/wp-admin/widgets.php">Appearance -> Widget</a> section.';

            }

        } else {

        if ( function_exists('is_active_sidebar') && is_active_sidebar('primary-sidebar') ) {

            dynamic_sidebar( 'primary-sidebar' );

        } else {

            echo 'Place any widget here via <a href="'.home_url().'/wp-admin/widgets.php">Appearance -> Widget</a> section.';

        }

        } ?>

    </aside>

</div>