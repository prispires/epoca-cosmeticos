<?php $disable_featured_image = get_field('disable_featured_image'); ?>

<div class="entry-wrapper sidebar white row collapse">

    <?php if ( has_post_format( 'gallery' )) { ?>

        <?php get_template_part( 'inc/post', 'header'); ?>

        <div class="keynote">
            <?php if( $post->post_excerpt ) { the_excerpt(); } else { } ?>
        </div>

        <?php get_template_part( 'inc/share'); get_template_part('inc/formats'); ?>

    <?php } ?>


    <div class="large-7 large-offset-1 medium-12 columns media-column">

        <?php if (!has_post_format( 'gallery' )){ ?>

            <?php get_template_part( 'inc/post', 'header'); ?>

            <?php if ( has_post_format( 'quote' ) || is_page() || has_post_format( 'status' ) ) { } else { ?>
                <div class="keynote">
                    <?php if( $post->post_excerpt ) { the_excerpt(); } else { } ?>
                </div>

            <?php } ?>

            <?php if (! is_page()) { get_template_part( 'inc/share'); } ?>
            <?php { get_template_part('inc/formats'); } ?>

        <?php } ?>

        <div class="entry-content">

            <?php the_content(); ?>

            <?php get_template_part( 'inc/rating', 'criteria'); ?>

        </div>


        <footer class="single-bottom">

            <?php wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('Pages:&nbsp;&nbsp;', 'revivaltheme'), 'link_before' =>'&nbsp;&nbsp;', 'after' => '</p></nav>' )); ?>

            <?php if ( has_post_format( 'quote' ) || is_page() ) {} else { ?><?php get_template_part( 'inc/meta'); ?><?php } ?>

            <?php dynamic_sidebar("After Post Horizontal"); ?>

        </footer>

    </div><!-- end wide column -->


    <?php get_sidebar(); ?>

</div>
