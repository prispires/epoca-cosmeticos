<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
    <title><?php if( ! is_home() ): wp_title( '|', true, 'right' ); endif; bloginfo( 'name' ); ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Favicon and Feed -->
    <?php $favicon = get_option( 'revivaltheme_favicon_upload' );
    if ($favicon !== '') { ?>
        <link rel="shortcut icon" type="image/png" href="<?php echo $favicon; ?>">
    <?php }?>

	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">

<?php wp_head(); ?>

</head>


<body <?php body_class(); ?>>

<div class="off-canvas-wrap" data-offcanvas>

    <div class="inner-wrap">

        <?php $newsticker = get_option('revivaltheme_newsticker' ); if ( $newsticker != '') { ?>

        <div class="row collapse">

            <div class="large-7 columns">


                    <?php get_template_part( 'layout/loop', 'newsticker'); ?>


            </div>

            <div class="large-5 columns">

            <?php get_template_part( 'inc/social'); ?>

            </div>

        </div>

        <?php } else { ?>

            <?php get_template_part( 'inc/social'); ?>

        <?php  } ?>

        <?php dynamic_sidebar('before-header'); ?>

        <!-- Start the main container -->
        <section id="wrapper" role="document">

            <?php
            $carousel = get_option('revivaltheme_carousel' );
            $carousel_position = get_option('revivaltheme_carousel_position' );
            if ( ( is_home()&&!is_paged() ) && ($carousel != '') && ($carousel_position == 'top')) { ?>


                <?php get_template_part( 'layout/loop', 'carousel_small'); ?>


            <?php  } ?>

            <header id="header" class="dark" role="banner">

                <?php get_template_part( 'inc/header', 'left'); ?>

                <?php if ( function_exists('is_active_sidebar') && is_active_sidebar('secondary-sidebar') ) { ?>
                    <div class="sidebar-icon"><a class="right-off-canvas-toggle"><i class="i-menu"></i></a></div>

                <?php } ?>


            </header>


            <?php
            $carousel = get_option('revivaltheme_carousel' );
            $carousel_position = get_option('revivaltheme_carousel_position' );
            if ( ( is_home()&&!is_paged() ) && ($carousel != '') && ($carousel_position == 'bottom')) { ?>


                <?php get_template_part( 'layout/loop', 'carousel'); ?>


            <?php  } ?>


            <?php if ( is_front_page() ) { ?>

                <?php dynamic_sidebar('homepage-after-header'); ?>

            <?php } ?>

