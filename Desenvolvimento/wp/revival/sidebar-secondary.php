<?php

if ( (function_exists('is_active_sidebar') && is_active_sidebar('secondary-sidebar')) && !is_singular() ) {

        echo '<aside id="side-panel" class="right-off-canvas-menu dark" role="complementary">';

        dynamic_sidebar( 'secondary-sidebar' );

        echo '</aside>';

} elseif ( (function_exists('is_active_sidebar') && is_active_sidebar('secondary-post-sidebar')) && is_singular () ) {

    echo '<aside id="side-panel" class="right-off-canvas-menu dark" role="complementary">';

        dynamic_sidebar( 'secondary-post-sidebar' );

        echo '</aside>';

} elseif ( (function_exists('is_active_sidebar') && is_active_sidebar('secondary-sidebar')) && (function_exists('is_active_sidebar') && !is_active_sidebar('secondary-post-sidebar')) && is_singular () ) {

    echo '<aside id="side-panel" class="right-off-canvas-menu dark" role="complementary">';

    dynamic_sidebar( 'secondary-sidebar' );

    echo '</aside>';

} else { } ?>