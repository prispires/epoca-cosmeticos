<?php // Create widget areas

register_sidebar(array('name'=> 'Primary Sidebar',
'id' => 'primary-sidebar',
'description' => __('Sidebar for homepage and archives.', 'revivaltheme'),
'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="panel">',
        'after_widget' => '</div></section>',
'before_title' => '<header class="widget-title"><h6>',
        'after_title' => '</h6></header>'
));

register_sidebar(array('name'=> 'Secondary Sidebar',
'id' => 'secondary-sidebar',
'description' => __('Right slide-out sidebar for homepage and archives. If is empty slide panel will be disabled.', 'revivaltheme'),
'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="panel">',
        'after_widget' => '</div></section>',
'before_title' => '<header class="widget-title"><h6>',
        'after_title' => '</h6></header>'
));


register_sidebar(array('name'=> 'Post Sidebar',
'id' => 'post-sidebar',
'description' => __('Sidebar for a single posts. If is empty, the primary widget area will be used.', 'revivaltheme'),
'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="panel">',
        'after_widget' => '</div></section>',
'before_title' => '<header class="widget-title"><h6>',
        'after_title' => '</h6></header>'
));

register_sidebar(array('name'=> 'Page Sidebar',
'id' => 'page-sidebar',
'description' => __('Sidebar for static pages. If is empty, the primary widget area will be used.', 'revivaltheme'),
'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="panel">',
        'after_widget' => '</div></section>',
'before_title' => '<header class="widget-title"><h6>',
        'after_title' => '</h6></header>'
));

register_sidebar(array('name'=> 'Secondary Post Sidebar',
'id' => 'secondary-post-sidebar',
'description' => __('Right slide-out sidebar for single posts and pages. If is empty Homepage/Archives Secondary Sidebar will be used.', 'revivaltheme'),
'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="panel">',
        'after_widget' => '</div></section>',
'before_title' => '<header class="widget-title"><h6>',
        'after_title' => '</h6></header>'
));

register_sidebar(array('name'=> 'Archives Sidebar',
'id' => 'archives-sidebar',
'description' => __('Sidebar for archives. If is empty, the primary widget area will be used.', 'revivaltheme'),
'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="panel">',
        'after_widget' => '</div></section>',
'before_title' => '<header class="widget-title"><h6>',
        'after_title' => '</h6></header>'
));

register_sidebar(array('name'=> 'Footer White First',
'id' => 'footer-white-first',
'description' => __('Appears in the footer section of the site. Widgets will be displayed in the first column on the white background.', 'revivaltheme'),
'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="panel">',
        'after_widget' => '</div></section>',
'before_title' => '<header class="widget-title"><h6>',
        'after_title' => '</h6></header>'
));

register_sidebar(array('name'=> 'Footer White Second',
'id' => 'footer-white-second',
'description' => __('Appears in the footer section of the site. Widgets will be displayed in the second column on the white background.', 'revivaltheme'),
'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="panel">',
        'after_widget' => '</div></section>',
'before_title' => '<header class="widget-title"><h6>',
        'after_title' => '</h6></header>'
));


register_sidebar(array('name'=> 'Footer White Third',
'id' => 'footer-white-third',
'description' => __('Appears in the footer section of the site. Widgets will be displayed in the third column on the white background.', 'revivaltheme'),
'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="panel">',
        'after_widget' => '</div></section>',
'before_title' => '<header class="widget-title"><h6>',
        'after_title' => '</h6></header>'
));

register_sidebar(array('name'=> 'Footer White Fourth',
'id' => 'footer-white-fourth',
'description' => __('Appears in the footer section of the site. Widgets will be displayed in the fourth column on the white background.', 'revivaltheme'),
'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="panel">',
        'after_widget' => '</div></section>',
'before_title' => '<header class="widget-title"><h6>',
        'after_title' => '</h6></header>'
));

register_sidebar(array('name'=> 'Footer Dark First Wide',
'id' => 'footer-dark-first-wide',
'description' => __('Appears in the footer section of the site. Widgets will be displayed in the first column of the two columns layout on the dark background.', 'revivaltheme'),
'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="panel">',
        'after_widget' => '</div></section>',
'before_title' => '<header class="widget-title"><h6>',
        'after_title' => '</h6></header>'
));

register_sidebar(array('name'=> 'Footer Dark Second Wide',
'id' => 'footer-dark-second-wide',
'description' => __('Appears in the footer section of the site. Widgets will be displayed in the second column of the two columns layout on the dark background.', 'revivaltheme'),
'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="panel">',
        'after_widget' => '</div></section>',
'before_title' => '<header class="widget-title"><h6>',
        'after_title' => '</h6></header>'
));

register_sidebar(array('name'=> 'Footer Fullwidth',
'id' => 'footer-fullwidth',
'description' => __('Appears in the footer section of the site. Widgets will be displayed in one wide column on the bottom of the page on the dark background.', 'revivaltheme'),
'before_widget' => '<section id="%1$s" class="row widget %2$s"><div class="footer-wide"><div class="panel">',
            'after_widget' => '</div></div></section>',
'before_title' => '<header class="widget-title"><h6>',
        'after_title' => '</h6></header>'
));

register_sidebar(array('name'=> 'Homepage After Header',
'id' => 'homepage-after-header',
'description' => __('Appears after header on homepage. You can place ads here, textwidget or any other widget with horizontal layout.', 'revivaltheme'),
'before_widget' => '<section id="%1$s" class="widget %2$s horizontal-panel white">',
    'after_widget' => '</section>',
'before_title' => '<header class="widget-title"><h6>',
        'after_title' => '</h6></header>'
));

register_sidebar(array('name'=> 'Before Header',
'id' => 'before-header',
'description' => __('Appears after main menu on all pages. You can place ads here, textwidget or any other widget with horizontal layout.', 'revivaltheme'),
'before_widget' => '<section id="%1$s" class="widget %2$s horizontal-panel white">',
    'after_widget' => '</section>',
'before_title' => '<header class="widget-title"><h6>',
        'after_title' => '</h6></header>'
));

register_sidebar(array('name'=> 'Before Post Horizontal',
'id' => 'before-post-horizontal',
'description' => __('Appears before signle post. You can place ads here, textwidget or any other widget with horizontal layout.', 'revivaltheme'),
'before_widget' => '<section id="%1$s" class="widget %2$s horizontal-panel white">',
    'after_widget' => '</section>',
'before_title' => '<header class="widget-title"><h5>',
        'after_title' => '</h5></header>'
));

register_sidebar(array('name'=> 'After Post Horizontal',
'id' => 'after-post-horizontal',
'description' => __('Appears after signle post. You can place ads here, textwidget or any other widget with horizontal layout.', 'revivaltheme'),
'before_widget' => '<section id="%1$s" class="widget %2$s horizontal-panel white">',
    'after_widget' => '</section>',
'before_title' => '<header class="widget-title"><h6>',
        'after_title' => '</h6></header>'
));