<?php

function revivaltheme_customizer( $wp_customize ) {
    $wp_customize->add_section(
        'revivaltheme_section_one',
        array(
            'title' => __('General','revivaltheme'),
            'description' => __('General settings','revivaltheme'),
            'priority' => 35,
        )
    );

    $wp_customize->add_section(
        'revivaltheme_section_two',
        array(
            'title' => __('Fonts','revivaltheme'),
            'description' => __('Google Fonst','revivaltheme'),
            'priority' => 36,
        )
    );


    $wp_customize->add_section(
        'revivaltheme_section_three',
        array(
            'title' => __('Layouts','revivaltheme'),
            'description' => __('Homepage, archive, author and search results layouts','revivaltheme'),
            'priority' => 37,
        )
    );

    $wp_customize->add_section(
        'revivaltheme_section_four',
        array(
            'title' => __('Social Media','revivaltheme'),
            'description' => __('Social Media buttons','revivaltheme'),
            'priority' => 38,
        )
    );

    /*Logo*/
    $wp_customize->add_setting( 'revivaltheme_logo_upload',
        array(
            'type' => 'option',
            'default' => ''
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'revivaltheme_logo_upload',
            array(
                'label' => __('Logo Image','revivaltheme'),
                'section' => 'revivaltheme_section_one',
                'settings' => 'revivaltheme_logo_upload',
                'priority' => 1
            )
        )
    );

    /*Header Background*/
    $wp_customize->add_setting( 'revivaltheme_header_bg',
        array(
            'type' => 'option',
            'default' => ''
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'revivaltheme_header_bg',
            array(
                'label' => __('Header Background','revivaltheme'),
                'section' => 'revivaltheme_section_one',
                'settings' => 'revivaltheme_header_bg',
                'priority' => 2
            )
        )
    );

    /*Favicon*/
    $wp_customize->add_setting( 'revivaltheme_favicon_upload',
        array(
            'type' => 'option',
            'default' => ''
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'revivaltheme_favicon_upload',
            array(
                'label' => __('Favicon Image (use .png format)','revivaltheme'),
                'section' => 'revivaltheme_section_one',
                'settings' => 'revivaltheme_favicon_upload',
                'priority' => 5
            )
        )
    );

    /*Header style @todo
    $wp_customize->add_setting(
        'revivaltheme_header_style',
        array(
            'default' => 'centered',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_header_style'
        )
    );

    $wp_customize->add_control(
        'revivaltheme_header_style',
        array(
            'type' => 'select',
            'label' => __('Header Style','revivaltheme'),
            'section' => 'revivaltheme_section_one',
            'choices' => array(
                'centered' => __('Centered Logo','revivaltheme'),
                'left' => __('Left Logo & Right Banner','revivaltheme')
            ),
            'priority' => 4
        )
    );*/


    /* NewsTicker */

    $wp_customize->add_setting(
        'revivaltheme_newsticker',
        array(
            'default' => '',
            'sanitize_callback' => 'revivaltheme_sanitize_checkbox',
            'type' => 'option'
        )
    );

    $wp_customize->add_control(
        'revivaltheme_newsticker',
        array(
            'type' => 'checkbox',
            'label' => __('Enable NewsTicker at the header?','revivaltheme'),
            'section' => 'revivaltheme_section_one',
            'priority' => 5
        )
    );

    /*Custom Color*/

    $wp_customize->add_setting(
        'revivaltheme_custom_color_1',
        array(
            'default' => '#e54e4f',
            'type' => 'option',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'revivaltheme_custom_color_1',
            array(
                'label' => __('Bright Color (links, highlights, etc.)','revivaltheme'),
                'section' => 'revivaltheme_section_one',
                'settings' => 'revivaltheme_custom_color_1',
                'priority' => 8
            )
        )
    );

    $wp_customize->add_setting(
        'revivaltheme_custom_color_2',
        array(
            'default' => '#010101',
            'type' => 'option',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'revivaltheme_custom_color_2',
            array(
                'label' => __('Dark Color (header background, etc.)','revivaltheme'),
                'section' => 'revivaltheme_section_one',
                'settings' => 'revivaltheme_custom_color_2',
                'priority' => 9
            )
        )
    );



    /*Copyright Text*/
    $wp_customize->add_setting(
        'revivaltheme_copyright_textbox',
        array(
            'default' => __('Designed by Themeous','revivaltheme'),
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_text',
        )
    );

    $wp_customize->add_control(
        'revivaltheme_copyright_textbox',
        array(
            'label' => __('Copyright text','revivaltheme'),
            'section' => 'revivaltheme_section_one',
            'type' => 'text',
            'priority' => 10
        )
    );


    /* Footer Logo */
    $wp_customize->add_setting( 'revivaltheme_logo_footer_upload',
        array(
            'type' => 'option',
            'default' => ''
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'revivaltheme_logo_footer_upload',
            array(
                'label' => __('Footer Logo (use Logo Widget)','revivaltheme'),
                'section' => 'revivaltheme_section_one',
                'settings' => 'revivaltheme_logo_footer_upload',
                'priority' => 14
            )
        )
    );


    /* Google Fonts */

    $wp_customize->add_setting(
        'revivaltheme_google_font_headings',
        array(
            'default' => '',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_text',
        )
    );

    $wp_customize->add_control(
        'revivaltheme_google_font_headings',
        array(
            'label' => __('Google Font Name for Headings, ex. Lato','revivaltheme'),
            'section' => 'revivaltheme_section_two',
            'type' => 'text',
            'priority' => 14
        )
    );

    $wp_customize->add_setting(
        'revivaltheme_google_font_body',
        array(
            'default' => '',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_text',
        )
    );

    $wp_customize->add_control(
        'revivaltheme_google_font_body',
        array(
            'label' => __('Google Font Name for Body Copy, ex. PT Sans','revivaltheme'),
            'section' => 'revivaltheme_section_two',
            'type' => 'text',
            'priority' => 15
        )
    );

    $wp_customize->add_setting(
        'revivaltheme_nouppercase',
        array(
            'default' => '',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_checkbox',
        )
    );

    $wp_customize->add_control(
        'revivaltheme_nouppercase',
        array(
            'type' => 'checkbox',
            'label' => __('Disable uppercase for titles?','revivaltheme'),
            'section' => 'revivaltheme_section_two',
            'priority' => 16
        )
    );


    /*Comments Number */
    $wp_customize->add_setting(
        'revivaltheme_comments_number',
        array(
            'default' => __('0','revivaltheme'),
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_number',
        )
    );

    $wp_customize->add_control(
        'revivaltheme_comments_number',
        array(
            'label' => __('Comments Number','revivaltheme'),
            'section' => 'revivaltheme_section_three',
            'type' => 'text',
            'priority' => 3
        )
    );

    /*Hide Meta */

    $wp_customize->add_setting(
        'revivaltheme_hide_meta',
        array(
            'default' => 'show_all',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_hide_meta'
        )
    );

    $wp_customize->add_control(
        'revivaltheme_hide_meta',
        array(
            'type' => 'select',
            'label' => __('Post info to show','revivaltheme'),
            'section' => 'revivaltheme_section_three',
            'choices' => array(
                'show_all' => __('Show all Post Info','revivaltheme'),
                'hide_all' => __('Hide all Post Info','revivaltheme'),
                'show_author_date' => __('Author + Date','revivaltheme'),
                'show_date_comments' => __('Date + Comments','revivaltheme'),
                'show_author_comments' => __('Author + Comments','revivaltheme'),
                'show_author' => __('Just Author','revivaltheme'),
                'show_date' => __('Just Date','revivaltheme')
            ),
            'priority' => 3
        )
    );

    /*Homepage Layout*/


    $wp_customize->add_setting(
        'revivaltheme_homepage_layout',
        array(
            'default' => 'magazine_4',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_layout'
        )
    );

    $wp_customize->add_control(
        'revivaltheme_homepage_layout',
        array(
            'type' => 'select',
            'label' => __('Homepage Layout','revivaltheme'),
            'section' => 'revivaltheme_section_three',
            'choices' => array(
                'magazine_2'=>__('2 Columns','revivaltheme'),
                'magazine_3'=>__('3 Columns','revivaltheme'),
                'magazine_4'=>__('4 Columns','revivaltheme'),
                'blog'=>__('1 Column','revivaltheme')
            ),
            'priority' => 4
        )
    );

    $wp_customize->add_setting(
        'revivaltheme_homepage_sidebar',
        array(
            'default' => 'fullwidth',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_sidebar'
        )
    );

    $wp_customize->add_control(
        'revivaltheme_homepage_sidebar',
        array(
            'type' => 'select',
            'label' => __('Homepage Sidebar','revivaltheme'),
            'section' => 'revivaltheme_section_three',
            'choices' => array(
                'fullwidth' => __('Fullwidth','revivaltheme'),
                'right_sidebar' => __('Right Sidebar','revivaltheme'),
            ),
            'priority' => 5
        )
    );

    /*Homepage Slideshow*/
    $wp_customize->add_setting(
        'revivaltheme_homepage_slider',
        array(
            'default' => '',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_checkbox',
        )
    );

    $wp_customize->add_control(
        'revivaltheme_homepage_slider',
        array(
            'type' => 'checkbox',
            'label' => __('Enable Homepage Magazine Slider?','revivaltheme'),
            'section' => 'revivaltheme_section_three',
            'priority' => 6
        )
    );


    /* Homepage Carousel */

    $wp_customize->add_setting(
        'revivaltheme_carousel',
        array(
            'default' => '',
            'sanitize_callback' => 'revivaltheme_sanitize_checkbox',
            'type' => 'option'
        )
    );

    $wp_customize->add_control(
        'revivaltheme_carousel',
        array(
            'type' => 'checkbox',
            'label' => __('Enable Homepage Carousel?','revivaltheme'),
            'section' => 'revivaltheme_section_three',
            'priority' => 7
        )
    );

    $wp_customize->add_setting(
        'revivaltheme_carousel_position',
        array(
            'default' => 'bottom',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_carousel'
        )
    );

    $wp_customize->add_control(
        'revivaltheme_carousel_position',
        array(
            'type' => 'select',
            'label' => __('Homepage Carousel Position','revivaltheme'),
            'section' => 'revivaltheme_section_three',
            'choices' => array(
                'top'=>__('Before Header','revivaltheme'),
                'bottom'=>__('After Header','revivaltheme')
            ),
            'priority' => 8
        )
    );

    /*Archive Layout*/

    $wp_customize->add_setting(
        'revivaltheme_archive_layout',
        array(
            'default' => 'magazine_3',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_layout'
        )
    );

    $wp_customize->add_control(
        'revivaltheme_archive_layout',
        array(
            'type' => 'select',
            'label' => __('Archive Layout','revivaltheme'),
            'section' => 'revivaltheme_section_three',
            'choices' => array(
                'magazine_2'=>__('2 Columns','revivaltheme'),
                'magazine_3'=>__('3 Columns','revivaltheme'),
                'magazine_4'=>__('4 Columns','revivaltheme'),
                'blog'=>__('1 Column','revivaltheme')
            ),
            'priority' => 11
        )
    );

    $wp_customize->add_setting(
        'revivaltheme_archive_sidebar',
        array(
            'default' => 'right_sidebar',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_sidebar'
        )
    );

    $wp_customize->add_control(
        'revivaltheme_archive_sidebar',
        array(
            'type' => 'select',
            'label' => __('Archive Sidebar','revivaltheme'),
            'section' => 'revivaltheme_section_three',
            'choices' => array(
                'fullwidth' => __('Fullwidth','revivaltheme'),
                'right_sidebar' => __('Right Sidebar','revivaltheme')
            ),
            'priority' => 12
        )
    );

    /*Author Layout*/

    $wp_customize->add_setting(
        'revivaltheme_author_layout',
        array(
            'default' => 'blog',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_layout'
        )
    );

    $wp_customize->add_control(
        'revivaltheme_author_layout',
        array(
            'type' => 'select',
            'label' => __('Author Page Layout','revivaltheme'),
            'section' => 'revivaltheme_section_three',
            'choices' => array(
                'magazine_2'=>__('2 Columns','revivaltheme'),
                'magazine_3'=>__('3 Columns','revivaltheme'),
                'magazine_4'=>__('4 Columns','revivaltheme'),
                'blog'=>__('1 Column','revivaltheme')
            ),
            'priority' => 13
        )
    );

    $wp_customize->add_setting(
        'revivaltheme_author_sidebar',
        array(
            'default' => 'fullwidth',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_sidebar'
        )
    );

    $wp_customize->add_control(
        'revivaltheme_author_sidebar',
        array(
            'type' => 'select',
            'label' => __('Author Sidebar','revivaltheme'),
            'section' => 'revivaltheme_section_three',
            'choices' => array(
                'fullwidth' => __('Fullwidth','revivaltheme'),
                'right_sidebar' => __('Right Sidebar','revivaltheme')
            ),
            'priority' => 14
        )
    );

    /*Tag Layout*/

    $wp_customize->add_setting(
        'revivaltheme_tag_layout',
        array(
            'default' => 'magazine_2',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_layout'
        )
    );

    $wp_customize->add_control(
        'revivaltheme_tag_layout',
        array(
            'type' => 'select',
            'label' => __('Tag Layout','revivaltheme'),
            'section' => 'revivaltheme_section_three',
            'choices' => array(
                'magazine_2'=>__('2 Columns','revivaltheme'),
                'magazine_3'=>__('3 Columns','revivaltheme'),
                'magazine_4'=>__('4 Columns','revivaltheme'),
                'blog'=>__('1 Column','revivaltheme')
            ),
            'priority' => 15
        )
    );


    $wp_customize->add_setting(
        'revivaltheme_tag_sidebar',
        array(
            'default' => 'left_sidebar',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_sidebar'
        )
    );

    $wp_customize->add_control(
        'revivaltheme_tag_sidebar',
        array(
            'type' => 'select',
            'label' => __('Tag Sidebar','revivaltheme'),
            'section' => 'revivaltheme_section_three',
            'choices' => array(
                'fullwidth' => __('Fullwidth','revivaltheme'),
                'right_sidebar' => __('Right Sidebar','revivaltheme')
            ),
            'priority' => 16
        )
    );

    /*Search Layout*/

    $wp_customize->add_setting(
        'revivaltheme_search_layout',
        array(
            'default' => 'blog',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_layout'
        )
    );

    $wp_customize->add_control(
        'revivaltheme_search_layout',
        array(
            'type' => 'select',
            'label' => __('Search Results Layout','revivaltheme'),
            'section' => 'revivaltheme_section_three',
            'choices' => array(
                'magazine_2'=>__('2 Columns','revivaltheme'),
                'magazine_3'=>__('3 Columns','revivaltheme'),
                'magazine_4'=>__('4 Columns','revivaltheme'),
                'blog'=>__('1 Column','revivaltheme')
            ),
            'priority' => 17
        )
    );

    $wp_customize->add_setting(
        'revivaltheme_search_sidebar',
        array(
            'default' => 'fullwidth',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_sidebar'
        )
    );

    $wp_customize->add_control(
        'revivaltheme_search_sidebar',
        array(
            'type' => 'select',
            'label' => __('Search Sidebar','revivaltheme'),
            'section' => 'revivaltheme_section_three',
            'choices' => array(
                'fullwidth' => __('Fullwidth','revivaltheme'),
                'right_sidebar' => __('Right Sidebar','revivaltheme')
            ),
            'priority' => 18
        )
    );

    /*Facebook URL*/
    $wp_customize->add_setting(
        'revivaltheme_facebook_url',
        array(
            'default' => '',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_text',
        )
    );

    $wp_customize->add_control(
        'revivaltheme_facebook_url',
        array(
            'label' => __('Facebook URL','revivaltheme'),
            'section' => 'revivaltheme_section_four',
            'type' => 'text',
            'priority' => 1
        )
    );

    /*Twitter URL*/
    $wp_customize->add_setting(
        'revivaltheme_twitter_url',
        array(
            'default' => '',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_text',
        )
    );

    $wp_customize->add_control(
        'revivaltheme_twitter_url',
        array(
            'label' => __('Twitter URL','revivaltheme'),
            'section' => 'revivaltheme_section_four',
            'type' => 'text',
            'priority' => 2
        )
    );

    /*Facebook URL*/
    $wp_customize->add_setting(
        'revivaltheme_pinterest_url',
        array(
            'default' => '',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_text',
        )
    );

    $wp_customize->add_control(
        'revivaltheme_pinterest_url',
        array(
            'label' => __('Pinterest URL','revivaltheme'),
            'section' => 'revivaltheme_section_four',
            'type' => 'text',
            'priority' => 3
        )
    );

    /*YouTube URL*/
    $wp_customize->add_setting(
        'revivaltheme_youtube_url',
        array(
            'default' => '',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_text',
        )
    );

    $wp_customize->add_control(
        'revivaltheme_youtube_url',
        array(
            'label' => __('YouTube URL','revivaltheme'),
            'section' => 'revivaltheme_section_four',
            'type' => 'text',
            'priority' => 4
        )
    );

    /*Google+ URL*/
    $wp_customize->add_setting(
        'revivaltheme_google_url',
        array(
            'default' => '',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_text',
        )
    );

    $wp_customize->add_control(
        'revivaltheme_google_url',
        array(
            'label' => __('Google+ URL','revivaltheme'),
            'section' => 'revivaltheme_section_four',
            'type' => 'text',
            'priority' => 5
        )
    );

    /*Linkedin URL*/
    $wp_customize->add_setting(
        'revivaltheme_linkedin_url',
        array(
            'default' => '',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_text',
        )
    );

    $wp_customize->add_control(
        'revivaltheme_linkedin_url',
        array(
            'label' => __('Linkedin URL','revivaltheme'),
            'section' => 'revivaltheme_section_four',
            'type' => 'text',
            'priority' => 6
        )
    );

    /*Github URL*/
    $wp_customize->add_setting(
        'revivaltheme_github_url',
        array(
            'default' => '',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_text',
        )
    );

    $wp_customize->add_control(
        'revivaltheme_github_url',
        array(
            'label' => __('Github URL','revivaltheme'),
            'section' => 'revivaltheme_section_four',
            'type' => 'text',
            'priority' => 7
        )
    );

    /*Vkontakte URL*/
    $wp_customize->add_setting(
        'revivaltheme_vkontakte_url',
        array(
            'default' => '',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_text',
        )
    );

    $wp_customize->add_control(
        'revivaltheme_vkontakte_url',
        array(
            'label' => __('Vkontakte URL','revivaltheme'),
            'section' => 'revivaltheme_section_four',
            'type' => 'text',
            'priority' => 7
        )
    );

    /*Instagram*/

    $wp_customize->add_setting(
        'revivaltheme_instagram_url',
        array(
            'default' => '',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_text',
        )
    );

    $wp_customize->add_control(
        'revivaltheme_instagram_url',
        array(
            'label' => __('Instagram URL','revivaltheme'),
            'section' => 'revivaltheme_section_four',
            'type' => 'text',
            'priority' => 7
        )
    );

    /*E-mail*/
    $wp_customize->add_setting(
        'revivaltheme_email',
        array(
            'default' => '',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_text',
        )
    );

    $wp_customize->add_control(
        'revivaltheme_email',
        array(
            'label' => __('E-mail','revivaltheme'),
            'section' => 'revivaltheme_section_four',
            'type' => 'text',
            'priority' => 8
        )
    );

    /*Skype*/
    $wp_customize->add_setting(
        'revivaltheme_skype',
        array(
            'default' => '',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_text',
        )
    );

    $wp_customize->add_control(
        'revivaltheme_skype',
        array(
            'label' => __('Skype','revivaltheme'),
            'section' => 'revivaltheme_section_four',
            'type' => 'text',
            'priority' => 9
        )
    );

    /*RSS*/
    $wp_customize->add_setting(
        'revivaltheme_rss_url',
        array(
            'default' => '/feed',
            'type' => 'option',
            'sanitize_callback' => 'revivaltheme_sanitize_text',
        )
    );

    $wp_customize->add_control(
        'revivaltheme_rss_url',
        array(
            'label' => __('RSS Feed URL','revivaltheme'),
            'section' => 'revivaltheme_section_four',
            'type' => 'text',
            'priority' => 10
        )
    );

}
add_action( 'customize_register', 'revivaltheme_customizer' );


function revivaltheme_sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}

function revivaltheme_sanitize_checkbox( $input ) {
    if ( $input == 1 ) {
        return 1;
    } else {
        return '';
    }
}

function revivaltheme_sanitize_layout( $input ) {
    $valid = array(
        'magazine_2'=>__('2 Columns','revivaltheme'),
        'magazine_3'=>__('3 Columns','revivaltheme'),
        'magazine_4'=>__('4 Columns','revivaltheme'),
        'blog'=>__('1 Column','revivaltheme')
    );

    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}
function revivaltheme_sanitize_sidebar( $input ) {
    $valid = array(
        'fullwidth' => __('Fullwidth','revivaltheme'),
        'right_sidebar' => __('Right Sidebar','revivaltheme')
    );

    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}
function revivaltheme_sanitize_carousel( $input ) {
    $valid = array(
        'top'=>__('Top','revivaltheme'),
        'bottom'=>__('Bottom','revivaltheme')
    );

    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

/* function revivaltheme_sanitize_header_style( $input ) {
    $valid = array(
        'centered' => __('Centered Logo','revivaltheme'),
        'left' => __('Left Logo & Right Banner','revivaltheme')
    );

    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
} */
function revivaltheme_sanitize_hide_meta( $input ) {
    $valid = array(
        'show_all' => __('Show all Post Info','revivaltheme'),
        'hide_all' => __('Hide all Post Info','revivaltheme'),
        'show_author_date' => __('Author + Date','revivaltheme'),
        'show_date_comments' => __('Date + Comments','revivaltheme'),
        'show_author_comments' => __('Author + Comments','revivaltheme'),
        'show_author' => __('Just Author','revivaltheme'),
        'show_date' => __('Just Date','revivaltheme')
    );

    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

function revivaltheme_sanitize_number( $value ) {
    $value = (int) $value; // Force the value into integer type.
    return ( 0  <= $value ) ? $value : null;
}