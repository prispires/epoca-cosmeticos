<?php

function revivaltheme_date() {
    echo '<div class="entry-date"><time datetime="'. get_the_time('c') .'">'.get_the_date('').'&nbsp;</time></div>';
}
function revivaltheme_date_news() {

    echo '<time datetime="'. get_the_time('c') .'">
	<span>'.get_the_time('').'</span>'.get_the_date('').'</time>';
}

// External Links from the Post Title
function revivaltheme_link_title() {
    global $post;
    $thePostID = $post->ID;
    $post_id = get_post($thePostID);
    $title = $post_id->post_title;
    $link = get_field( 'title_url' );

    echo '<a href="'.$link.'" rel="nofollow" target="_blank" title="'.$title.'">'.$title.'</a>';
}
function revivaltheme_link_subtitle() {
    global $post;
    $thePostID = $post->ID;
    $post_id = get_post($thePostID);
    $title = $post_id->post_title;
    $link = get_field( 'title_url' );

    echo '<a href="'.$link.'" rel="nofollow" target="_blank" class="gray-link" title="'.$title.'"><i class="i-link">&nbsp;</i>'.$link.'</a>';
}

// Thumbnail Title
function revivaltheme_thumbnail_title() {
    global $post;
    $thumbnail_id    = get_post_thumbnail_id($post->ID);
    $thumbnail_image = get_posts(array('p' => $thumbnail_id, 'post_type' => 'attachment'));

    if ($thumbnail_image && isset($thumbnail_image[0])) {
        echo $thumbnail_image[0]->post_title;
    }
}

/* Custom excerpt length
* Based on http://www.wprecipes.com/wordpress-improved-the_excerpt-function
*/

function revivaltheme_excerpt($length) { // Max excerpt length. Length is set in characters
    global $post;
    $text = $post->post_excerpt;
    if ( '' == $text ) {
        $text = get_the_content('');
        $text = apply_filters('the_content', $text);
        $text = str_replace(']]>', ']]>', $text);
    }
    $text = strip_shortcodes($text); // optional, recommended
    $text = strip_tags($text); // use ' $text = strip_tags($text,'<p><a>'); ' if you want to keep some tags

    $text = mb_substr($text,0,$length);
    $excerpt = reverse_strrchr($text, '.', 1);
    if( $excerpt ) {
        echo apply_filters('get_the_excerpt',$excerpt);
        echo ' <a href="'. get_permalink() .'"><i class="i-right-open-big"></i></a>';
    } else {
        echo apply_filters('get_the_excerpt',$text);
        echo ' <a href="'. get_permalink() .'" class="i-right-open-big"></a>';
    }
}

// Returns the portion of haystack which goes until the last occurrence of needle
function reverse_strrchr($haystack, $needle, $trail) {
    return strrpos($haystack, $needle) ? substr($haystack, 0, strrpos($haystack, $needle) + $trail) : false;
}

/******************************************************************************
 * @Author: Richard Chonak
 * @Date:   August 6, 2013
 * @Description: Ensures closure of HTML tags opened in an automatically generated excerpt.
 * Also trims off any trailing incomplete HTML tag at the end of the excerpt.
 * @Tested: Up to WordPress version 3.6
 *
 * @Author: Boutros AbiChedid
 * @Date:   June 20, 2011
 * @Websites: http://bacsoftwareconsulting.com/ ; http://blueoliveonline.com/
 * @Description: Preserves HTML formating to the automatically generated Excerpt.
 * Also Code modifies the default excerpt_length and excerpt_more filters.
 * @Tested: Up to WordPress version 3.1.3
 *******************************************************************************/
function revivaltheme_content($text) {
    $raw_excerpt = $text;
    if ( '' == $text ) {
        //Retrieve the post content.
        $text = get_the_content('');

        //Delete all shortcode tags from the content.
        $text = strip_shortcodes( $text );

        $text = apply_filters('the_content', $text);
        $text = str_replace(']]>', ']]&gt;', $text);

        $allowed_tags = '<small>,<strong>,<em>,<b>,<i>,<p>,<br>,<a>,<blockquote>,<ul>,<li>'; /*** MODIFY THIS. Add the allowed HTML tags separated by a comma.***/
        $twopart_tags = '<small>,<strong>,<em>,<b>,<i>,<p>,<br>,<a>,<blockquote>,<ul>,<li>'; /*** MODIFY THIS. Add the twopart HTML tags separated by a comma.***/
        /* turn tag list into one big search pattern */
        $search_patterns = "/" . str_replace(",","|",str_replace(">", "[^>]*>",$twopart_tags)) . '/';

        $text = strip_tags($text, $allowed_tags);

        $excerpt_word_count = 42; /*** MODIFY THIS. change the excerpt word count to any integer you like.***/
        $excerpt_length = apply_filters('excerpt_length', $excerpt_word_count);

        $excerpt_end = '... <a href="'. get_permalink() .'"><i class="i-right-open-big"></i></a>'; /*** MODIFY THIS. change the excerpt endind to something else.***/
        $excerpt_more = apply_filters('excerpt_more', ' ' . $excerpt_end);

        $words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
        if ( count($words) > $excerpt_length ) {
            array_pop($words);
            $text = implode(' ', $words);
            $text = $text . $excerpt_more;
        } else {
            $text = implode(' ', $words);
        };


        /* if fragment ends in open tag, trim off */
        preg_replace ("/<[^>]*$/", "", $text);

        /* search for tags in excerpt */
        preg_match_all ($search_patterns, $text, $matches);
        /* if any tags found, check for matching pairs */
        $tagstack = array ("");
        $tagsfound = $matches[0];
        while ( count ($tagsfound) > 0) {
            $tagmatch = array_shift($tagsfound);
            /* if it's a closing tag, hooray; but if it's not, then look for the closer */
            if ( !strpos($tagmatch,"</") && !strpos ($tagmatch,"/>") ) {
                preg_match("/\pL+/",$tagmatch, $tagwords);
                $endtag = "</" . $tagwords[0] . ">";
                /* if this tag was not closed, put the closing tag on the stack */
                if (!in_array($endtag, $tagsfound) ) {
                    array_push($tagstack,$endtag);
                };
            };
        };

        /* if any unbalanced tags were found, add the closing tags */
        while (count ($tagstack) > 1) {
            $text = $text . array_pop($tagstack);
        }
    }
    return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
}
remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'revivaltheme_content');

/* Custom Menu
 * Courtesy of Kriesi.at. http://www.kriesi.at/archives/improve-your-wordpress-navigation-menu-output
 * and Reverie http://themefortress.com/
 */

class revivaltheme_walker extends Walker_Nav_Menu {

    var $nav_bar = '';

    function __construct( $nav_args = '' ) {

        $defaults = array(
            'item_type' => 'li',
            'in_top_bar' => false,
        );
        $this->nav_bar = apply_filters( 'req_nav_args', wp_parse_args( $nav_args, $defaults ) );
    }

    function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {

        $id_field = $this->db_fields['id'];
        if ( is_object( $args[0] ) ) {
            $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
        }
        return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }

    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;

        $flyout_toggle = '';
        if ( $args->has_children && $this->nav_bar['item_type'] == 'li' ) {

            if ( $depth == 0 && $this->nav_bar['in_top_bar'] == false ) {

                $classes[] = 'has-flyout';
                $flyout_toggle = '<a href="#" class="flyout-toggle"><span></span></a>';

            } else if ( $this->nav_bar['in_top_bar'] == true ) {

                $classes[] = 'has-dropdown';

                $flyout_toggle = '';}

        }

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

        if ( $depth < 4 ) {
            $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';
        } else {
            $output .= $indent . ( $this->nav_bar['in_top_bar'] == true ? '' : '' ) . '<' . $this->nav_bar['item_type'] . ' id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';
        }

        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        if ( $item->object == 'category' ) {

            $cat = $item->object_id;
            $cat_icon = get_field( 'category_icon', 'category_' . $cat );

            if ( $cat_icon ) {

                $item_output .= $cat_icon;

            }
        } elseif ( ($item->object == 'post') || ($item->object == 'page') ) {
            $item_id = $item->object_id;

            $page_icon = get_field( 'item_icon', $item_id);

            if ( $page_icon ) {

                $item_output .= $page_icon;

            }
        }
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= '</a>';
        $item_output .= $flyout_toggle; // Add possible flyout toggle
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

    function end_el( &$output, $item, $depth = 0, $args = array() ) {

        if ( $depth > 0 ) {
            $output .= "</li>\n";
        } else {
            $output .= "</" . $this->nav_bar['item_type'] . ">\n";
        }
    }

    function start_lvl( &$output, $depth = 0, $args = array() ) {

        if ( $depth == 0 && $this->nav_bar['item_type'] == 'li' ) {
            $indent = str_repeat("\t", 1);
            $output .= $this->nav_bar['in_top_bar'] == true ? "\n$indent<ul class=\"first dropdown\">\n" : "\n$indent<ul class=\"flyout\">\n";
        } else {
            $indent = str_repeat("\t", $depth);
            $output .= $this->nav_bar['in_top_bar'] == true ? "\n$indent<ul class=\"dropdown\">\n" : "\n$indent<ul class=\"level-$depth\">\n";
        }
    }
}

// @todo Get Category Slugs
function revivaltheme_cat_slug() {
    $cats = array();
    foreach((get_the_category()) as $category) {
        $cats[] = $category->slug;
    }
    $slug = implode(' ', $cats);
    return $slug;
}

function revivaltheme_categories() {

    $cats = get_categories('child_of='.get_query_var('cat'));


    $html = '<dl class="sub-nav">';
    $html .= '<dt></dt><dd class="active"><a href="#" data-filter="*" class="selected">All</a></dd>';

    foreach ($cats as $cat) {

        $html .= '<dd><a href="#" data-filter=".'.$cat->category_nicename.'">'.$cat->cat_name.'</a></dd>';
    }

    $html .= '</dl>';

    echo $html;
}

add_filter( 'embed_oembed_html', 'revivaltheme_oembed_filter' );
function revivaltheme_oembed_filter( $code ){
    if( (stripos( $code, 'youtube.com' ) !== FALSE) || (stripos( $code, 'vimeo.com' ) !== FALSE ) && stripos( $code, 'iframe' ) !== FALSE )
        $code = '<div class="flex-video widescreen">'.$code.'</div>';

    return $code;
}