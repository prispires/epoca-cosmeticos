<?php
/**
 * Plugin Name: Revival / Recent News
 * Plugin URI: http://themeous.ocm
 * Description: Adds a widget to display latest posts from the selected category.
 * Version: 1.0
 * Author: Anastasia Chupina
 * Author URI: http://themeous.com
 */

add_action( 'widgets_init', 'revivaltheme_News_Recent_load_widgets' );

function revivaltheme_News_Recent_load_widgets() {
    register_widget( 'Revival_Themeous_News_Recent_Widget' );
}

class Revival_Themeous_News_Recent_Widget extends WP_Widget {

    /**
     * Widget setup.
     */
    function Revival_Themeous_News_Recent_Widget() {
        /* Widget settings. */
        $widget_ops = array( 'classname' => 'recent-widget', 'description' => __('Adds a widget to display latest posts from the selected category.', 'revivaltheme') );

        /* Widget control settings. */
        $control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'revivaltheme-news-recent-widget' );

        /* Create the widget. */
        $this->WP_Widget( 'revivaltheme-news-recent-widget', __('Revival / Recent News', 'revivaltheme'), $widget_ops, $control_ops );
    }

    /**
     * How to display the widget on the screen.
     */
    function widget( $args, $instance ) {
        global $post;
        $post_old = $post; // Save the post object.
        extract( $args );

        /* Our variables from the widget settings. */
        $title = apply_filters('widget_title', $instance['title'] );
        $num = $instance['num'];
        $cat = $instance['cat'];
        $cat_posts = new WP_Query(
            "posts_per_page=" . $instance["num"] .
                "&cat=" . $instance['cat'] .
                "&orderby=date&order=DESC"
        );

        /* Before widget (defined by themes). */
        echo $before_widget;

        // Widget title
        if ( $title )
            echo $before_title . $title . $after_title; ?>


        <?php
        while ( $cat_posts->have_posts() )
        {
            $cat_posts->the_post();
            ?>

            <div class="small-block list-item">

                <?php if ( '' != get_the_post_thumbnail() ) { ?>

                    <div class="media-holder overlay hide-for-portrait">

                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">


                            <?php $square = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'square'); ?>

                            <img src="<?php echo $square[0]; ?>" width="60" height="60" alt="<?php revivaltheme_thumbnail_title();?>">


                        </a>

                    </div>


                <?php } ?>

                <p class="title-text">
                    <a href="<?php the_permalink(); ?>">
                        <?php the_title(); ?>
                    </a>
                </p>

                <?php
                if ( get_field('enable_review') == '1' ) { ?>
                    <span class="side-score"><?php get_template_part( 'inc/rating'); ?></span>
                <?php }  ?>

                <div class="entry-date"><?php revivaltheme_date_news(); ?></div>



            </div>

           <?php wp_reset_query(); ?>

        <?php } echo $after_widget; $post = $post_old; // Restore the post object.

    }
    /**
     * Update the widget settings.
     */
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;

        /* Strip tags for title and name to remove HTML (important for text inputs). */
        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['num'] = strip_tags( $new_instance['num'] );
        $instance['cat'] = $new_instance['cat'];

        return $instance;
    }

    function form( $instance ) {

        /* Set up some default widget settings. */
        $defaults = array( 'title' => __('Recent News', 'revivaltheme'), 'num' => '4', 'cat' => '1' );
        $instance = wp_parse_args( (array) $instance, $defaults ); ?>

        <p>
            <label for="<?php echo $this->get_field_id("title"); ?>">
                <?php _e( 'Title', 'revivaltheme' ); ?>:
                <input class="widefat" id="<?php echo $this->get_field_id("title"); ?>" name="<?php echo $this->get_field_name("title"); ?>" type="text" value="<?php echo esc_attr($instance["title"]); ?>" />
            </label>
        </p>

        <p>
            <label>
                <?php _e( 'Category', 'revivaltheme' ); ?>:
                <?php wp_dropdown_categories( array( 'name' => $this->get_field_name("cat"), 'selected' => $instance['cat'] ) ); ?>
            </label>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id("num"); ?>">
                <?php _e('Number of news to show', 'revivaltheme'); ?>:
                <input style="text-align: center;" id="<?php echo $this->get_field_id("num"); ?>" name="<?php echo $this->get_field_name("num"); ?>" type="text" value="<?php echo absint($instance["num"]); ?>" size='3' />
            </label>
        </p>

    <?php
    }
}

?>