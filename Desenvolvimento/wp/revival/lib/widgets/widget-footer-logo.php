<?php
/**
 * Plugin Name: Revival / Footer Logo
 * Plugin URI: http://themeous.ocm
 * Description: Text logo in the footer
 * Version: 1.0
 * Author: Anastasia Chupina
 * Author URI: http://themeous.com
 */

add_action( 'widgets_init', 'revivaltheme_logo_load_widgets' );

function revivaltheme_logo_load_widgets() {
    register_widget( 'Revival_Themeous_Logo' );
}

class Revival_Themeous_Logo extends WP_Widget {

    /**
     * Widget setup.
     */
    function Revival_Themeous_Logo() {
        /* Widget settings. */
        $widget_ops = array( 'classname' => 'logo-widget', 'description' => __('Adds a widget to display latest posts.', 'revivaltheme') );

        /* Widget control settings. */
        $control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'revival-logo-widget' );

        /* Create the widget. */
        $this->WP_Widget( 'revival-logo-widget', __('Revival / Logo', 'revivaltheme'), $widget_ops, $control_ops );
    }

    /**
     * How to display the widget on the screen.
     */
    function widget( $args, $instance ) {
        global $post;
        $post_old = $post; // Save the post object.

        extract( $args );



        /* Before widget (defined by themes). */
        echo $before_widget; ?>

        <?php $logo_footer = get_option( 'revivaltheme_logo_footer_upload' );
        if ($logo_footer !== '') { ?>

            <div class="logo">
                <a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php echo $logo_footer; ?>"  alt="<?php bloginfo('name'); ?>" /></a>
            </div>
        <?php } else { ?>

            <div class="text-logo animated fadeInUp">

                <h2>
                    <a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a>

                </h2>

                <h4><?php echo get_bloginfo ('description');  ?></h4>

            </div>

        <?php } ?>


        <?php echo $after_widget; $post = $post_old; // Restore the post object.

    }

    function form($instance) { ?>

        <!-- Widget Title: Text Input -->
        <p><?php _e('If you upload a footer logo image in Appearance > Customize, the image will be used, otherwise it will be replaced with a text logo.', 'revivaltheme'); ?>
        </p>

        <p><?php _e('This widget has no options.', 'revivaltheme'); ?></p>



    <?php
    }
}

?>