<?php
/**
 * Plugin Name: Revival / Tabs
 * Plugin URI: http://themeous.ocm
 * Description: Adds a widget to display latest posts, most commented posts and featured posts in tabs.
 * Version: 1.0
 * Author: Anastasia Chupina
 * Author URI: http://themeous.com
 */

add_action( 'widgets_init', 'revivaltheme_tabs_load_widgets' );

function revivaltheme_tabs_load_widgets() {
    register_widget( 'Revival_Themeous_Tabs' );
}

class Revival_Themeous_Tabs extends WP_Widget {

    /**
     * Widget setup.
     */
    function Revival_Themeous_Tabs() {
        /* Widget settings. */
        $widget_ops = array( 'classname' => 'recent-widget', 'description' => __('Adds a widget to display latest posts.', 'revivaltheme') );

        /* Widget control settings. */
        $control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'revival-tabs-widget' );

        /* Create the widget. */
        $this->WP_Widget( 'revival-tabs-widget', __('Revival / Tabs', 'revivaltheme'), $widget_ops, $control_ops );
    }

    /**
     * How to display the widget on the screen.
     */
    function widget( $args, $instance ) {
        global $post;
        $post_old = $post; // Save the post object.
        extract( $args );

        /* Our variables from the widget settings. */
        $num = $instance['num'];
        $tabs_posts_featured = new WP_Query(
            "posts_per_page=" . $instance["num"] .
            "&orderby=orderby=date&order=DESC&meta_key=show_featured&meta_value=1"
        );
        $tabs_posts_popular = new WP_Query(
            "posts_per_page=" . $instance["num"] .
            "&orderby=comment_count&order=DESC"
        );
        $tabs_posts_recent = new WP_Query(
            "posts_per_page=" . $instance["num"] .
            "&orderby=date&order=DESC"
        );

        /* Before widget (defined by themes). */
        echo $before_widget; ?>

        <dl class="tabs" data-tab><dt></dt>
            <dd class="active"><a href="#panel_<?php echo $id; ?>-1" title="<?php _e('Featured', 'revivaltheme'); ?>"><i class="i-clock"></i></a></dd>
            <dd><a href="#panel_<?php echo $id; ?>-2" title="<?php _e('Popular', 'revivaltheme'); ?>"><i class="i-comment-alt"></i></a></dd>
            <dd><a href="#panel_<?php echo $id; ?>-3" title="<?php _e('New', 'revivaltheme'); ?>"><i class="i-heart-empty"></i></a></dd>
        </dl>
        <div class="tabs-content">
        <div class="content active" id="panel_<?php echo $id; ?>-1">


            <?php
            while ( $tabs_posts_recent->have_posts() )
            {
                $tabs_posts_recent->the_post();
                ?>

                <div class="small-block list-item">

                    <?php if ( '' != get_the_post_thumbnail() ) { ?>

                        <div class="media-holder overlay hide-for-portrait">

                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">


                                <?php $square = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'square'); ?>

                                <img src="<?php echo $square[0]; ?>" width="60" height="60" alt="<?php revivaltheme_thumbnail_title();?>">


                            </a>

                        </div>


                    <?php } ?>

                    <p class="title-text">

                        <a href="<?php the_permalink(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </p>

                    <?php
                    if ( get_field('enable_review') == '1' ) { ?>
                        <span class="side-score"><?php get_template_part( 'inc/rating'); ?></span>
                    <?php }  ?>

                    <?php revivaltheme_date(); ?>

                </div>

            <?php wp_reset_query(); } ?>


            </div>
            <div class="content" id="panel_<?php echo $id; ?>-2">
                <?php
                while ( $tabs_posts_popular->have_posts() )
                {
                    $tabs_posts_popular->the_post();
                    ?>

                    <div class="small-block list-item">

                        <?php if ( '' != get_the_post_thumbnail() ) { ?>

                            <div class="media-holder overlay hide-for-portrait">

                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">


                                    <?php $square = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'square'); ?>

                                    <img src="<?php echo $square[0]; ?>" width="60" height="60" alt="<?php revivaltheme_thumbnail_title();?>">


                                </a>

                            </div>


                        <?php } ?>

                        <p class="title-text">

                            <a href="<?php the_permalink(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </p>

                        <?php
                        if ( get_field('enable_review') == '1' ) { ?>
                            <span class="side-score"><?php get_template_part( 'inc/rating'); ?></span>
                        <?php }  ?>

                        <?php revivaltheme_date(); ?>

                    </div>

                    <?php wp_reset_query(); } ?>
            </div>
            <div class="content" id="panel_<?php echo $id; ?>-3">
                <?php
                while ( $tabs_posts_featured->have_posts() )
                {
                $tabs_posts_featured->the_post();
                ?>

                <div class="small-block list-item">

                    <?php if ( '' != get_the_post_thumbnail() ) { ?>

                    <div class="media-holder overlay hide-for-portrait">

                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">


                            <?php $square = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'square'); ?>

                            <img src="<?php echo $square[0]; ?>" width="60" height="60" alt="<?php revivaltheme_thumbnail_title();?>">


                        </a>

                    </div>


                    <?php } ?>

                    <p class="title-text">

                        <a href="<?php the_permalink(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </p>

                    <?php
                    if ( get_field('enable_review') == '1' ) { ?>
                    <span class="side-score"><?php get_template_part( 'inc/rating'); ?></span>
                    <?php }  ?>

                <?php revivaltheme_date(); ?>

                </div>

                    <?php wp_reset_query(); } ?>
            </div>

            </div>


        <?php echo $after_widget; $post = $post_old; // Restore the post object.

    }
    /**
     * Update the widget settings.
     */
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;

        /* Strip tags for title and name to remove HTML (important for text inputs). */
        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['num'] = strip_tags( $new_instance['num'] );

        return $instance;
    }

    function form( $instance ) {

        /* Set up some default widget settings. */
        $defaults = array( 'num' => '5' );
        $instance = wp_parse_args( (array) $instance, $defaults ); ?>

        <!-- Widget Title: Text Input -->
       
        <p>
            <label for="<?php echo $this->get_field_id( 'num' ); ?>"><?php _e('Number of posts to show', 'revivaltheme'); ?></label>
            <input id="<?php echo $this->get_field_id( 'num' ); ?>" name="<?php echo $this->get_field_name( 'num' ); ?>" value="<?php echo $instance['num']; ?>" style="width:100%;" />
        </p>

    <?php
    }
}

?>