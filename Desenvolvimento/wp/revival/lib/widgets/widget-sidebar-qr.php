<?php
/**
 * Plugin Name: Revival / QR Link
 * Plugin URI: http://themeous.ocm
 * Description: QR link to the current post.
 * Version: 1.0
 * Author: Anastasia Chupina
 * Author URI: http://themeous.com
 */

add_action( 'widgets_init', 'revivaltheme_qr_load_widgets' );

function revivaltheme_qr_load_widgets() {
    register_widget( 'Revival_Themeous_QR' );
}

class Revival_Themeous_QR extends WP_Widget {

    /**
     * Widget setup.
     */
    function Revival_Themeous_QR() {
        /* Widget settings. */
        $widget_ops = array( 'classname' => 'qr-widget', 'description' => __('Adds a widget to display latest posts.', 'revivaltheme') );

        /* Widget control settings. */
        $control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'revival-qr-widget' );

        /* Create the widget. */
        $this->WP_Widget( 'revival-qr-widget', __('Revival / QR Link', 'revivaltheme'), $widget_ops, $control_ops );
    }

    /**
     * How to display the widget on the screen.
     */
    function widget( $args, $instance ) {
        global $post;
        $post_old = $post; // Save the post object.
        extract( $args );

        $title = apply_filters('widget_title', $instance['title'] );
        $url = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];


        /* Before widget (defined by themes). */
        echo $before_widget;

        // Widget title
        if ( $title )
            echo $before_title . $title . $after_title; ?>


            <img src="http://api.qrserver.com/v1/create-qr-code/?size=190x190&amp;data=<?php echo $url; ?>" title="<?php echo $url; ?>" alt="QR: <?php echo $url; ?>" />
            <p><br /><?php _e('Use this image to get the QR link to this page', 'revivaltheme'); ?></p>


        <?php echo $after_widget; $post = $post_old; // Restore the post object.

    }

    /**
     * Update the widget settings.
     */
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;

        /* Strip tags for title and name to remove HTML (important for text inputs). */
        $instance['title'] = strip_tags( $new_instance['title'] );

        return $instance;
    }

    function form($instance) {

        /* Set up some default widget settings. */
        $defaults = array( 'title' => __('QR Link', 'revivaltheme'));
        $instance = wp_parse_args( (array) $instance, $defaults ); ?>

        <p>
            <label for="<?php echo $this->get_field_id("title"); ?>">
                <?php _e( 'Title', 'revivaltheme' ); ?>:
                <input class="widefat" id="<?php echo $this->get_field_id("title"); ?>" name="<?php echo $this->get_field_name("title"); ?>" type="text" value="<?php echo esc_attr($instance["title"]); ?>" />
            </label>
        </p>

        <p>
            <?php _e('This widget has no other options.', 'revivaltheme'); ?>
        </p>



    <?php
    }
}

?>