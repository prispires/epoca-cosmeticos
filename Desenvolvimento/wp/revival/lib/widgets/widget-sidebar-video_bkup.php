<?php
/**
 * Plugin Name: Revival / Video
 * Plugin URI: http://themeous.ocm
 * Description: Widget that embeds a video from any supported video sites. Code based on: https://github.com/zyml/project-ar2/blob/master/library/widgets.php
 * Version: 1.0
 * Author: Anastasia Chupina
 * Author URI: http://themeous.com
 */

add_action( 'widgets_init', 'revivaltheme_video_load_widgets' );

function revivaltheme_video_load_widgets() {
    register_widget( 'Revival_Themeous_Video_Widget' );
}

class Revival_Themeous_Video_Widget extends WP_Widget {

    public function __construct() {

        $widget_args = array (
            'classname'		=> 'themeous_video_widget',
            'description'	=> __( 'Widget that embeds a video from any supported video sites.', 'revivaltheme' ),
        );
        $control_args = array ( 'width' => 300, );

        $this->WP_Widget( 'themeous_video_widget', sprintf( __( 'Revival / Video', 'revivaltheme' ), wp_get_theme()->get( 'Name' ) ), $widget_args, $control_args );
    }

    public function widget( $args, $instance ) {

        global $wp_embed;

        extract( $args, EXTR_SKIP );

        if ( $instance[ 'video' ] == '' ) return false;

        $title = apply_filters( 'widget_title', $instance[ 'title' ] );

        echo $before_widget;
        echo $before_title . $title . $after_title;

        echo $wp_embed->run_shortcode( '[embed]' . $instance[ 'video' ] . '[/embed]' );

        echo $after_widget;

    }

    public function update( $new_instance, $old_instance ) {

        $instance = $old_instance;

        $instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );
        $instance[ 'video' ] = esc_url( $new_instance[ 'video' ] );

        return $instance;

    }

    public function form( $instance ) {

        $instance = wp_parse_args( ( array )$instance, array (
            'title' => __( 'Video', 'revivaltheme' ),
            'video'	=> '',
        ) );

        ?>
    <p><label for="<?php echo $this->get_field_id( 'title' ) ?>"><?php _e( 'Title:', 'revivaltheme' ) ?></label><br />
        <input type="text" id="<?php echo $this->get_field_id( 'title' ) ?>" name="<?php echo $this->get_field_name( 'title' ) ?>" size="33" value="<?php echo strip_tags( $instance[ 'title' ] ) ?>" />
    </p>

    <p><label for="<?php echo $this->get_field_id( 'video' ) ?>"><?php _e( 'Video URL:', 'revivaltheme' ) ?></label><br />
        <input type="text" id="<?php echo $this->get_field_id( 'video' ) ?>" name="<?php echo $this->get_field_name( 'video' ) ?>" size="33" value="<?php echo esc_url( $instance[ 'video' ] ) ?>" />
    </p>

    <p><a href="http://codex.wordpress.org/Embeds"><?php _e( 'Supported Video Sites', 'revivaltheme' ) ?></a></p>

    <?php

    }

}