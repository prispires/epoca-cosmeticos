<?php

// start all the functions
add_action('after_setup_theme','revivaltheme_startup');

function revivaltheme_startup() {

    add_action('wp_enqueue_scripts', 'revivaltheme_load_fonts');
    add_action('wp_enqueue_scripts', 'revivaltheme_scripts', 999);
    add_action( 'wp_enqueue_scripts', 'revivaltheme_css_style' );
    add_action( 'wp_enqueue_scripts', 'revivaltheme_custom_css_style' );
    add_action( 'admin_enqueue_scripts', 'revivaltheme_theme_admin_css_style' );
    // ie conditional wrapper
    add_filter( 'style_loader_tag', 'revivaltheme_ie_conditional', 10, 2 );
    // clean up gallery output in wp
    add_filter('gallery_style', 'revivaltheme_gallery_style');

}

/**********************
Enqueue CSS and Scripts
**********************/

function revivaltheme_load_fonts() {
    $protocol = is_ssl() ? 'https' : 'http';
    $headings_font = get_option( 'revivaltheme_google_font_headings' );
    $body_font = get_option( 'revivaltheme_google_font_body' );

    // enqueue base scripts and styles
    if ($headings_font !== '') {

    wp_register_style('google-fonts-headings', "$protocol://fonts.googleapis.com/css?family=$headings_font:400");

    } else {

    wp_register_style('google-fonts-headings', "$protocol://fonts.googleapis.com/css?family=Cinzel:400");

    }
    if ($body_font !== '') {

    wp_register_style('google-fonts-body', "$protocol://fonts.googleapis.com/css?family=$body_font:400,400italic,500,500italic");

    } else {

    wp_register_style('google-fonts-body', "$protocol://fonts.googleapis.com/css?family=Raleway:400,400italic,500,500italic");

    }

    wp_enqueue_style( 'google-fonts-headings');
    wp_enqueue_style( 'google-fonts-body');
}

// loading modernizr and jquery, and reply script
function revivaltheme_scripts() {
  if (!is_admin()) {

    // comment reply script for threaded comments
    if( get_option( 'thread_comments' ) )  { wp_enqueue_script( 'comment-reply' ); }

      // adding scripts files in footer

      wp_register_script( 'foundation-js', get_template_directory_uri() . '/js/foundation.min.js', array( 'jquery' ), '', true );

      wp_register_script( 'plugins-js', get_template_directory_uri() . '/js/plugins.min.js', array( 'jquery', 'jquery-ui-core'), '', true );

    wp_register_script( 'settings-js', get_template_directory_uri() . '/js/settings.js', array( 'jquery' ), '', true );

    global $is_IE;
    if ($is_IE) {
       wp_register_script ( 'html5shiv', "http://html5shiv.googlecode.com/svn/trunk/html5.js" , false, true);
       wp_register_script ( 'ie-js', get_template_directory_uri() . '/js/ie.min.js', false, true);
    }

    // enqueue scripts
    wp_enqueue_script( 'jquery');
    wp_enqueue_script( 'foundation-js' );
    wp_enqueue_script( 'plugins-js' );
    wp_enqueue_script( 'settings-js' );
    wp_enqueue_script( 'html5shiv' );
    wp_enqueue_script( 'ie-js' );

  }
}
function revivaltheme_css_style()
{
    if (!is_admin()) {
    // minimized font stylesheet
    wp_register_style( 'fontello-stylesheet', get_template_directory_uri() . '/css/fontello.min.css', array(), '', 'screen' );

    // foundation stylesheet
    wp_register_style( 'foundation-stylesheet', get_template_directory_uri() . '/css/basic.min.css', array(), '',  'all' );

    // main style under root directory
    wp_register_style( 'stylesheet', get_stylesheet_directory_uri() . '/style.css', array(), '', 'all' );

    // responsive styles
    wp_register_style( 'responsive-stylesheet', get_template_directory_uri() . '/css/responsive.min.css', array(), '', 'screen');

    wp_enqueue_style( 'fontello-stylesheet' );
    wp_enqueue_style( 'foundation-stylesheet' );
    wp_enqueue_style( 'stylesheet' );
    wp_enqueue_style( 'responsive-stylesheet' );
    }

}

function revivaltheme_theme_admin_css_style() {

        wp_register_style( 'fontello-stylesheet', get_template_directory_uri() . '/css/fontello.css', array(), '', 'screen' );

        wp_register_style( 'theme-admin-stylesheet', get_template_directory_uri() . '/css/theme-admin.css', array(), '', 'screen' );

        wp_enqueue_style( 'theme-admin-stylesheet' );
        wp_enqueue_style( 'fontello-stylesheet' );

}

function revivaltheme_custom_css_style() {

    wp_enqueue_style(
        'custom-color-1',
        get_template_directory_uri() . '/css/custom_color_1.css'
    );

    wp_enqueue_style(
        'custom-color-2',
        get_template_directory_uri() . '/css/custom_color_2.css'
    );

    wp_enqueue_style(
        'custom-headings-font',
        get_template_directory_uri() . '/css/custom_headings_font.css'
    );

    wp_enqueue_style(
        'custom-body-font',
        get_template_directory_uri() . '/css/custom_body_font.css'
    );

    wp_enqueue_style(
        'custom-header-bg',
        get_template_directory_uri() . '/css/header_bg.css'
    );

    $color_1 = get_option( 'revivaltheme_custom_color_1' );
    $color_2 = get_option( 'revivaltheme_custom_color_2' );
    $headings_font = get_option( 'revivaltheme_google_font_headings' );
    $body_font = get_option( 'revivaltheme_google_font_body' );
    $header_bg = get_option( 'revivaltheme_header_bg' );

    $custom_headings_font="
    h1, h2, h3, h4, h5, h6, .breadcrumbs, .widget_calendar thead>tr>th, .rating_label, .wp-caption-text, .side-score, .nivo-lightbox-title, .total-score, input#submit, .wpcf7-submit, .button, .postfix, .entry-content::first-letter, .rating-criteria-score, .page-numbers { font-family: '$headings_font', Georgia, Times, 'Times New Roman', serif !important; }";

    $custom_body_font = "body, .is-hover h5, .text-logo h4, .rssSummary { font-family: '$body_font',  Arial, Verdana, 'Helvetica Neue', Helvetica, sans-serif !important; }";

    $custom_color_1 = ".dropdown-wrapper, .widget_calendar thead>tr>th, .entry-header .total-score, .post-image, .color, .masonry .color .icon-single, .nav > li > .sub-menu, .grid-item .item-icon, .colorful, table#wp-calendar td#today, .owl-theme .owl-dots .owl-dot.active span, .fn:hover, .colorful .top-bar-section .dropdown li a, .colorful .entry-date, .sidebar-icon, .postfix, .postfix:hover, .postfix:focus, .author-meta .fn, .no-image, .about-author.colorful { background-color: $color_1; }

    a, h1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover, h5 a:hover, h6 a:hover, .widget ul li a:hover, .filter:hover, .blog-grid .format a:hover, h2.section-title.color a:hover, .text-bg h3 a:hover, ul#recentcomments li a.url:hover, p.title-text a:hover, .dark table#wp-calendar td a:hover, .side-nav li a:hover, .rating-bg .total-score, ul.newsticker a i, #footer-bottom ul li a:hover, .tabs dd.active a, .dark .tabs dd.active a, .page-numbers li.current a, .page-numbers li.current a:hover, .page-numbers li.current a:focus, .page-numbers li span:hover a,.page-numbers li span a:focus, .page-numbers li span.current, .entry-date a:hover, .dark .tagcloud a:hover, .star-rating span, a:hover, a:focus, .grid-item h2 a:hover, .dark p.title-text a:hover, .dark table#wp-calendar td a, .dark a.gray-link:hover, .dark h4.entry-title a:hover, ul.off-canvas-list li a:hover, .dark ul.off-canvas-list li ul li a:hover, .comment .comment-date a:hover, input#submit:hover, input#submit:focus, .wpcf7-submit:hover, .wpcf7-submit:focus, .button:hover, .button:focus, .white .tagcloud a:hover { color: $color_1; }

   .top-bar-section ul li.has-dropdown .first.dropdown:before {border-color: transparent transparent $color_1 transparent;}

   blockquote, table#wp-calendar thead, .progress_bar .rating_single_bar .rating_bar,  .tagcloud a:hover, input#submit:hover, input#submit:focus, .wpcf7-submit:hover, .wpcf7-submit:focus, .button:hover, .button:focus, .postfix:focus, .postfix:hover { border-color: $color_1; }";

    $custom_color_2 = ".dark, .colorful.dark, .dark .entry-date, .dark table tr.even, .dark table tr.alt, .dark table tr:nth-of-type(even), .dark table, .dark .pad, .dark td#prev, .dark td#next, #primary.colorful.dark.expanded { background-color: $color_2; }
    ";

    $custom_header_bg = "#header { background-image: url($header_bg); }";

    if ($color_1 !== '#e54e4f') {
        wp_add_inline_style( 'custom-color-1', $custom_color_1 );
    }
    if ($color_2 !== '#010101') {
        wp_add_inline_style( 'custom-color-2', $custom_color_2 );
    }

    if ($headings_font !== '') {
    wp_add_inline_style( 'custom-headings-font', $custom_headings_font );
    }
    if ($body_font !== '') {
        wp_add_inline_style( 'custom-body-font', $custom_body_font );
    }
    if ($header_bg !== '') {
        wp_add_inline_style( 'custom-header-bg', $custom_header_bg );
    }
}
    
// adding the conditional wrapper around ie stylesheet
// source: http://code.garyjones.co.uk/ie-conditional-style-sheets-wordpress/
function revivaltheme_ie_conditional( $tag, $handle ) {
	if ( 'theme-ie-only' == $handle )
		$tag = '<!--[if lt IE 9]>' . "\n" . $tag . '<![endif]-->' . "\n";
	return $tag;
}

// remove injected CSS from gallery
function revivaltheme_gallery_style($css) {
    return preg_replace("!<style type='text/css'>(.*?)</style>!s", '', $css);
}