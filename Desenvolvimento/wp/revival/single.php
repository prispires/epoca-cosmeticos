<?php get_header(); ?>
<?php $post_sidebar=get_field( 'post_sidebar' ); ?>

    <section id="content" class="row collapse animated fadeIn" role="main">

        <div class="large-12 columns"><!-- Row for main content area -->

            <div class="single-page main">


                <?php while (have_posts()) : the_post(); ?>


                    <article <?php post_class() ?> id="post-<?php the_ID(); ?>">

                        <?php dynamic_sidebar('before-post-horizontal'); ?>


                        <?php  if ( $post_sidebar == 'right_sidebar' || $post_sidebar == 'left_sidebar' ) { ?>

                            <?php get_template_part('content', 'sidebar'); ?>

                        <?php  } else { ?>

                            <?php get_template_part('content'); ?>

                        <?php  }  ?>


                        <?php dynamic_sidebar('after-post-horizontal'); ?>


                    </article>

                    <div id="comments" class="gray">
                        <div class="row collapse">
                            <div class="large-7 large-centered columns">
                                <?php comments_template( '', true ); ?>
                            </div>
                        </div>
                    </div>


                <?php endwhile; // End the loop ?>



            </div>




        </div><!--end wide column-->


    </section>

<?php get_footer(); ?>