<?php $cat = get_query_var('cat');
$category_sidebar = get_field( 'cat_sidebar', 'category_'.$cat.'' );
$category_carousel = get_field( 'cat_carousel', 'category_'.$cat.'' );
$category_layout = get_field( 'cat_layout', 'category_'.$cat.'' );
?>
<?php get_header(); ?>

<?php if ( $category_carousel ){ ?>

    <?php get_template_part( 'layout/loop', 'carousel'); ?>

<?php  } ?>

    <div class="row collapse white">

        <?php if (category_description( $cat ) != '') { ?>

            <div class="large-4 medium-5 small-12 columns">
                <h1 class="archive-title animated fadeInRight"><?php single_cat_title(); ?></h1>
            </div>


            <div class="large-8 medium-7 small-12 columns">
                <div class="cat-description"><?php echo category_description(); ?></div>
            </div>

        <?php } else { ?>

            <div class="large-12 columns">
                <h1 class="archive-title"><?php single_cat_title(); ?></h1>
            </div>

        <?php } ?>

    </div>

<section class="gray animated fadeIn" role="main">

    <div class="row collapse">

    <?php if ($category_sidebar == 'fullwidth') { ?>

       <div class="<?php if ($category_layout == 'blog') { ?>large-10 large-centered<?php } else { ?>large-12<?php } ?> columns">

    <?php } else { ?>

        <div class="large-9 medium-12 small-12 columns">

    <?php } ?>


        <?php if ( have_posts() ) : ?>


        <?php // Layout
        if ($category_layout == 'magazine_2') { get_template_part('layout/loop', 'magazine_2'); }
        elseif ($category_layout == 'magazine_3') { get_template_part('layout/loop', 'magazine_3'); }
        elseif ($category_layout == 'magazine_4') { get_template_part('layout/loop', 'magazine_4'); }
        else { get_template_part('layout/loop', 'blog'); } ?>

        <?php else : ?>

            <?php get_template_part( 'content', 'none' ); ?>

        <?php endif; // end have_posts() check ?>


    </div><!--end main column-->


    <?php if ( $category_sidebar == 'fullwidth') { } else { ?>

        <?php get_sidebar(); ?>

    <?php } ?>


    </div><!--End Row-->

</section>

<?php get_footer(); ?>