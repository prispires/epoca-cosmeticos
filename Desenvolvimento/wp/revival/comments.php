<?php function revivaltheme_comments($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?>>

		<article id="comment-<?php comment_ID(); ?>" class="row collapse">

            <div class="large-12 columns">

                <header class="comment-author left hide-for-small">
                    <?php echo get_avatar($comment,$size='56'); ?>
                </header>


                <section class="comment left">

                    <?php if ($comment->comment_approved == '0') : ?>
                        <div class="notice">
                            <p class="bottom"><?php _e('Your comment is awaiting moderation.', 'revivaltheme') ?></p>
                        </div>
                    <?php endif; ?>

                    <div class="author-meta">
                        <?php printf(__('<cite class="fn">%s</cite>', 'revivaltheme'), get_comment_author_link()) ?>
                        <time datetime="<?php echo comment_date('c') ?>" class="comment-date"><i class="i-clock">&nbsp;</i><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s', 'revivaltheme'), get_comment_date(''),  get_comment_time()) ?></a></time>
                        <?php edit_comment_link(__('Edit', 'revivaltheme'), '', '') ?>
                        <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                    </div>

                    <?php comment_text() ?>

                </section>

            </div>

		</article>
<?php } ?>

<?php
// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die (__('Please do not load this page directly. Thanks!', 'revivaltheme'));

	if ( post_password_required() ) { ?>
		<div class="notice">
			<p class="bottom"><?php _e('This post is password protected. Enter the password to view comments.', 'revivaltheme'); ?></p>
		</div>
	<?php
		return;
	}
?>
<?php // You can start editing here. Customize the respond form below ?>
<?php if ( have_comments() ) : ?>
		<h3><?php comments_number(__('No Responses to', 'revivaltheme'), __('One Response to', 'revivaltheme'), __('% Responses to', 'revivaltheme') ); ?> &#8220;<?php the_title(); ?>&#8221;</h3>
		<ol class="commentlist">
		<?php wp_list_comments('type=comment&callback=revivaltheme_comments'); ?>

		</ol>
		<footer>
			<nav id="comments-nav">
				<div class="comments-previous"><?php previous_comments_link( __( '&larr; Older comments', 'revivaltheme' ) ); ?></div>
				<div class="comments-next"><?php next_comments_link( __( 'Newer comments &rarr;', 'revivaltheme' ) ); ?></div>
			</nav>
		</footer>
<?php else : // this is displayed if there are no comments so far ?>
	<?php if ( comments_open() ) : ?>
	<?php else : // comments are closed ?>
		<div class="notice">
			<p class="bottom"><?php _e('Comments are closed.', 'revivaltheme') ?></p>
		</div>
	<?php endif; ?>
<?php endif; ?>
<?php if ( comments_open() ) : ?>

    <?php comment_form(array('comment_notes_after' => '')) ?>

<?php endif; // if you delete this the sky will fall on your head ?>