//16-06-2014 --- #11325
$(document).ready(function() {

    // headerflutuante		
    $(window).bind("scroll", function() {
        var $this = $(this);
        if ($this.scrollTop() > 18) {
            $(".header_flutuante").stop(true, true).fadeIn(500);
            $(".ui-autocomplete").addClass("fixo");
        } else {
            $(".header_flutuante").stop(true, true).fadeOut(500);
            $(".ui-autocomplete").removeClass("fixo");
        }
    });


    $(".header_flutuante ul.menu_dept li.item").mouseenter(function() {
        $(".header_flutuante ul.menu_dept li ul.sub_dept").stop(true, true).slideDown(300);
    });

    $(".header_flutuante ul.menu_dept li.item").mouseleave(function() {
        $(".header_flutuante ul.menu_dept li ul.sub_dept").stop(true, true).slideUp(300);
    });

    $(".header_flutuante ul.menu_dept li ul.sub_dept").mouseenter(function() {
        $(".header_flutuante ul.menu_dept li.item").addClass("ativo");
    });

    $(".header_flutuante ul.menu_dept li ul.sub_dept").mouseleave(function() {
        $(".header_flutuante ul.menu_dept li.item").removeClass("ativo");
    });

    // BANNER TOPO
    // clicando no botao a.close ele fecha o bannerTop
    $(".topbanner a.close").click(function() {
        $(".topbanner").slideUp(300);
    });

    // MENU SUPERIOR
    function menuEnter() {
        $(this).find("ul.sub").stop(true, true).slideDown(300);
    }
    ;

    function menuOut() {
        $(this).find("ul.sub").stop(true, true).slideUp(300);
    }
    ;

    $(".menu100 .menu").hoverIntent({
        selector: 'h2.item_menu, h2.m_marcas',
        over: menuEnter,
        out: menuOut
    });

    $(".menu100 .menu h2.item_menu ul.sub").mouseenter(function() {
        $(this).parent("li.item_menu").stop(true, true).addClass("ativo");
    });

    $(".menu100 .menu h2.item_menu ul.sub").mouseleave(function() {
        $(this).parent("li.item_menu ").stop(true, true).removeClass("ativo");
    });

    // MENU SUPERIOR HAD TAG SEO
        
    function menuEnter2() {
        $(this).find("ul.sub").stop(true, true).slideDown(300);
    }
    ;

    function menuOut() {
        $(this).find("ul.sub").stop(true, true).slideUp(300);
    }
    ;

    $(".menu100 .menu").hoverIntent({
        selector: 'h2.item_menu, h2.m_marcas',
        over: menuEnter,
        out: menuOut
    });

    $(".menu100 .menu h2.item_menu ul.sub").mouseenter(function() {
        $(this).parent("h2.item_menu").stop(true, true).addClass("ativo");
    });

    $(".menu100 .menu h2.item_menu ul.sub").mouseleave(function() {
        $(this).parent("h2.item_menu ").stop(true, true).removeClass("ativo");
    });
    


    // PREENCHE O CAMPO OCULTO DO CADASTRO DE NEWS
    $(".menu_sec_cadastro #newsletterClientName").val("Cliente");

    //fim js Geral

    if ($("body").hasClass("home")) {

        //   	$('html, body').animate({
        //        scrollTop: $(".topbanner").offset().top +=1
        // }, 1000);

        // BANNER HOME
        $("li.royalSlide").each(function() {
            if ($(this).find("img").length == 0)
                $(this).remove();

        });

        $('#banner-rotator').royalSlider({
            slideshowEnabled: true,
            slideshowDelay: 4000,
            slideTransitionSpeed: 300,
            slideTransitionType: "fade",
            imageScaleMode: "fill",
            hideArrowOnLastSlide: true,
            slideSpacing: 0
        });

        //carrossel
        if ($(".cont_masc li").length > 4) {
            $(".cont_masc ul").jcarousel({
                scroll: 3
            });
        }
        if ($(".cont_femi li").length > 4) {
            $(".cont_femi ul").jcarousel({
                scroll: 3
            });
        }
        if ($(".cont_novidades li").length > 4) {
            $(".cont_novidades ul").jcarousel({
                scroll: 3
            });
        } 
        
        //carrocel marcas
        $(".marcas ul").jcarousel({
            auto: 4
        });

        // $(".jcarousel-prev, .jcarousel-next").click(function(){
        // 	setTimeout(function(){
        // 		$("html, body").animate({ scrollTop: ($(window).scrollTop()+1) }, 100);
        // 	},1000);

        // });

    }//fim Home

    $(window).bind("scroll", function() {
        var $this = $(this);
        if ($this.scrollTop() > 180) {
            $("a.news_fixa").stop(true, true).animate({right: "0px"}, 200);
        } else {
            $("a.news_fixa").stop(true, true).animate({right: "-70px"}, 200);
            $('.news_fixed').animate({right: "-300px"}, 200);
        }
    });

    $("a.news_fixa").click(function() {
        $(this).animate({right: "-70px"}, 200);
        $('.news_fixed').animate({right: "0px"}, 200);
        return false
    });

    $('.news_fixed').mouseleave(function() {
        $(this).stop(true, true).animate({right: "-300px"}, 200);
        $("a.news_fixa").stop(true, true).animate({right: "0px"}, 200);
    });






    // categoria
    if ($("body.cat_res").length > 0) {
        // scroll na sidebar

        // verifica se tem mais de 10 itens, e adiciona uma class pra diferenciar das outras
        $(".cat_res .sidebar fieldset div").each(function() {
            if ($(this).find("label").length > 10) {
                $(this).addClass("scroll");
            }
        });

        if ($("body.cat_res").length > 0) {
            $(".cat_res .sidebar fieldset div.scroll").mCustomScrollbar();
        }

        //exibir prateleira em lista
        $('.grid-select').addClass('selecionado');
        $('.grid-select').click(function() {
            $('.list-select').removeClass('selecionado')
            $(this).addClass('selecionado');
            $('.main .prateleira').removeClass('plista');
        });
        $('.list-select').click(function() {
            $('.grid-select').removeClass('selecionado')
            $(this).addClass('selecionado');
            $('.main .prateleira').addClass('plista');
        });

        //adiciona classe ao body se resultado de busca nao encontrar resultado	    
        if ($(".busca-vazio, .didyoumean").length > 0) {
            $(".gridListControl").hide();
        }

        // verifica se Ã© pagina de marcas
        if ($(".cat_res .marca_perso .banner").length > 0) {
            $("body").addClass("pag_marca");
        }
    }//fim Categoria

    if ($("body.lista").length > 0) {
        $("body.lista .basic li.visibility span label[for='giftlistispublic-no']").html("Somente o criador da lista");
        $("body.lista .basic li.visibility span label[for='giftlistispublic-yes']").html("Qualquer pessoa");
    } // fim Lista

    // Pagina de marcas
    var textoMarcasBread = '<li class="last"><a href="#">Marcas</a></li>';
    $('body.nossasmarcas #content .bread-crumb ul').append(textoMarcasBread);

    //Legado
    // Minha Conta - Modais				
    var statusAlterarPerfil = false;
    $('body.account #content_abas a#edit-data-link').click(function() {
        if (statusAlterarPerfil == false) {
            $('#editar-perfil').fadeIn('fast');
            statusAlterarPerfil = true;
        }
        else if (statusAlterarPerfil == true) {
            $('#editar-perfil').fadeOut('fast');
            statusAlterarPerfil = false;
        }
    });
    $('#editar-perfil .close').click(function() {
        $('#editar-perfil').fadeOut('fast');
        statusAlterarPerfil = false;
    });

    var statusEndRemove = false;
    $('body.account p.edit-address-link a.delete').click(function() {
        if (statusEndRemove == false) {
            $('#address-remove').fadeIn('fast');
            statusEndRemove = true;
        }
        else if (statusEndRemove == true) {
            $('#address-remove').fadeOut('fast');
            statusEndRemove = false;
        }
    });
    $('#address-remove .close').click(function() {
        $('#address-remove').fadeOut('fast');
        statusEndRemove = false;
    });

    var statusEndUpdate = false;
    $('body.account p.new-address-link a.address-update').click(function() {
        if (statusEndUpdate == false) {
            $('#address-edit').fadeIn('fast');
            statusEndUpdate = true;
        }
        else if (statusEndUpdate == true) {
            $('#address-edit').fadeOut('fast');
            statusEndUpdate = false;
        }
    });
    $('#address-edit .close').click(function() {
        $('#address-edit').fadeOut('fast');
        statusEndUpdate = false;
    });
    //fim legado		



}); // fim do document ready


$(window).load(function() {

    //click em ver todas as marcas
    $('.cont_marcas_fix .header_marcas_fix a.vertodas').click(function() {
        window.location.href = "/marcas";
    })//fim click em ver todas as marcas

    if ($('.name_brand .brands a').hasClass('4711') == true) {
        $('.name_brand .brands a').parent(".brandName").addClass("marca4711");
    }

    // JOGA O KIT NA SELECAO DE SKU
    var skuKit = $(".similar_prat ul li").html();
    $(".selectSku .skuList span.group_0").append(skuKit);

    $(".thumb-color .selectSku .sku-option-wrap").mouseenter(function() {
        $(this).find(".thumb-texture").stop(true, true).fadeIn(300);
        if ($(this).find(".thumb-texture").find("noscript").length > 0)
        {
            var img = $(this).find(".thumb-texture").find("noscript").text();
            $(this).find(".thumb-texture").html(img);
        }
    });

    $(".thumb-color .selectSku .sku-option-wrap").mouseleave(function() {
        $(this).find(".thumb-texture").stop(true, true).fadeOut(300);
    });


    if ($("body.lista.create").length > 0) {
        $("input#giftlistispublic-yes").trigger("click").trigger("change");
    } // fim Lista

    if($("body").hasClass("categoria")){
        // muda a posição da faixa de precos
        var precoCat = $(".cat_res .sidebar fieldset.filtro_faixa-de-preco").clone();
        $(precoCat).insertBefore(".cat_res .sidebar fieldset.filtro_marca");
        $(".cat_res .sidebar fieldset.filtro_faixa-de-preco").last().remove();

        // Ativando smartresearch para itens movidos
        $(".sidebar input[type='checkbox']").vtexSmartResearch({});
    }

});



$(document).ajaxStop(function() {

   



    if ($("body.home").length > 0) {
        // ABA IMPERDIVEL
        $(".home .imperdiveis .title ul li a").click(function() {
            $(".home .imperdiveis .title ul li").removeClass("ativo");
            $(this).parent("li").addClass("ativo");
            //$("html, body").animate({ scrollTop: ($(window).scrollTop()+2) }, 100);
            return false;
        });

        $(".home .imperdiveis .title ul li.masc a").click(function() {
            $(".home .imperdiveis .cont_prat .cont_masc").fadeIn(500);
            $(".home .imperdiveis .cont_prat .cont_femi").fadeOut(500);
            //$("html, body").animate({ scrollTop: ($(window).scrollTop()+2) }, 100);
        });

        $(".home .imperdiveis .title ul li.femi a").click(function() {
            $(".home .imperdiveis .cont_prat .cont_femi").fadeIn(500);
            $(".home .imperdiveis .cont_prat .cont_masc").fadeOut(500);
            //$("html, body").animate({ scrollTop: ($(window).scrollTop()+2) }, 100);
        });


        // ABA OFERTAS
        $(".home .ofertas .title ul li a").click(function() {
            $(".home .ofertas .title ul li").removeClass("ativo");
            $(this).parent("li").addClass("ativo");
            //$("html, body").animate({ scrollTop: ($(window).scrollTop()+2) }, 100);
            return false;
        });

        $(".home .ofertas .title ul li.perfumes a").click(function() {
            $(".home .ofertas .cont_prat .cont_perfumes").fadeIn(500);
            $(".home .ofertas .cont_prat .cont_maquiagem").fadeOut(500);
            $(".home .ofertas .cont_prat .cont_cabelo").fadeOut(500);
            //$("html, body").animate({ scrollTop: ($(window).scrollTop()+2) }, 100);
        });


        $(".home .ofertas .title ul li.maquiagem a").click(function() {
            $(".home .ofertas .cont_prat .cont_perfumes").fadeOut(500);
            $(".home .ofertas .cont_prat .cont_maquiagem").fadeIn(500);
            $(".home .ofertas .cont_prat .cont_cabelo").fadeOut(500);
            //$("html, body").animate({ scrollTop: ($(window).scrollTop()+2) }, 100);
        });

        $(".home .ofertas .title ul li.cabelo a").click(function() {
            $(".home .ofertas .cont_prat .cont_perfumes").fadeOut(500);
            $(".home .ofertas .cont_prat .cont_maquiagem").fadeOut(500);
            $(".home .ofertas .cont_prat .cont_cabelo").fadeIn(500);
            //$("html, body").animate({ scrollTop: ($(window).scrollTop()+2) }, 100);
        });
    }//fim Home
}); // fim do ajaxStop

function getFiltroMarcas()
{
    $.ajax({
        url: "/app-marcas",
        dataType: "html",
        success: function(data) {
            var dataHTML = data.substring(data.indexOf("<!--initmarcasapp-->"), data.indexOf("<!--endmarcasapp-->"));
            $(".m_marcas ul, #marcas-lateral").html(dataHTML);

            afterLoadAppMarcas();
            $(".dif.m_marcas .sub").removeClass("fix");
        }
    });

    function afterLoadAppMarcas()
    {

        $(window).bind("scroll", function() {
            var $this = $(this);
            if ($this.scrollTop() > 180) {
                $(".btn_marca").stop(true, true).animate({right: "0px"}, 200);
            } else {
                $(".btn_marca").stop(true, true).animate({right: "-70px"}, 200);
                $('.fix').animate({right: "-300px"}, 200);
            }
        });

        $(".btn_marca").click(function() {
            $(this).animate({right: "-70px"}, 200);
            $('.fix').animate({right: "0px"}, 200);
            return false
        });

        $('.fix').mouseleave(function() {
            $(this).stop(true, true).animate({right: "-300px"}, 200);
            $(".btn_marca").stop(true, true).animate({right: "0px"}, 200);
        });


        // $('.cont_marcas_fix .cont_resu .brandFilter ul').append('<li><a href="#">A marca 01</a></li><li><a href="#">A marca 02</a></li><li><a href="#">A marca 03</a></li><li><a href="#">A marca 04</a></li><li><a href="#">A marca 05</a></li><li><a href="#">A marca 06</a></li><li><a href="#">A marca 07</a></li><li><a href="#">A marca 08</a></li><li><a href="#">A marca 09</a></li><li><a href="#">A marca 10</a></li> <li><a href="#">B marca 01</a></li><li><a href="#">B marca 02</a></li><li><a href="#">B marca 03</a></li><li><a href="#">B marca 04</a></li><li><a href="#">B marca 05</a></li><li><a href="#">B marca 06</a></li><li><a href="#">B marca 07</a></li><li><a href="#">B marca 08</a></li>');


        $(".cont_marcas_fix .cont_resu .brandFilter ul li a").attr("target", "_parent");

        var listaTotasMarcas = $(".cont_marcas_fix .cont_resu .brandFilter ul").html();

        // INICIALMENTE COLOCA TODAS AS MARCAS
        $(".cont_marcas_fix .cont_resu .marcas_resu ul").html(listaTotasMarcas);


        // MARCAR A ULTIMA LETRA ESCOLHIDA
        $(".cont_marcas_fix .header_marcas_fix .inicial li a").click(function() {
            $(".cont_marcas_fix .header_marcas_fix .inicial li a").removeClass("ativo");
            $(this).addClass("ativo");
        });


        $(".cont_marcas_fix .header_marcas_fix .inicial li").click(function() {

            var container = $(this).parent().parent().parent();

            container.find(".cont_resu .marcas_resu ul").html(" ");
            var letraEscolhida = $(this).attr("class");


            container.find(".cont_resu .brandFilter ul li a").each(function() {

                var nomeMarca = $(this).text().toString();
                var inicialMarca = nomeMarca.charAt(0).toLowerCase();

                if (letraEscolhida == inicialMarca) {
                    var marcasFiltradas = $(this).parent("li").html();

                    container.find(".cont_resu .marcas_resu ul").append("<li>" + marcasFiltradas + "</li>");
                }

            });

            if (container.find(".cont_resu .marcas_resu li").length < 1) {
                container.find(".cont_resu .marcas_resu ul").html("<p>Nenhuma marca com a letra <span>" + letraEscolhida + "</span></p>");
            }


            // SCROLL
            if (container.find(".cont_resu .marcas_resu li").length > 9) {
                container.find(".cont_resu .marcas_resu ul").mCustomScrollbar();
                // $(".m_marcas .cont_marcas_fix .cont_resu .marcas_resu ul").mCustomScrollbar();
            }


            return false;

        });


        // ver todas
        $("a.vertodas").click(function() {
            $(".cont_marcas_fix .cont_resu .marcas_resu ul").html(listaTotasMarcas);
            $(".cont_marcas_fix .header_marcas_fix .inicial li a").removeClass("ativo");

            // SCROLL
            if ($(".cont_marcas_fix .cont_resu .marcas_resu li").length > 9) {
                $(".cont_marcas_fix .cont_resu .marcas_resu ul").mCustomScrollbar();
                // $(".m_marcas .cont_marcas_fix .cont_resu .marcas_resu ul").mCustomScrollbar();
            }

            return false;
        });



        // SCROLL
        if ($(".cont_marcas_fix .cont_resu .marcas_resu li").length > 9) {
            $(".cont_marcas_fix .cont_resu .marcas_resu ul").mCustomScrollbar();
            // $(".m_marcas .cont_marcas_fix .cont_resu .marcas_resu ul").mCustomScrollbar();
        }
    }
}

//ORGANIZANDO SITEMAP
$(window).load(function(){    
    if($("body").hasClass("sitemap")){  
        $(".cont100 .cont").append("<ul class='columns'></ul>");
        var contclass = 0;
        $(".cont100 .cont .menu-departamento h3").each(function(){
            $('.columns').append('<li class="colmap mapcol'+contclass+'"></li>');
            $(this).next().appendTo('.columns li.mapcol'+contclass+'');
            $(this).prependTo('.columns li.mapcol'+contclass+'');       
            if(contclass == 6){$(".menu-departamento").show();}
            contclass++;
        }); 
    }
});

$(document).one('ajaxStop', function(){

 //gerador de %
    if( $('body').hasClass('produto') ){
        var deValor = $('.infoLeft .valor-de strong').text().replace('.', '').replace(',', '.').split('R$ ');
        var porValor = $('.infoLeft .valor-por strong').text().replace('.', '').replace(',', '.').split('R$ ');
        var descontoPorc = ((deValor[1] - porValor[1]) / deValor[1]) * 100;
        if(descontoPorc.toFixed(0) != 'NaN'){
            $('.preco .plugin-preco .productPrice').append('<p class="priceOffProduto">' + descontoPorc.toFixed(0) + '% OFF</p>');
            if($('.selectSku').is(":visible")){
              $('.priceOffProduto').addClass('mostraSku');
            }
        }
    }

});