jQuery(document).foundation({
    offcanvas: {
    close_on_click: false
}}
);

jQuery(document).ready(function() {

    /* Nivo Lightbox */

    jQuery('a.light').nivoLightbox();
    jQuery('a.search-button').nivoLightbox({effect: 'fade', theme: 'custom'});
    jQuery('a.share-button').nivoLightbox({effect: 'fade', theme: 'custom'});
    jQuery('a.bright').nivoLightbox({theme: 'custom'});


    /* Scroll To Top */

    jQuery(".scroll-top").hide();

    jQuery(function () {
        jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 50) {
                jQuery('.scroll-top').fadeIn();
            } else {
                jQuery('.scroll-top').fadeOut();
            }
        });

        jQuery('.scroll-top a').click(function () {
            jQuery('body,html').animate({
                scrollTop: 0
            }, 400);
            return false;
        });
    });



    /* Slider */

    jQuery('.slider').owlCarousel({
        loop: true, // loop is true up to 1199px screen.
        nav: true, // is true across all sizes
        navText: ['',''],
        dots: false,
        mobileBoost: true,
        nestedItemSelector: 'slide',
        //pagination: false,
        //lightweightStructure:true,
        margin: 0, // margin 10px till 960 breakpoint
        merge: true,
        center: true,
        autoplay:true,
        autoplayTimeout: 3000,
        autoplaySpeed: 6000,
        smartSpeed: 1500,
        autoplayHoverPause: true,
        items: 1
    });

    jQuery('.carousel').owlCarousel({
        loop: true,
        items: 2,
        nav: true, // is true across all sizes
        navText: ['',''],
        dots: false,
        mobileBoost: true,
        margin: 0, // margin 10px till 960 breakpoint
        merge: false,
        center: true,
        autoplay:true,
        autoplayTimeout: 3000,
        autoplaySpeed: 6000,
        smartSpeed: 1500,
        autoplayHoverPause: true,

        responsive:{
            0:{
                items:1 // In this configuration 1 is enabled from 0px up to 479px screen size
            },
            480:{
                items:1
            },

            678:{
                items:1 // from this breakpoint 678 to 959
            },

            960:{
                items:2 // from this breakpoint 960 to 1199
            },

            1200:{
                items:2
            }
        }
    });

    jQuery('.carousel-small').owlCarousel({
        loop: true,
        items: 2,
        nav: true, // is true across all sizes
        navText: ['',''],
        dots: false,
        mobileBoost: true,
        margin: 0, // margin 10px till 960 breakpoint
        merge: false,
        center: true,
        autoplay:true,
        autoplayTimeout: 3000,
        autoplaySpeed: 6000,
        smartSpeed: 1500,
        autoplayHoverPause: true,

        responsive:{
            0:{
                items:1 // In this configuration 1 is enabled from 0px up to 479px screen size
            },
            480:{
                items:1
            },

            678:{
                items:1 // from this breakpoint 678 to 959
            },

            960:{
                items:2 // from this breakpoint 960 to 1199
            },

            1200:{
                items:3
            }
        }
    });

    jQuery('.newsticker').owlCarousel({
        items: 1,
        loop: true, // loop is true up to 1199px screen.
        nav: true, // is true across all sizes
        navText: ['',''],
        dots: false,
        mobileBoost: true,
        animateIn: 'fadeInDown',
        animateOut: 'fadeOut',
        autoWidth:false,
        nestedItemSelector: 'news-item',
        //pagination: false,
        //lightweightStructure:true,
        margin: 0, // margin 10px till 960 breakpoint
        merge: false,
        center: false
    });




});

/*Isotope*/

var $container6 = jQuery('.container-6cols');

$container6.imagesLoaded( function(){
    $container6.isotope({
        itemSelector : '.isotope-item',
        resizable: false,
        animationEngine : 'css',
        masonry: { columnWidth: '.grid-sizer' },
        filter: '*'
    });
});

var $container5 = jQuery('.container-5cols');

$container5.imagesLoaded( function(){
    $container5.isotope({
        itemSelector : '.isotope-item',
        resizable: false,
        animationEngine : 'css',
        masonry: { columnWidth: '.grid-sizer' },
        filter: '*'
    });
});

var $container4 = jQuery('.container-4cols');

$container4.imagesLoaded( function(){
    $container4.isotope({
        itemSelector : '.isotope-item',
        resizable: false,
        animationEngine : 'css',
        masonry: { columnWidth: '.grid-sizer' },
        filter: '*'
    });
});

var $container3 = jQuery('.container-3cols');

$container3.imagesLoaded( function(){
    $container3.isotope({
        itemSelector : '.isotope-item',
        resizable: false,
        animationEngine : 'css',
        masonry: { columnWidth: '.grid-sizer' },
        filter: '*'
    });
});

var $container2 = jQuery('.container-2cols');

$container2.imagesLoaded( function(){
    $container2.isotope({
        itemSelector : '.isotope-item',
        resizable: false,
        animationEngine : 'css',
        masonry: { columnWidth: '.grid-sizer' },
        filter: '*'
    });
});

// filtering
jQuery('nav.filter dl dd a').click(function(){
    var selector = jQuery(this).attr('data-filter');
    $container4.isotope({
        filter: selector,
        animationOptions: {
            duration: 750,
            easing: 'linear',
            queue: false
        }
    });
    return false;
});

jQuery('nav.filter dl dd a').click(function(){
    var selector = jQuery(this).attr('data-filter');
    $container5.isotope({
        filter: selector,
        animationOptions: {
            duration: 750,
            easing: 'linear',
            queue: false
        }
    });
    return false;
});
var $optionSets = jQuery('nav.filter dl'),
    $optionLinks = $optionSets.find('a');

$optionLinks.click(function(){
    var $this = jQuery(this);
    // don't proceed if already selected
    if ( $this.hasClass('selected') ) {
        return false;
    }
    var $optionSet = $this.parents('nav.filter dl');
    $optionSet.find('.selected').removeClass('selected');
    $this.addClass('selected');
});

/* Fixed Menu
var nav = jQuery('.contain-to-grid');

jQuery(window).scroll(function () {
    if (jQuery(this).scrollTop() > 193 ) {
        nav.addClass("fixed");
    } else {
        nav.removeClass("fixed");
    }
});
 */