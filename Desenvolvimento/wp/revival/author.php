<?php
$author_layout = get_option('revivaltheme_author_layout' );
$author_sidebar = get_option('revivaltheme_author_sidebar' );
?>
<?php get_header(); ?>

<div class="about-author white row">

    <div class="large-10 large-centered columns">


    <?php if(have_posts()) the_post();

    // If a user has filled out their description, show a bio on their entries.
    if(get_the_author_meta('description')):
        ?>
        <div class="author-img large-5 columns">
            <?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'author_bio_avatar_size', 300 ) ); ?>
        </div>


    <?php endif; rewind_posts(); ?>

        <div class="large-7 columns end">

            <?php
            if(have_posts()) the_post();

            // If a user has filled out their description, show a bio on their entries.
            if(get_the_author_meta('description')):
                ?>
                <h1 class="archive-title"><?php $author = get_userdata( get_query_var('author') );?><?php echo $author->display_name;?>  <i class="i-pencil"></i> <span><?php _e('Author Posts & Bio', 'revivaltheme'); ?></span></h1>


                <p><strong><?php _e('Author Since:', 'revivaltheme'); ?></strong> <?php the_author_meta('user_registered'); ?></p>
                <p><strong><?php _e('Website:', 'revivaltheme'); ?></strong> <a href="<?php the_author_meta( 'user_url' ); ?>"><?php the_author_meta( 'user_url' ); ?></a></p>

                <p><strong><?php _e('Bio:', 'revivaltheme'); ?></strong> <?php the_author_meta('description'); ?></p>

        </div>

        <?php endif;
        rewind_posts();
        ?>

        </div>

    </div>


    <section class="gray animated fadeIn" role="main">

        <div class="row collapse">

            <?php if ($author_sidebar == 'fullwidth') { ?>

            <div class="<?php if ($author_layout == 'blog') { ?>large-10 large-centered<?php } else { ?>large-12<?php } ?> columns">

                <?php } else { ?>

                <div class="large-9 medium-12 small-12 columns">

                    <?php } ?>


                    <?php if ( have_posts() ) : ?>


                        <?php // Layout
                        if ($author_layout == 'magazine_2') { get_template_part('layout/loop', 'magazine_2'); }
                        elseif ($author_layout == 'magazine_3') { get_template_part('layout/loop', 'magazine_3'); }
                        elseif ($author_layout == 'magazine_4') { get_template_part('layout/loop', 'magazine_4'); }
                        else { get_template_part('layout/loop', 'blog'); } ?>

                    <?php else : ?>

                        <?php get_template_part( 'content', 'none' ); ?>

                    <?php endif; // end have_posts() check ?>


                </div><!--end main column-->


                <?php if ( $author_sidebar == 'fullwidth') { } else { ?>

                    <?php get_sidebar(); ?>

                <?php } ?>


            </div><!--End Row-->

    </section>

<?php get_footer(); ?>