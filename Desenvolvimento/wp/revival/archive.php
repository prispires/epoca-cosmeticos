<?php
$archive_layout = get_option('revivaltheme_archive_layout' );
$archive_sidebar = get_option('revivaltheme_archive_sidebar' );
?>
<?php get_header(); ?>


    <div class="row collapse">


        <div class="large-12 columns">
            <h1 class="archive-title animated fadeInRight">
                <span><?php _e('Archives', 'revivaltheme'); ?> <i class="i-calendar-1"></i> </span>
                <?php if ( is_day() ) : ?>
                    <?php the_time('D, j F Y'); ?>

                <?php elseif ( is_month() ) : ?>

                    <?php the_time('F Y'); ?>

                <?php elseif ( is_year() ) : ?>

                    <?php the_time('Y'); ?>


                <?php else : ?>
                    <?php _e( 'Blog Archives', 'revivaltheme' ); ?>

                <?php endif; ?>
            </h1>
        </div>

    </div>

    <section class="gray animated fadeIn" role="main">

        <div class="row collapse">

            <?php if ($archive_sidebar == 'fullwidth') { ?>

            <div class="<?php if ($archive_layout == 'blog') { ?>large-10 large-centered<?php } else { ?>large-12<?php } ?> columns">

                <?php } else { ?>

                <div class="large-9 medium-12 small-12 columns">

                    <?php } ?>


                    <?php if ( have_posts() ) : ?>


                        <?php // Layout
                        if ($archive_layout == 'magazine_2') { get_template_part('layout/loop', 'magazine_2'); }
                        elseif ($archive_layout == 'magazine_3') { get_template_part('layout/loop', 'magazine_3'); }
                        elseif ($archive_layout == 'magazine_4') { get_template_part('layout/loop', 'magazine_4'); }
                        else { get_template_part('layout/loop', 'blog'); } ?>

                    <?php else : ?>

                        <?php get_template_part( 'content', 'none' ); ?>

                    <?php endif; // end have_posts() check ?>


                </div><!--end main column-->


                <?php if ( $archive_sidebar == 'fullwidth') { } else { ?>

                    <?php get_sidebar(); ?>

                <?php } ?>


            </div><!--End Row-->

    </section>

<?php get_footer(); ?>