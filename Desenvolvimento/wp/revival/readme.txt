Revival is a responsive magazine/blog WordPress theme.

Changelog

= V1.0 - 04.06.2014 =
+ Initial Release.

= V1.0.1 - 06.06.2014 =
+ Fixed single pagination position.
+ Fixed white color on white background for status post in blog grid.
+ More stable Owl Carousel version.
+ Some styles improvements for single post with right sidebar.
+ Font weight 300 changed to 400 to make main content more readable for non-Retina displays.

= V1.0.2 - 06.06.2014 =
+ Better colors for dark background
+ Improved status post styles.
+ Fixed footer menu styles.
+ Removed white space if there are no widgets in footer.
+ Fullwidth footer widget area is at the bottom of all other widgets now.

= V1.0.3 - 11.06.2014 =
+ Optional entry meta info.
+ Updated Owl Carousel.

= V1.0.4 - 12.06.2014 =
+ Fixed margins for pages and quotes.
+ Featured image for pages.
+ Wider video / audio column for fullwidth post.

= V1.1 - 16.06.2014 =
+ Optional header background.
+ Optional date/author entry meta.
+ Cleaner functions.php file.
+ Envato Wordpress Toolkit added as recommended plugin.
+ Wider column for News Ticker in header.
+ News Ticker is hidden for tablets.
+ User avatar in comments is hidden for small devices.
+ Third-party plugins and libraries are moved to /vendor folder.
+ Paddings for image logo.