<?php
$search_layout = get_option('revivaltheme_search_layout' );
$search_sidebar = get_option('revivaltheme_search_sidebar' );
?>
<?php get_header(); ?>


    <div class="row collapse">


        <div class="large-12 columns">
            <h1 class="archive-title animated fadeInRight">
                <span><?php _e('Resultado da Pesquisa', 'revivaltheme'); ?> <i class="i-search"></i> </span>
                "<?php echo get_search_query(); ?>"
            </h1>
        </div>

    </div>

    <section class="gray animated fadeIn" role="main">

        <div class="row collapse">

            <?php if ($search_sidebar == 'fullwidth') { ?>

            <div class="<?php if ($search_layout == 'blog') { ?>large-10 large-centered<?php } else { ?>large-12<?php } ?> columns">

                <?php } else { ?>

                <div class="large-9 medium-12 small-12 columns">

                    <?php } ?>


                    <?php if ( have_posts() ) : ?>


                        <?php // Layout
                        if ($search_layout == 'magazine_2') { get_template_part('layout/loop', 'magazine_2'); }
                        elseif ($search_layout == 'magazine_3') { get_template_part('layout/loop', 'magazine_3'); }
                        elseif ($search_layout == 'magazine_4') { get_template_part('layout/loop', 'magazine_4'); }
                        else { get_template_part('layout/loop', 'blog'); } ?>

                    <?php else : ?>

                        <?php get_template_part( 'content', 'none' ); ?>

                    <?php endif; // end have_posts() check ?>


                </div><!--end main column-->


                <?php if ( $search_sidebar == 'fullwidth') { } else { ?>

                        <?php get_sidebar(); ?>

                <?php } ?>


            </div><!--End Row-->

    </section>

<?php get_footer(); ?>