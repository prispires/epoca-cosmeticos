<?php $detect = new Mobile_Detect;
$hide_meta = get_option( 'revivaltheme_hide_meta' );
$cat = get_query_var('cat'); ?>

<div class="isotope-item w2 animated fadeIn">
<div class="slider <?php if ( has_post_format( 'quote' ) || $hide_meta !== '') { echo 'no-date'; } ?> text-slider owl-carousel animated fadeIn">

    <?php if (is_category()) {
            $args = array(
                'numberposts' => -1,
                'ignore_sticky_posts' => 1,
                'post_type' => 'post',
                'meta_key' => 'show_in_slider',
                'cat' => $cat,
                'meta_value' => '1'
            );
        } else {
            $args = array(
                'posts_per_page' => 20,
                'ignore_sticky_posts' => 1,
                'post_type' => 'post',
                'meta_key' => 'show_in_slider',
                'meta_value' => '1'
            );
        }
        // get results
        $the_query = new WP_Query( $args );


        if( $the_query->have_posts() ): while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


            <div class="slide <?php $captionbg = get_field('caption_background');  if ( $captionbg ) { echo $captionbg; } else { echo 'white'; } ?>">

                <article <?php post_class(''); ?>>


                <?php if ( '' != get_the_post_thumbnail() ) {
                    $medium_crop = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium_crop');
                    $small_crop = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'small_crop'); ?>

                    <div class="media-holder overlay">

                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">


                            <?php if ( $detect->isMobile() && !$detect->isTablet() ) { ?>
                                <img src="<?php echo $small_crop[0]; ?>" alt="<?php revivaltheme_thumbnail_title();?>">

                            <?php } else { ?>
                                <img src="<?php echo $medium_crop[0]; ?>" alt="<?php revivaltheme_thumbnail_title();?>">
                            <?php } ?>

                        </a>


                    </div>

                <?php } ?>


                <div class="text-holder">

                    <?php if ( has_post_format( 'quote' ) || has_post_format( 'status' ) || has_post_format( 'aside' ) ) {}

                    elseif ( has_post_format( 'link' )) { ?>
                        <h2 class="entry-title"><?php revivaltheme_link_title() ?></h2>
                    <?php  } else { ?>
                        <h2 class="entry-title"><a href="<?php the_permalink(); ?>">
                                <?php $short_title = the_title("","",false);
                                $short_title_2 = mb_substr($short_title,0,55);
                                echo $short_title_2;
                                if($short_title_2!=$short_title) { echo "..."; } ?>
                            </a></h2>
                    <?php } ?>

                    <?php
                    if ( get_field('enable_review') == '1' ) { ?>

                        <span class="side-score"><?php get_template_part( 'inc/rating'); ?></span>

                    <?php }  ?>

                    <p class="excerpt-text"><?php revivaltheme_excerpt(160); ?></p>


                </div>

                <?php if ( has_post_format( 'quote' ) || ($hide_meta == 'hide_all' || $hide_meta == 'show_author' )) { echo '<div class="divider"></div>'; } else { ?><div class="date-meta"><?php revivaltheme_date(); ?></div><?php } ?>

            </article>

         </div>


        <?php endwhile; ?>
        <?php endif; wp_reset_query(); ?>


</div><!--end slider-->
</div>
