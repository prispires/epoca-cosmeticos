<div class="blog-grid animated fadeInDown">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <?php get_template_part( 'content', 'blog'); ?>

    <?php endwhile; else: ?>

        <p class="alert-box message">
            <?php _e( 'This section does not contain any posts yet', 'revivaltheme' ); ?>
        </p>

    <?php endif; wp_reset_query(); ?>

</div>