<?php $news = get_field( 'news_ticker' );
?>
<section id="news" class="hide-for-medium-down">

    <div class="newsticker owl-carousel white">

        <?php
        $wp_query = new WP_Query(
            array(
                'posts_per_page' => 10,
                'ignore_sticky_posts' => 1,
                'post_type' => 'post',
                'meta_key' => 'news_ticker',
            )
        );

        if ( $wp_query->have_posts() ) :
            while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

                <div class="news-item animated fadeInDown">

                    <a href="<?php the_permalink(); ?>">
                        <?php the_title(); ?>
                    </a>

                    <span class="entry-date"><?php echo get_the_date(''); ?></span>


                </div>

            <?php endwhile; ?>

        <?php endif; ?>

        <?php wp_reset_query(); ?>

    </div>
</section>