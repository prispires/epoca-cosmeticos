<?php
$detect = new Mobile_Detect;
$images=get_field('top_gallery'); if( $images ): ?>

<div class="portfolio row collapse">

        <?php foreach( $images as $image ): ?>

            <div class="large-6 medium-6 columns">

                <div class="media-holder has-hover">

                    <a href="<?php if ( $detect->isMobile() && !$detect->isTablet() ) { ?><?php echo $image['sizes']['medium_full']; ?><?php } else { ?><?php echo $image['sizes']['large_full']; ?><?php } ?>" title="<?php revivaltheme_thumbnail_title(); ?>" class="info light">


                        <?php if ( $detect->isMobile() && !$detect->isTablet() ) { ?>
                            <img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['title']; ?>" />
                        <?php } else { ?>
                            <img src="<?php echo $image['sizes']['medium_crop']; ?>" alt="<?php echo $image['title']; ?>" />
                        <?php } ?>

                            <div class="is-hover">
                                <div class="inner no-bg">
                                    <div class="icon">
                                        <i class="i-lightbulb"></i>
                                    </div>
                                </div>
                            </div>


                    </a>
                </div>

            </div>

        <?php endforeach; ?>

    </div>


<?php endif; ?>