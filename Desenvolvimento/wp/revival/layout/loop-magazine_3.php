<div class="isotope container-3cols masonry animated fadeInDown">

    <div class="grid-sizer"></div>

      <?php $homepage_slider = get_option('revivaltheme_homepage_slider' ); if ( is_home()&&!is_paged()&&$homepage_slider ) {  ?>

            <?php get_template_part( 'layout/loop', 'slider'); ?>


    <?php } ?>


    <?php while (have_posts()) : the_post(); ?>

        <?php $check_slider = get_field('show_in_slider'); if ( !$check_slider ) { ?>

            <?php get_template_part( 'content', 'magazine'); ?>

        <?php } ?>

    <?php endwhile; ?>

</div>