<?php $detect = new Mobile_Detect;
$cat = get_query_var('cat'); ?>

<div class="carousel owl-carousel animated fadeIn">

    <?php if (is_category()) {
        $args = array(
            'numberposts' => -1,
            'ignore_sticky_posts' => 1,
            'post_type' => 'post',
            'meta_key' => 'show_in_carousel',
            'cat' => $cat,
            'meta_value' => '1'
        );
    } else {
        $args = array(
            'posts_per_page' => 20,
            'ignore_sticky_posts' => 1,
            'post_type' => 'post',
            'meta_key' => 'show_in_carousel',
            'meta_value' => '1'
        );
    }
    // get results
    $the_query = new WP_Query( $args );


    if( $the_query->have_posts() ): while ( $the_query->have_posts() ) : $the_query->the_post(); ?>




        <div class="media-holder overlay has-hover">

            <?php if ( '' != get_the_post_thumbnail() ) { ?>

                <?php $medium_crop = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium_crop' );
                $small = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'small' ); ?>



                <?php if ( $detect->isMobile() && !$detect->isTablet() ) { ?>
                    <img src="<?php echo $small[0]; ?>" alt="<?php the_title(); ?>" />
                <?php } else { ?>
                    <img src="<?php echo $medium_crop[0]; ?>" alt="<?php the_title(); ?>" />
                <?php } ?>


            <?php } else { ?>
                <div class="no-image">
                    <a href="<?php the_permalink(); ?>">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/image-ratio.gif" title="No image" />
                    </a>
                </div>
            <?php }  ?>

            <div class="is-hover">

                <div class="inner">


                    <?php if ( has_post_format( 'link' )) { ?>
                        <h2><?php revivaltheme_link_title() ?></h2>
                    <?php  } else { ?>
                        <h2>
                            <a href="<?php the_permalink(); ?>">
                                <?php $short_title = the_title("","",false);
                                $short_title_2 = mb_substr($short_title,0,40);
                                echo $short_title_2;
                                if($short_title_2!=$short_title) { echo "..."; } ?>
                            </a>
                        </h2>

                    <?php } ?>

                    <h5 class="hide-for-small"><?php the_category(' / '); ?></h5>

                </div><!--end inner-->

            </div><!--end is_hover-->


        </div><!--end media_holder-->


    <?php endwhile; ?>
    <?php endif; wp_reset_query(); ?>


</div><!--end slider-->