<?php $detect = new Mobile_Detect;
$images=get_field('top_gallery'); if( $images ): ?>

    <div class="carousel owl-carousel animated fadeIn">


    <?php foreach( $images as $image ): ?>

        <div class="slide">

            <a href="<?php if ( $detect->isMobile() && !$detect->isTablet() ) { ?><?php echo $image['sizes']['medium_crop']; ?><?php } else { ?><?php echo $image['sizes']['large_full']; ?><?php } ?>" class="light">

                <?php if ( $detect->isMobile() && !$detect->isTablet() ) { ?>
                    <img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['title']; ?>" />
                <?php } else { ?>
                    <img src="<?php echo $image['sizes']['medium_crop']; ?>" alt="<?php echo $image['title']; ?>" />
                <?php } ?>

            </a>


        </div>

    <?php endforeach; ?>


</div>


<?php endif; ?>