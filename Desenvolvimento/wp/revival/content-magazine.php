<?php $featured = get_field('show_featured'); ?>
<?php $captionbg = get_field('caption_background'); ?>

<div class="isotope-item <?php if ( $featured ) {?>w2<?php } ?> <?php echo revivaltheme_cat_slug(); ?>">

    <div class="<?php if ( $captionbg ) { echo $captionbg; } else { echo 'white'; } ?>">

        <article <?php post_class(''); ?>>


            <?php if ( ( has_post_format( 'quote') ) || ( has_post_format('link') ) || ( has_post_format('aside') ) ) { } else { ?>
                    <?php { get_template_part( 'inc/featured');  } ?>

            <?php } ?>

            <?php { get_template_part( 'inc/magazine', 'text');  } ?>


        </article>

    </div><!--end post class-->

</div><!--end isotope item-->