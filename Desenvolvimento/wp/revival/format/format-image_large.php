<?php $detect = new Mobile_Detect;
$medium_full = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium_full' );
$large_full = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large_full' ); ?>

<div class="media-holder">

    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <?php if ( $detect->isMobile() && !$detect->isTablet() ) { ?>
            <img src="<?php echo $medium_full[0]; ?>" alt="<?php revivaltheme_thumbnail_title();?>">
        <?php } else { ?>
            <img src="<?php echo $large_full[0]; ?>" alt="<?php revivaltheme_thumbnail_title();?>">
        <?php } ?>

    </a>

</div>