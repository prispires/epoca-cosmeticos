<?php
$tweet = get_field( 'tweet_link' ); ?>

<?php if ($tweet) { ?>

    <div class="tweet">

        <?php
        $htmlcode = wp_oembed_get($tweet);
        echo $htmlcode; ?>

    </div>
<?php } ?>