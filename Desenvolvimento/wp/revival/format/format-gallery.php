<?php $gallery_type=get_field( 'gallery_type' ); ?>

<?php if ( $gallery_type == 'gallery_columns_2') {?>

    <?php get_template_part('layout/loop', 'gallery_columns_2'); ?>

<?php } elseif ( $gallery_type == 'gallery_columns_3') {?>

<?php get_template_part('layout/loop', 'gallery_columns_3'); ?>

<?php } elseif ( $gallery_type == 'carousel_gallery') {?>

    <?php get_template_part('layout/loop', 'gallery_carousel'); ?>

<?php } else { ?>

    <?php get_template_part('layout/loop', 'gallery_slider'); ?>

<?php } ?>
