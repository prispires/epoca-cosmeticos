<?php
$audio = get_field( 'soundcloud_audio' ); ?>

<div class="media-holder animated fadeInDown">

    <div class="soundcloud">

        <?php
        $htmlcode = wp_oembed_get($audio);
        echo $htmlcode; ?>

    </div>

</div>
