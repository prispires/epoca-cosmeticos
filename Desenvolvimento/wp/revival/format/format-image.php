<?php $detect = new Mobile_Detect;
$large_crop = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large_crop' );
$small = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'small' ); ?>


<div class="media-holder animated fadeInDown">

    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <?php if ( $detect->isMobile() && !$detect->isTablet() ) { ?>
            <img src="<?php echo $medium_full[0]; ?>" alt="<?php revivaltheme_thumbnail_title();?>">
        <?php } else { ?>
            <img src="<?php echo $large_full[0]; ?>" alt="<?php revivaltheme_thumbnail_title();?>">
        <?php } ?>

    </a>


</div>