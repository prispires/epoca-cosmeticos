<?php $video_youtube = get_field( 'video_youtube' );
$video_vimeo = get_field( 'video_vimeo' );
$video_brightcove = get_field( 'video_brightcove' );
if($video_youtube) {
echo '<div class="flex-video widescreen"><iframe src="http://www.youtube.com/embed/'.$video_youtube.'?rel=0&showinfo=0"></iframe></div>';	}
elseif ($video_vimeo) {echo '<div class="flex-video vimeo widescreen"><iframe src="http://player.vimeo.com/video/'.$video_vimeo.'?title=0&amp;byline=0&amp;portrait=0"></iframe></div>'; }
elseif ( $video_brightcove || class_exists ('BC_Shortcode')) {
    echo do_shortcode( '<div class="flex-video brightcove widescreen">[bc]http://bcove.me/'.$video_brightcove.'[/bc]</div>' );
}
?>