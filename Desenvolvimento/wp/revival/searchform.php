<form role="search" class="custom" method="get" id="searchform" action="<?php echo home_url('/'); ?>">
    <div class="row collapse">
        <div class="small-8 large-8 medium-7 columns">
            <input type="text" value="" name="s" id="s-top" placeholder="<?php _e('Buscar', 'revivaltheme'); ?>">

        </div>
        <div class="small-4 large-4 medium-5 columns">
            <input type="submit" id="searchsubmit-top" value="<?php _e('enviar', 'revivaltheme'); ?>" class="postfix">
        </div>
    </div>
</form>
