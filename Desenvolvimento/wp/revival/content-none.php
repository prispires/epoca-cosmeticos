<!-- Row for main content area -->
<div id="content" class="row collapse animated fadeIn">

    <div class="small-12 large-7 large-centered medium-8 medium-centered columns" role="main">

        <div id="post-0" class="entry-wrapper white">

            <article <?php post_class() ?>>
                <header class="entry-header">
                    <h1 class="entry-title"><?php _e( 'Não encontramos! :(', 'revivaltheme' ); ?></h1>
                </header>
                <div class="entry-content">
                    <p><?php _e( 'Que tal se você tentar buscar um termo diferente?', 'revivaltheme' ); ?></p>

                    <div class="divider-medium"></div>
                    <?php get_search_form(); ?>

                </div>
            </article>

        </div>

    </div>

</div>