<div class="header_flutuante">
    <div class="top100">
        <div class="top">
            <h1 class="logo"><a href="/">Época Cosméticos - Perfumaria</a></h1>

        <div class="search">
            <fieldset class="busca">
            <form role="search" class="custom" method="get" id="searchform" action="<?php echo home_url('/'); ?>">
                <input type="text" value="" name="s" id="s-top" placeholder="<?php _e('Buscar...', 'revivaltheme'); ?>">
                <input type="button" class="btn-buscar" value="Buscar" id="ftBtn7e7000330a95461d84d51b4777618bb8">
            </form>
            </fieldset>
        </div>

<div class="regua">
    <div class="box-banner">
        <a>
            <img width="257" height="43" complete="complete" src="http://epocacosmeticos.vteximg.com.br/arquivos/ids/155208/banner_regua_topo_281_28.png?v=635339337063370000" alt="bannerReguaTopo" id="ihttp://epocacosmeticos.vteximg.com.br/arquivos/ids/155208/banner_regua_topo_281_28.png?v=635339337063370000">
        </a>
    </div>
</div>

            <ul class="menu_dept">
                <li class="item"><a href="#" class="item">Departamentos</a>
                    <ul class="sub_dept">
                        <li><a href="http://www.epocacosmeticos.com.br/perfumes">Perfumes</a></li>
                        <li><a href="http://www.epocacosmeticos.com.br/maquiagem">Maquiagem</a></li>
                        <li><a href="http://www.epocacosmeticos.com.br/cabelos">Cabelos</a></li>
                        <li><a href="http://www.epocacosmeticos.com.br/dermocosmeticos">Dermocosm&eacute;ticos</a></li>
                        <li><a href="http://www.epocacosmeticos.com.br/tratamentos">Tratamentos</a></li>
                        <li><a href="http://www.epocacosmeticos.com.br/corpo-e-banho">Corpo e Banho</a></li>
                        <li><a href="http://www.epocacosmeticos.com.br/solares">Solares</a></li>
                    </ul>
                </li>
            </ul>


        </div> <!-- top -->
    </div> <!-- top100 -->
</div> <!-- header_flutuante -->

<div class="topbanner">
    <div style="margin:0px auto; padding:0px; width:955px; height:40px">
        <img width="955" height="40" style="display:block;" src="http://epocacosmeticos.vteximg.com.br/arquivos/bannertopo-10xsemjuros2.jpg?v=635348815784870000">
    </div><!-- <vtex:bannerTopo /> -->
        <a class="close" href="#"></a>
</div><!-- Final top banner -->
<div class="topbar100">
    <div class="topbar">
        <div class="welc">
            <div rel="http://www.epocacosmeticos.com.br/login" class="ajax-content-loader">
        <p class="welcome">
        <!--Ol&aacute; <span class="visitante">Visitante</span>, Bem vindo!
        <em><a id="login"></a></em>-->
        </p>
        <div class="logaqui"><em> <a href="http://www.epocacosmeticos.com.br/Site/Login.aspx"></a></em></div><em>
        </em>
        <p></p>
            </div>
        </div>
        <ul class="menu_topbar">
            <li class="atendimento">
                <a href="http://www.epocacosmeticos.com.br/centralatendimento">Atendimento</a>
            </li>
            <li class="meuspedidos">
                <a href="http://www.epocacosmeticos.com.br/account/orders">Meus Pedidos</a>
            </li>
            <li class="lista">
                <a href="http://www.epocacosmeticos.com.br/giftList">Lista de Desejos</a>
            </li>
        </ul>
    </div>
</div><!-- Final Login area -->

<div class="menuMobile">
    <h1 class="logo">
        <a href="/"><img src="http://blog.epocacosmeticos.com.br/wp-content/themes/revival/img/logoEpoca.jpg" /></a>
    </h1>
     <div class="options">
            <div id="show"><img src="http://blog.epocacosmeticos.com.br/wp-content/themes/revival/img/options.jpg" /></div>
     </div>
            <div id="subMenuMobile" style="display:none">
                
                <div class="showSubMenu">
                    <div id="hide"><img src="http://blog.epocacosmeticos.com.br/wp-content/themes/revival/img/fecharMenu.jpg" /></div>
                        <div class="search">
                            <fieldset class="busca">
                            <form role="search" class="custom" method="get" id="searchform" action="<?php echo home_url('/'); ?>">
                                <input type="text" value="" name="s" id="s-top" placeholder="<?php _e('Buscar...', 'revivaltheme'); ?>">
                                <input type="button" class="btn-buscar" value="Buscar" id="ftBtn7e7000330a95461d84d51b4777618bb8">
                            </form>
                            </fieldset>
                        </div>
                        <h4>
                            <a class="princ" href="http://www.epocacosmeticos.com.br/perfumes">Perfumes</a>
                        </h4>
                        <h4>
                            <a class="princ" href="http://www.epocacosmeticos.com.br/maquiagem">Maquiagem</a>
                        </h4>
                        <h4>
                            <a class="princ" href="http://www.epocacosmeticos.com.br/cabelos">Cabelos</a>
                        </h4>
                        <h4>
                            <a class="princ" href="http://www.epocacosmeticos.com.br/dermocosmeticos">Dermocosm&eacute;ticos</a>
                        </h4>
                        <h4>
                            <a class="princ" href="http://www.epocacosmeticos.com.br/tratamentos">Tratamentos</a>
                        </h4>
                        <h4>
                            <a class="princ" href="http://www.epocacosmeticos.com.br/corpo-e-banho">Corpo e Banho</a>
                        </h4>
                        <h4>
                            <a class="princ" href="http://www.epocacosmeticos.com.br/ofertas">Ofertas</a>
                        </h4>
                        <h4 class="dif m_marcas">
                            <a class="princ" href="http://www.epocacosmeticos.com.br/marcas">Marcas</a>
                        </h4>
                </div>
            </div>
</div><!-- Fecha Menu Mobile -->

<div class="top100">
    <div class="top">
        <h1 class="logo">
            <a href="/">Época Cosméticos - Perfumaria</a>
        </h1>
        <div class="search">
            <fieldset class="busca">
            <form role="search" class="custom" method="get" id="searchform" action="<?php echo home_url('/'); ?>">
                <input type="text" value="" name="s" id="s-top" placeholder="<?php _e('Buscar...', 'revivaltheme'); ?>">
                <input type="button" class="btn-buscar" value="Buscar" id="ftBtn7e7000330a95461d84d51b4777618bb8">
            </form>
            </fieldset>
        </div>
        <!-- search -->
        <div class="regua">
            <div class="box-banner">
                <a>
                    <img width="257" height="43" complete="complete" src="http://epocacosmeticos.vteximg.com.br/arquivos/ids/155208/banner_regua_topo_281_28.png?v=635339337063370000" alt="bannerReguaTopo" id="ihttp://epocacosmeticos.vteximg.com.br/arquivos/ids/155208/banner_regua_topo_281_28.png?v=635339337063370000">
                </a>
            </div>
        </div>
        <!-- regua -->
        <a class="carrinho" href="/site/carrinho.aspx">
            <div class="portal-totalizers-ref">
                <div class="amount-items-in-cart">
                    <div class="cartInfoWrapper">
                        <span class="title">
                            <span id="MostraTextoXml1">Resumo do Carrinho</span>
                        </span>
                        <ul class="cart-info">
                            <li class="amount-products">
                                <strong>
                                    <span id="MostraTextoXml2">Total de Produtos:</span>
                                </strong>
                                <em class="amount-products-em">0</em>
                            </li>
                            <li class="amount-items">
                                <strong>
                                    <span id="MostraTextoXml3"></span>
                                </strong>
                                <em class="amount-items-em"></em>
                            </li>
                            <li class="amount-kits">
                                <strong><span id="MostraTextoXml4">Total de Kits:</span></strong>
                                <em class="amount-kits-em"></em>
                            </li>
                            <li class="total-cart">
                                <strong><span id="MostraTextoXml5">Valor Total:</span></strong>
                                <em class="total-cart-em">R$ 0,00</em>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <span class="item"></span>
        </a>
        <!-- carrinho -->
    </div>
    <!-- top -->
</div>

<div class="menu100">
    <div class="menu">
        <h4 class="item_menu perfumes">
            <a class="princ" href="http://www.epocacosmeticos.com.br/perfumes">Perfumes</a>
        </h4>
        <h4 class="item_menu maquiagem">
            <a class="princ" href="http://www.epocacosmeticos.com.br/maquiagem">Maquiagem</a>
        </h4>
        <h4 class="item_menu cabelos">
            <a class="princ" href="http://www.epocacosmeticos.com.br/cabelos">Cabelos</a>
        </h4>
        <h4 class="item_menu dermocosmeticos">
            <a class="princ" href="http://www.epocacosmeticos.com.br/dermocosmeticos">Dermocosm&eacute;ticos</a>
        </h4>
        <h4 class="item_menu tratamento">
            <a class="princ" href="http://www.epocacosmeticos.com.br/tratamentos">Tratamentos</a>
        </h4>
        <h4 class="item_menu corpoBanho">
            <a class="princ" href="http://www.epocacosmeticos.com.br/corpo-e-banho">Corpo e Banho</a>
        </h4>
        <h4 class="item_menu solares">
            <a class="princ" href="http://www.epocacosmeticos.com.br/ofertas">Ofertas</a>

        </h4>
        <h4 class="dif m_marcas">
            <a class="princ" href="http://www.epocacosmeticos.com.br/marcas">Marcas</a>
        </h4>
    </div>
</div>