<style type="text/css">
    body{overflow-x: hidden; }
    h2{letter-spacing:0px!important; padding:0!important}
    .top100 .top h1.logo{margin:11px 5px 0 40px!important;}
    .cont_footer .footer_baixo .forma_pagamento{float:left!important;margin-left:-40px!important;}
    .footer_social{display:none!important;}
    .menu100 .menu h2.item_menu, .menu100 .menu h2.dif, .menu{background-color:#262626; }
    a.princ{margin:0 0 0 35px!important;}
    .menu100 .menu{width:600px!important; height: 72px!important;}
    .cont_footer .footer_baixo .footer_baixo_baixo .endereco{width:270px!important;}
    .cont_footer .footer_cima .footer_menu .col_footer{width:144px!important; margin-right: 0px!important;}
    .menu100 .menu h2.item_menu, .menu100 .menu h2.dif, .menu{margin:0 0 0 -10px !important}
    .menu100{height: 72px!important;}
    .cont_footer .footer_baixo .footer_baixo_baixo{height:120px!important;}
    .cont_footer .footer_baixo .selos{width:auto!important;}
</style>
<div class="header_flutuante">
    <div class="top100">
        <div class="top">
            <h1 class="logo"><a href="/">Época Cosméticos - Perfumaria</a></h1>

        <div class="search">
            <fieldset class="busca">
            <form role="search" class="custom" method="get" id="searchform" action="<?php echo home_url('/'); ?>">
                <input type="text" value="" name="s" id="s-top" placeholder="<?php _e('Buscar...', 'revivaltheme'); ?>">
                <input type="button" class="btn-buscar" value="Buscar" id="ftBtn7e7000330a95461d84d51b4777618bb8">
            </form>
            </fieldset>
        </div>



        </div> <!-- top -->
    </div> <!-- top100 -->
</div> <!-- header_flutuante -->

<div class="topbar100">
    <div class="topbar"></div>
</div><!-- Final Login area -->

<div class="top100">
    <div class="top">
        <h1 class="logo">
            <a href="/">Época Cosméticos - Perfumaria</a>
        </h1>
        <div class="search">
            <fieldset class="busca">
            <form role="search" class="custom" method="get" id="searchform" action="<?php echo home_url('/'); ?>">
                <input type="text" value="" name="s" id="s-top" placeholder="<?php _e('Buscar...', 'revivaltheme'); ?>">
                <input type="button" class="btn-buscar" value="Buscar" id="ftBtn7e7000330a95461d84d51b4777618bb8">
            </form>
            </fieldset>
        </div>
        <!-- search -->

    </div>
    <!-- top -->
</div>
<div class="menu100">
    <div class="menu">
        <h2 class="item_menu perfumes">
            <a class="princ" href="http://www.epocacosmeticos.com.br/perfumes">Perfumes</a>
        </h2>
        <h2 class="item_menu maquiagem">
            <a class="princ" href="http://www.epocacosmeticos.com.br/maquiagem">Maquiagem</a>
        </h2>
        <h2 class="item_menu cabelos">
            <a class="princ" href="http://www.epocacosmeticos.com.br/cabelos">Cabelos</a>
        </h2>
        <h2 class="item_menu dermocosmeticos">
            <a class="princ" href="http://www.epocacosmeticos.com.br/dermocosmeticos">Dermocosm&eacute;ticos</a>
        </h2>
        <h2 class="item_menu tratamento">
            <a class="princ" href="http://www.epocacosmeticos.com.br/tratamentos">Tratamentos</a>
        </h2>

        <h2 class="item_menu corpoBanho">
            <a class="princ" href="http://www.epocacosmeticos.com.br/corpo-e-banho">Corpo e Banho</a>
        </h2>
        <h2 class="item_menu solares">
            <a class="princ" href="http://www.epocacosmeticos.com.br/ofertas">Ofertas</a>

        </h2>
        <h2 class="dif m_marcas">
            <a class="princ" href="http://www.epocacosmeticos.com.br/marcas">Marcas</a>
        </h2>
    </div>
</div>