<?php
$comments_number = get_option('revivaltheme_comments_number');
$comment_count = get_comment_count($post->ID);

if (('0' != $comments_number ) && comments_open() && $comment_count['approved'] > 0) { ?>

    <div class="discuss">

    <?php
    $args = array(
    'number' => $comments_number,
    'post_id' => $post->ID,
    'status' => 'approve'
    );
    $comments = get_comments($args);
    foreach($comments as $comment) {
        ?>
        <div class="comments-meta<?php $show_avatars = get_option('show_avatars'); $comments_number = get_comments_number(); if ($comments_number == 0 && $show_avatars == '0') { echo ' text-align-center'; } ?>">
            <?php if ($show_avatars == '1') { ?>
            <div class="comments-meta-avatar"><?php echo get_avatar( $comment->comment_author_email , '60'); ?></div>
            <div class="comments-meta-comment">
                <?php } ?>
                <span class="comments-meta-author"><?php echo $comment->comment_author; ?></span> <span class="comments-quote"><i class="i-quote"></i></span>
                <?php echo $comment->comment_content; ?>
                <?php if ($show_avatars == '1') { ?>
            </div>
        <?php } ?>
        </div>
    <?php } ?>

        <div class="comments-meta-all">
            <a href="<?php the_permalink() ?>/#comments" title="<?php _e('View all', 'revivaltheme'); ?> <?php  comments_number(); ?>"><?php echo get_comments_number( '0', '1', '%' )?><br /><i class="i-down-open-big"></i></a>
        </div>

     </div>

<?php } ?>