<style type="text/css">
    body{overflow-x: hidden; }
    h2{letter-spacing:0px!important; padding:0!important}
    .top100 .top h1.logo{margin:11px 5px 0 15px!important;}
    .cont_footer .footer_baixo .forma_pagamento{float:left!important;margin-left:-40px!important;}
    .footer_social{display:none!important;}
    .menu100 .menu h2.item_menu, .menu100 .menu h2.dif, .menu{background-color:#262626; }
    a.princ{margin:0 0 0 35px!important;}
    .menu100 .menu{width:600px!important; height: 72px!important;}
    .cont_footer .footer_baixo .footer_baixo_baixo .endereco{width:270px!important;}
    .cont_footer .footer_cima .footer_menu .col_footer{width:144px!important; margin-right: 0px!important;}
    .menu100 .menu h2.item_menu, .menu100 .menu h2.dif, .menu{margin:0 0 0 -10px !important}
    .menu100{height: 72px!important;}
    .cont_footer .footer_baixo .footer_baixo_baixo{height:120px!important;}
    .cont_footer .footer_baixo .selos{width:auto!important;}
    .top100 .top .search, .top100 .top .search fieldset{width:auto!important;}
    .top100 .top .search{margin:25px 0 0 20px!important;}
</style>

<div class="top100">
    <div class="top">
        <h1 class="logo">
            <a href="/">Época Cosméticos - Perfumaria</a>
        </h1>
        <div class="search">
            <fieldset class="busca">
            <form role="search" class="custom" method="get" id="searchform" action="<?php echo home_url('/'); ?>">
                <input type="text" value="" name="s" id="s-top" placeholder="<?php _e('Buscar...', 'revivaltheme'); ?>">
                <input type="button" class="btn-buscar" value="Buscar" id="ftBtn7e7000330a95461d84d51b4777618bb8">
            </form>
            </fieldset>
        </div>
        <!-- search -->

    </div>
    <!-- top -->
</div>
