<div class="row collapse">

<div class="header-logo animated fadeIn dark large-4 medium-12 small-12 columns">

<?php $logo = get_option( 'revivaltheme_logo_upload' );
if ($logo !== '') { ?>

    <span class="logo">
                <a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php echo $logo; ?>"  alt="<?php bloginfo('name'); ?>" /></a>
            </span>
<?php } else { ?>

    <span class="text-logo">

                <h1>
                    <a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a>

                </h1>

                <h4><?php echo get_bloginfo ('description');  ?></h4>

            </span>

<?php } ?>




</div>

<div class="large-8 medium-12 small-12 columns">

    <nav id="primary" class="colorful dark top-bar" data-topbar>

        <ul class="title-area">
            <li class="name"></li>


            <li class="toggle-topbar menu-icon"><a href="#"><span><i class="i-down-open-big"></i> <?php _e('Menu', 'revivaltheme') ?></span></a></li>

            </ul>

            <section class="top-bar-section">

            <?php
            wp_nav_menu( array(
                'theme_location' => 'primary',
                'container' => false,
                'depth' => 3,
                'items_wrap' => '<ul class="right">%3$s</ul>',
                'fallback_cb' => 'revivaltheme_menu_fallback', // workaround to show a message to set up a menu
                'walker' => new revivaltheme_walker( array(
                        'in_top_bar' => true,
                        'item_type' => 'li'
                    ) ),
            ) );
            ?>
            </section>


    </nav>

</div>

</div>