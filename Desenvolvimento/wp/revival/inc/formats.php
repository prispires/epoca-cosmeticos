<?php
$detect = new Mobile_Detect;
$medium_full = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium_full');
$large_full = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large_full');
$disable_featured_image = get_field('disable_featured_image');
$additional = get_field( 'additional_info' );
?>

<?php $gallery=get_field( 'top_gallery' ); if ( ( has_post_format( 'gallery' )) && ( $gallery !== '')) { ?>

        <div class="media-holder animated fadeInDown">

        <?php if (is_single ()) { get_template_part('format/format', 'gallery'); } else { get_template_part('layout/loop', 'gallery_slider'); } ?>

        </div>


<?php } elseif ( has_post_format( 'video' )) { ?>

        <div class="media-holder animated fadeInDown">

            <?php get_template_part('format/format', 'video'); ?>

        </div>

<?php } elseif ( ( has_post_format( 'image' )) && ( has_post_thumbnail() )) { ?>


        <div class="media-holder animated fadeInDown">

            <?php get_template_part('format/format', 'image_large'); ?>

        </div>


<?php } elseif ( has_post_format( 'audio' )) { ?>


    <div class="media-holder animated fadeInDown">

        <?php get_template_part('format/format', 'audio'); ?>

    </div>


<?php } elseif ( '' != get_the_post_thumbnail() && !$disable_featured_image ) { ?>

    <div class="media-holder animated fadeInDown">

        <?php get_template_part('format/format', 'image_large'); ?>

    </div>

<?php } elseif ( has_post_format( 'status' )) { ?>

    <div class="media-holder animated fadeInDown">

        <?php get_template_part('format/format', 'status'); ?>

    </div>

<?php } else { echo '<div class="divider-small"></div>';  }  ?>


    <?php
    if ($additional) { ?>
        <?php echo '<div class="additional">';
        echo $additional;
        echo '</div>';
        ?>
    <?php } ?>