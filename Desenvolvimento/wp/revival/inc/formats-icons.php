<?php if ( has_post_format( 'aside' )) { ?>

    <a href="<?php the_permalink(); ?>" class="info"><i class="i-pencil"></i></a>

<?php } elseif ( has_post_format( 'gallery' )) { ?>

    <a href="<?php the_permalink(); ?>"><i class="i-picture-1"></i></a>

<?php } elseif ( has_post_format( 'image' ) && (has_post_thumbnail()) ) { ?>

<?php $large = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large_full'); ?>

    <a href="<?php echo $large[0]; ?>" title="<?php revivaltheme_thumbnail_title(); ?>" class="info light"><i class="i-picture"></i></a>

<?php } elseif ( has_post_format( 'link' )) { ?>

    <a href="<?php the_permalink(); ?>"><i class="i-link"></i></a>

<?php } elseif ( has_post_format( 'quote' )) { ?>

    <a href="<?php the_permalink(); ?>"><i class="i-quote"></i></a>

<?php } elseif ( has_post_format( 'status' )) { ?>

    <a href="<?php the_permalink(); ?>"><i class="i-twitter"></i></a>

<?php } elseif ( has_post_format( 'audio' )) { ?>

    <a href="<?php the_permalink(); ?>"><i class="i-soundcloud"></i></a>


<?php } elseif ( has_post_format( 'video' )) {
    $video_youtube = get_field( 'video_youtube' );
    $video_vimeo = get_field( 'video_vimeo' ); ?>

    <?php if ($video_youtube) { ?>

    <a href="<?php the_permalink(); ?>"><i class="i-youtube"></i></a>

    <?php } elseif ($video_vimeo) { ?>

    <a href="<?php the_permalink(); ?>"><i class="i-vimeo"></i></a>

    <?php } else { ?>

    <a href="<?php the_permalink(); ?>"><i class="i-video"></i></a>

    <?php } ?>

<?php } else { ?>

    <a href="<?php the_permalink(); ?>" class="info"><i class="i-doc-text"></i></a>

<?php } ?>