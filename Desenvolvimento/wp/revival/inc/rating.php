<?php
$score_main = get_field( 'total_score' );
$review_criteria = get_field( 'review_criteria' );
$score = array();
if (!$review_criteria) { ?>

    <div class="rating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
        <div class="star-rating" title="<?php printf( __( 'Rated %s out of 5', 'revivaltheme' ), $score_main ); ?>">
			<span style="width:<?php echo ( ( $score_main / 5 ) * 100 ); ?>%">
				<strong itemprop="ratingValue" class="rating"><?php echo esc_html( $score_main ); ?></strong> <?php _e( 'out of 5', 'revivaltheme' ); ?>
			</span>
        </div>

    </div>

<?php } else {


    if ( $review_criteria ){
        foreach( $review_criteria as $key => $element ){
            $score[$key] = $element['criteria_score'];
        }

// Get total score if no one was defined
        $score_elements = count( $score );
        $score_sum = array_sum( $score );
        $score_total = $score_sum / $score_elements;
        ?>

        <div class="rating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
            <div class="star-rating" title="<?php printf( __( 'Rated %s out of 5', 'revivaltheme' ), $score_total ); ?>">
			<span style="width:<?php echo ( ( $score_total / 5 ) * 100 ); ?>%">
				<strong itemprop="ratingValue" class="rating"><?php echo esc_html( $score_main ); ?></strong> <?php _e( 'out of 5', 'revivaltheme' ); ?>
			</span>
            </div>

        </div>

    <?php }} ?>