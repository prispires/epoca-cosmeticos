<div class="colorful row">
    <div class="large-10 large-centered columns">
        <?php /* Display navigation to next/previous pages when applicable */ ?>
        <?php if ( function_exists('revivaltheme_pagination') ) { revivaltheme_pagination(); } else if ( is_paged() ) { ?>
            <nav id="post-nav">
                <div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'revivaltheme' ) ); ?></div>
                <div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'revivaltheme' ) ); ?></div>
            </nav>
        <?php } ?>
    </div>
</div>