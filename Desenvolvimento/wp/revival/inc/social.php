<div id="social-container" class="white">

    <?php $newsticker = get_option('revivaltheme_newsticker' ); if ( $newsticker != '') { ?>

    <div class="right">

    <?php  } else { ?>

    <div class="left">

    <?php } ?>

    <div class="social-item">

        <div class="icon search">

            <a href="#mini-search" class="search-button"><i class="i-search"></i></a>

        </div>

    </div>

    <?php $value = get_option( 'revivaltheme_rss_url' );
    if ($value !== '') { ?>
        <div class="social-item">
            <div class="icon rss">
                <a class="social" href="<?php echo $value; ?>" target="_blank" title="RSS"><i class="i-rss"></i></a>
            </div>
        </div>
    <?php } ?>




    <?php $value = get_option( 'revivaltheme_email' );
    if ($value !== '') { ?>
        <div class="social-item">
            <div class="icon mail">
                <a class="social" href="mailto:<?php echo $value; ?>"><i class="i-mail"></i></a>
            </div>
        </div>
    <?php } ?>

    <?php $value = get_option( 'revivaltheme_skype' );
    if ($value !== '') { ?>
        <div class="social-item">
            <div class="icon skype">
                <a class="social" href="skype:<?php echo $value; ?>?call" target="_blank" title="Skype"><i class="i-skype"></i></a>
            </div>
        </div>
    <?php } ?>


    <?php if ( $newsticker != '') { } else { ?>
    </div>
    <div class="right">
    <?php }?>


    <?php $value = get_option( 'revivaltheme_github_url' );
        if ($value !== '') { ?>
        <div class="social-item">
           <div class="icon github"><a class="social" href="<?php echo $value; ?>" target="_blank" title="Github"><i class="i-github"></i></a>
           </div>
        </div>
    <?php } ?>

    <?php $value = get_option( 'revivaltheme_vkontakte_url' );
        if ($value !== '') { ?>
        <div class="social-item">
           <div class="icon vkontakte"><a class="social" href="<?php echo $value; ?>" target="_blank" title="Vkontakte"><i class="i-vkontakte"></i></a>
           </div>
        </div>
    <?php } ?>

    <?php $value = get_option( 'revivaltheme_instagram_url' );
    if ($value !== '') { ?>
        <div class="social-item">
            <div class="icon instagram"><a class="social" href="<?php echo $value; ?>" target="_blank" title="Instagram"><i class="i-instagram"></i></a>
            </div>
        </div>
    <?php } ?>

    <?php $value = get_option( 'revivaltheme_linkedin_url' );
        if ($value !== '') { ?>
        <div class="social-item">
            <div class="icon linkedin">
                <a class="social" href="<?php echo $value; ?>" target="_blank" title="Linkedin"><i class="i-linkedin"></i></a>
            </div>
        </div>
    <?php } ?>

    <?php $value = get_option( 'revivaltheme_google_url' );
        if ($value !== '') { ?>
        <div class="social-item">
            <div class="icon gplus">
                <a class="social" href="<?php echo $value; ?>" target="_blank" title="Google+"><i class="i-googleplus"></i></a>
            </div>
        </div>
    <?php } ?>

    <?php $value = get_option( 'revivaltheme_youtube_url' );
        if ($value !== '') { ?>
        <div class="social-item">
            <div class="icon youtube">
                <a class="social" href="<?php echo $value; ?>" target="_blank" title="Youtube"><i class="i-youtube"></i></a>
            </div>
        </div>
    <?php } ?>

    <?php $value = get_option( 'revivaltheme_pinterest_url' );
        if ($value !== '') { ?>
        <div class="social-item">
            <div class="icon pinterest">
                <a class="social" href="<?php echo $value; ?>" target="_blank" title="Pinterest"><i class="i-pinterest"></i></a>
            </div>
        </div>
    <?php } ?>

    <?php $value = get_option( 'revivaltheme_twitter_url' );
        if ($value !== '') { ?>
        <div class="social-item">
            <div class="icon twitter">
                <a class="social" href="<?php echo $value; ?>" target="_blank" title="Twitter"><i class="i-twitter"></i></a>
            </div>
        </div>
    <?php } ?>

    <?php $value = get_option( 'revivaltheme_facebook_url' );
        if ($value !== '') { ?>
        <div class="social-item">
            <div class="icon facebook">
                <a class="social" href="<?php echo $value; ?>" target="_blank" title="Facebook"><i class="i-facebook"></i></a>
            </div>
        </div>
    <?php } ?>

</div>

</div>