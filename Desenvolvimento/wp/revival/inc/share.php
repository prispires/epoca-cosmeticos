<!-- Share Buttons -->

<?php $vk = get_option( 'revivaltheme_vkontakte_url' ); ?>
<div id="share">
    <ul class="inline-list <?php if ($vk !== '') { ?>wide<?php } ?>">

        <li class="facebook animated fadeInUp">
            <a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" target="blank"><i class="i-facebook"></i></a>
        </li>


        <li class="twitter animated fadeInDown">
            <a href="https://twitter.com/intent/tweet?original_referer=<?php the_permalink(); ?>&amp;text=<?php the_title(); ?>&tw_p=tweetbutton&url=<?php the_permalink(); ?>&via=<?php bloginfo( 'name' ); ?>" target="_blank"><i class="i-twitter"></i></a>
        </li>


        <?php
        if ($vk !== '') { ?>
            <li class="vkontakte">
                <a href="http://vk.com/share.php?url=<?php the_permalink();?>" target="_blank"><i class="i-vkontakte"></i></a>
            </li>
        <?php } ?>


        <li class="pinterest animated fadeInUp">
            <?php $large_full = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large_full' ); ?>
            <a href="//pinterest.com/pin/create/button/?url=<?php the_permalink();?>&amp;media=<?php echo $large_full[0]; ?>&amp;description=<?php the_title(); ?>" target="_blank"><i class="i-pinterest"></i></a>
        </li>

        <li class="gplus animated fadeInDown">
            <a href="https://plusone.google.com/_/+1/confirm?hl=en-US&amp;url=<?php the_permalink() ?>" target="_blank"><i class="i-googleplus"></i></a>
        </li>


    </ul>
</div>