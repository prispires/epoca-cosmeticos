<div class="icon-single">

    <?php if ( has_post_format( 'aside' )) { ?>

    <i class="i-lamp"></i>

    <?php } elseif ( has_post_format( 'gallery' )) { ?>

        <i class="i-picture"></i>

    <?php } elseif ( has_post_format( 'image' )) { ?>

        <i class="i-camera-alt"></i>

    <?php } elseif ( has_post_format( 'link' )) { ?>

        <a href="<?php the_permalink(); ?>" class="info"><i class="i-link"></i></a>

    <?php } elseif ( has_post_format( 'quote' )) { ?>

        <i class="i-quote"></i>

    <?php } elseif ( has_post_format( 'status' )) { ?>

        <i class="i-heart"></i>

    <?php } elseif ( has_post_format( 'audio' )) {  ?>

        <i class="i-volume-up"></i>


    <?php } elseif ( has_post_format( 'video' )) { ?>

        <i class="i-video"></i>

    <?php } else { ?>

        <i class="i-doc-text-inv"></i>

    <?php } ?>

</div>