<?php the_sub_field('criteria_header'); ?>

<strong><?php $score_criteria = get_sub_field('criteria_score') ?></strong>

<div class="rating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">

    <div class="star-rating" title="<?php printf( __( 'Rated %s out of 5', 'revivaltheme' ), $score_criteria); ?>">
        <span style="width:<?php echo ( $score_criteria / 5 ) * 100; ?>%">
            <strong itemprop="ratingValue" class="rating"><?php echo esc_html( $score_criteria ); ?></strong> <?php _e( 'out of 5', 'revivaltheme' ); ?>
        </span>
    </div>

</div>
