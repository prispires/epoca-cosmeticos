<div class="text-holder">

    <?php if ( has_post_format( 'quote' ) ) {
        echo '<div class="left quote"><i class="i-quote"></i></div>';
    } elseif ( has_post_format( 'status' ) || has_post_format( 'aside' ) ) {
    } elseif ( has_post_format( 'link' )) { ?>
        <h2 class="entry-title"><?php revivaltheme_link_title() ?></h2>
        <?php revivaltheme_link_subtitle() ?>
    <?php  } else { ?>
        <h2 class="entry-title"><a href="<?php the_permalink(); ?>">
            <?php $short_title = the_title("","",false);
            $short_title_2 = mb_substr($short_title,0,55);
            echo $short_title_2;
            if($short_title_2!=$short_title) { echo "..."; } ?>
        </a></h2>
    <?php } ?>

    <?php
    //if ( get_field('enable_review') == '1' ) { ?>

        <!--<span class="side-score"><?php //get_template_part( 'inc/rating'); ?></span>-->

    <?php //}  ?>
    <?php if(function_exists('the_ratings')) { the_ratings(); } ?>
    <?php //rw_the_post_rating(); ?>
    <p class="excerpt-text"><?php revivaltheme_excerpt(200); ?></p>


</div>

<?php $hide_meta = get_option( 'revivaltheme_hide_meta' ); if ( has_post_format( 'quote' )) { echo'<div class="divider-small"></div>'; } elseif ($hide_meta == 'hide_all' || $hide_meta == 'show_author' || $hide_meta == 'show_author_comments' ) { echo '<div class="divider"></div>'; } else { ?><div class="date-meta"><?php revivaltheme_date(); ?></div><?php } ?>

<?php get_template_part( 'inc/discuss' ); ?>
