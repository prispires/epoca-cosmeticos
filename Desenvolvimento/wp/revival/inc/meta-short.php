<?php $hide_meta = get_option( 'revivaltheme_hide_meta' ); ?>

<div class="entry-date">

    <?php if ($hide_meta == 'show_date' || $hide_meta == 'show_date_comments') { } else { ?>
        <span class="entry-author"><i class="i-pencil">&nbsp;</i><?php _e( 'Author:', 'revivaltheme' ); ?>&nbsp;<?php echo get_the_author_link() ?></span>&nbsp;

    <?php } ?>

    <?php if ( $hide_meta == 'show_author' || $hide_meta == 'show_author_comments') { } else { ?>
        <time datetime="<?php echo get_the_time('c'); ?>">
            <i class="i-clock">&nbsp;</i><?php echo get_the_date(''); ?>&nbsp;
        </time>&nbsp;
    <?php } ?>

    <?php if ($hide_meta == 'show_date' || $hide_meta == 'show_author_date' || $hide_meta == 'show_author') { } else { ?>
        <a href="<?php echo get_comments_link();?>"><i class="i-comment">&nbsp;</i>
            <?php echo get_comments_number( '0', '1', '%' )?>
        </a>&nbsp;
    <?php } ?>

</div>