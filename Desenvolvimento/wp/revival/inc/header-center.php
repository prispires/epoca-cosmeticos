<div class="header-logo dark">

<?php $logo = get_option( 'revivaltheme_logo_upload' );
if ($logo !== '') { ?>

    <span class="logo">
                <a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php echo $logo; ?>"  alt="<?php bloginfo('name'); ?>" /></a>
            </span>
<?php } else { ?>

    <span class="text-logo animated fadeInRight">

                <h1>
                    <a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a>

                </h1>

                <h4 class="animated fadeInLeftBig"><?php echo get_bloginfo ('description');  ?></h4>

            </span>

<?php } ?>

<?php if ( function_exists('is_active_sidebar') && is_active_sidebar(17) ) { ?>
    <div class="sidebar-icon"><a class="right-off-canvas-toggle"><i class="i-menu"></i></a></div>
<?php } ?>

</div>


<nav id="primary" class="colorful dark top-bar" data-topbar>

    <ul class="title-area">
        <li class="name"></li>


        <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>

        </ul>

        <section class="top-bar-section">

        <?php
        wp_nav_menu( array(
            'theme_location' => 'primary',
            'container' => false,
            'depth' => 3,
            'items_wrap' => '<ul>%3$s</ul>',
            'fallback_cb' => 'revivaltheme_menu_fallback', // workaround to show a message to set up a menu
            'walker' => new revivaltheme_walker( array(
                    'in_top_bar' => true,
                    'item_type' => 'li'
                ) ),
        ) );
        ?>
        </section>


</nav>