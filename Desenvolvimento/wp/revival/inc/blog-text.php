<?php $hide_meta = get_option( 'revivaltheme_hide_meta' ); ?>

<?php if ( has_post_format( 'quote' ) || has_post_format( 'status' ) || has_post_format( 'aside' ) ) {}

elseif ( has_post_format( 'link' )) { ?>
    <h2 class="entry-title"><?php revivaltheme_link_title() ?></h2>
<?php  } else { ?>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>">
            <?php $short_title = the_title("","",false);
            $short_title_2 = mb_substr($short_title,0,55);
            echo $short_title_2;
            if($short_title_2!=$short_title) { echo "..."; } ?>
        </a></h2>
<?php } ?>

<?php echo revivaltheme_content(''); ?>

<?php if ( has_post_format( 'status' ) || has_post_format( 'quote' ) || ($hide_meta == 'hide_all') ) { } else { ?><?php get_template_part( 'inc/meta', 'short'); ?><?php } ?>