<?php $hide_meta = get_option( 'revivaltheme_hide_meta' ); ?>

<?php if ( $hide_meta !== 'hide_all' ) { ?>

<ul class="aside-meta hide-for-medium-down">

    <?php if ( $hide_meta == 'show_author' || $hide_meta == 'show_author_comments') { } else { ?>
        <li>
            <time datetime="<?php echo get_the_time('c') ?>"><?php echo get_the_date(''); ?></time>
        </li>
    <?php } ?>

    <?php if ($hide_meta == 'show_date' || $hide_meta == 'show_date_comments') { } else { ?>
        <li>
            <?php _e( 'Author:', 'revivaltheme' ); ?>&nbsp;<?php the_author_posts_link();?>
        </li>
    <?php } ?>

    <?php if ($hide_meta == 'show_date' || $hide_meta == 'show_author_date' || $hide_meta == 'show_author') { } else { ?>
        <li>
        <?php comments_popup_link(__('No responses', 'revivaltheme'), __('One response', 'revivaltheme'), __('% responses', 'revivaltheme') ); ?>
        </li>
    <?php }?>

    <?php if ($hide_meta == 'show_all') { ?>
       <li>
            <?php the_tags(__('Tags: ', 'revivaltheme')); ?>
        </li>
    <?php }?>

</ul>

<?php  } else  {}?>