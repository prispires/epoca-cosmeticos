<header class="entry-header">

    <?php if (! is_page()) { ?>

        <h3 class="category-title">
            <?php
            global $post;
            $cat=get_the_category( $post->ID );
            $catid = $cat[0]-> term_id;
            $category_icon = get_field( 'category_icon', 'category_'.$catid.'' );
            ?>

            <a href="<?php echo get_category_link($catid); ?>">

                <?php if ( $category_icon ) {?>
                    <?php echo $category_icon; ?>
                <?php } ?>

                <?php echo get_cat_name($catid); ?>

            </a>

        </h3>

    <?php } ?>

    <?php if ( has_post_format( 'link' )) { ?>

        <h1 class="entry-title"><?php revivaltheme_link_title() ?></h1>

        <h2 class="subheader entry-title"><?php revivaltheme_link_subtitle() ?></h2>

    <?php } elseif ( has_post_format( 'quote' ) || has_post_format( 'status' ) || has_post_format( 'aside' )) { } else { ?>

        <h1 class="entry-title"><?php the_title(); ?></h1>

    <?php } ?>

</header>