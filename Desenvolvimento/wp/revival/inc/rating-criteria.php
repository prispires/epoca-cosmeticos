<?php
// Post Rating - Defined by post author in admin post edit page
if ( get_field('enable_review') == '1' && ((get_field('total_score_summary'))&&(get_field('total_score_header'))) || (get_field('review_criteria') )) { ?>

    <div class="rating-bg">

        <?php get_template_part( 'inc/rating'); ?>

        <?php if ( get_field('total_score_header')) { ?>

            <h3><?php the_field('total_score_header'); ?></h3>

        <?php } ?>



    </div>

    <div class="rating-panel">

        <div class="rating-header">

            <?php
            if ( get_field('total_score_summary') ) { ?>

                <p class="total-summary">
                    <?php the_field('total_score_summary');  ?>
                </p>

            <?php } ?>

        </div>


        <?php if(get_field('review_criteria')): ?>


                <?php while(has_sub_field('review_criteria')): ?>

                    <div class="rating-criteria">

                        <div class="rating-criteria-score">

                            <?php get_template_part( 'inc/rating', 'criteria_star'); ?>

                        </div>


                        <div class="summary">
                            <?php the_sub_field('criteria_summary'); ?>
                        </div>

                    </div>

                <?php endwhile; ?>

        <?php endif; ?>

    </div>


<?php }  ?>