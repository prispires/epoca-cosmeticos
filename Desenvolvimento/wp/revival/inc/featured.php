<?php $detect = new Mobile_Detect;
$featured = get_field('show_featured');
$small_full = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'small_full');
$medium_full = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium_full');
$small = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'small');
?>

<?php $gallery=get_field( 'top_gallery' );
if ( ( has_post_format( 'gallery' )) && ( $gallery !== '') ) { ?>


        <?php $images=get_field('top_gallery'); if( $images ): ?>


            <?php $c=0; foreach( $images as $image ): $c++; ?>


            <?php if ($c == 1) { ?>

                <div class="media-holder has-hover">

                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="overlay-link"></a>

                    <?php if ( $detect->isMobile() && !$detect->isTablet() ) { ?>
                        <img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['title']; ?>" />
                    <?php } elseif ( $featured ) {?>
                        <img src="<?php echo $image['sizes']['medium_full']; ?>" alt="<?php echo $image['title']; ?>" />
                    <?php } else { ?>
                    <img src="<?php echo $image['sizes']['small_full']; ?>" alt="<?php echo $image['title']; ?>" />
                    <?php } ?>


                    <div class="is-hover">
                        <div class="inner no-bg">
                            <div class="icon">
                                <a href="<?php if ( $detect->isMobile() && !$detect->isTablet() ) { ?><?php echo $image['sizes']['medium_full']; ?><?php } else { ?><?php echo $image['sizes']['large_full']; ?><?php } ?>" class="light" data-lightbox-gallery="gallery_<?php the_ID(); ?>" title="<?php echo $image['title']; ?>"> <i class="i-picture-1"></i></a>
                            </div>
                        </div>
                    </div>

                  </div>

            <?php } ?>


            <?php endforeach; ?>


        <?php endif; ?>



<?php } elseif ( has_post_format( 'audio' ) ) { ?>


    <div class="media-holder overlay">
        <?php get_template_part('format/format', 'audio'); ?>

    </div>


<?php } elseif ( has_post_format( 'video' )  ) { ?>

    <div class="media-holder">

        <?php get_template_part('format/format', 'video'); ?>

    </div>

<?php } elseif ( has_post_format( 'status' )) { ?>

    <div class="media-holder">

        <?php get_template_part('format/format', 'status'); ?>

    </div>

<?php } elseif ( '' != get_the_post_thumbnail() ) { ?>

    <div class="media-holder has-hover">

        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="overlay-link"></a>

            <?php if ( $detect->isMobile() && !$detect->isTablet() ) { ?>
                <img src="<?php echo $small[0]; ?>" alt="<?php revivaltheme_thumbnail_title();?>">
            <?php } elseif ( $featured ) {?>
                <img src="<?php echo $medium_full[0]; ?>" alt="<?php revivaltheme_thumbnail_title();?>">
            <?php } else { ?>
                <img src="<?php echo $small_full[0]; ?>" alt="<?php revivaltheme_thumbnail_title();?>">
            <?php } ?>



        <div class="is-hover">
            <div class="inner no-bg">
                <div class="icon">
                    <?php get_template_part('inc/formats', 'icons'); ?>
                </div>
            </div>
        </div>


    </div>

<?php } else {   }  ?>