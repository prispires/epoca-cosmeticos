<?php get_header(); ?>

<!-- Row for main content area -->
<div id="content" class="row collapse animated fadeIn">

    <div class="small-12 large-7 large-centered medium-8 medium-centered columns" role="main">

        <div id="post-0" class="entry-wrapper white">

            <article <?php post_class() ?>>
                <header class="entry-header">
                    <h1 class="entry-title"><?php _e('File Not Found', 'revivaltheme'); ?></h1>
                </header>
                <div class="entry-content">
                    <p><?php _e('The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.', 'revivaltheme'); ?></p>
                    <div class="divider-medium"></div>
                    <?php get_search_form(); ?>
                    <p><?php _e('Please try the following:', 'revivaltheme'); ?></p>
                    <ul>
                        <li><?php _e('Check your spelling', 'revivaltheme'); ?></li>
                        <li><?php printf(__('Return to the <a href="%s">home page</a>', 'revivaltheme'), home_url()); ?></li>
                        <li><?php _e('Click the <a href="javascript:history.back()">Back</a> button', 'revivaltheme'); ?></li>
                    </ul>
                </div>
            </article>

        </div>

    </div>

</div>
<?php get_footer();  ?>