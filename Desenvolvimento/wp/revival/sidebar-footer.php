<div class="row collapse footer-line">


    <div class="large-3 medium-6 small-12 columns">

        <?php dynamic_sidebar('footer-white-first'); ?>

    </div>

    <div class="large-3 medium-6 small-12 columns">

        <?php dynamic_sidebar('footer-white-second'); ?>

    </div>

    <div class="large-3 hide-for-medium-down small-12 columns">

        <?php dynamic_sidebar('footer-white-third'); ?>

    </div>

    <div class="large-3 hide-for-medium-down small-12 columns">

        <?php dynamic_sidebar('footer-white-fourth'); ?>

    </div>



</div>

<div class="row collapse dark footer-line">

    <div class="large-6 medium-12 small-12 columns">

        <?php dynamic_sidebar('footer-dark-first-wide'); ?>

    </div>

    <div class="large-6 medium-12 small-12 columns">

        <?php dynamic_sidebar('footer-dark-second-wide'); ?>

    </div>

</div>

<div class="row collapse dark">

    <div class="large-12 small-12 columns">

        <?php dynamic_sidebar('footer-fullwidth'); ?>

    </div>

</div>