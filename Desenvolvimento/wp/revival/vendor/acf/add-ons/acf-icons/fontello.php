<?php

class acf_field_fontello extends acf_field
{
	// vars
	var $settings, // will hold info such as dir / path
		$defaults; // will hold default field options

	function __construct()
	{
		$this->name = 'fontello';
		$this->label = __('Fontello Icon','revivaltheme');
		$this->category = __('Content','revivaltheme'); // Basic, Content, Choice, etc
		$this->defaults = array(
			'allow_null' 	=>	0,
			'save_format'	=>  'element',
			'default_value'	=>	'',
			'choices'		=>	array(
                'i-wordpress' => '&#xe800;&nbsp;',
                'i-vkontakte' => '&#xe801;&nbsp;',
                'i-vimeo' => '&#xe802;&nbsp;',
                'i-tumblr' => '&#xe803;&nbsp;',
                'i-twitter' => '&#xe804;&nbsp;',
                'i-stumbleupon' => '&#xe805;&nbsp;',
                'i-skype' => '&#xe806;&nbsp;',
                'i-reddit' => '&#xe807;&nbsp;',
                'i-pinterest' => '&#xe808;&nbsp;',
                'i-picasa' => '&#xe809;&nbsp;',
                'i-linkedin' => '&#xe80a;&nbsp;',
                'i-instagram' => '&#xe80b;&nbsp;',
                'i-googleplus' => '&#xe80c;&nbsp;',
                'i-github' => '&#xe80d;&nbsp;',
                'i-facebook' => '&#xe80e;&nbsp;',
                'i-flickr' => '&#xe80f;&nbsp;',
                'i-dribbble' => '&#xe810;&nbsp;',
                'i-delicious' => '&#xe811;&nbsp;',
                'i-search' => '&#xe812;&nbsp;',
                'i-mail' => '&#xe813;&nbsp;',
                'i-heart' => '&#xe814;&nbsp;',
                'i-heart-empty' => '&#xe815;&nbsp;',
                'i-video-alt' => '&#xe816;&nbsp;',
                'i-picture' => '&#xe817;&nbsp;',
                'i-camera' => '&#xe818;&nbsp;',
                'i-edit-alt' => '&#xe819;&nbsp;',
                'i-comment' => '&#xe81a;&nbsp;',
                'i-comment-alt' => '&#xe81b;&nbsp;',
                'i-paypal' => '&#xe81c;&nbsp;',
                'i-mastercard' => '&#xe81d;&nbsp;',
                'i-export' => '&#xe81e;&nbsp;',
                'i-pencil' => '&#xe81f;&nbsp;',
                'i-location' => '&#xe820;&nbsp;',
                'i-path' => '&#xe821;&nbsp;',
                'i-blogger' => '&#xe822;&nbsp;',
                'i-clock' => '&#xe823;&nbsp;',
                'i-volume' => '&#xe824;&nbsp;',
                'i-rss' => '&#xe825;&nbsp;',
                'i-cogs' => '&#xe826;&nbsp;',
                'i-cog' => '&#xe827;&nbsp;',
                'i-tags' => '&#xe828;&nbsp;',
                'i-tag' => '&#xe829;&nbsp;',
                'i-folder-close' => '&#xe82a;&nbsp;',
                'i-folder-open' => '&#xe82b;&nbsp;',
                'i-music' => '&#xe82c;&nbsp;',
                'i-quote' => '&#xe82d;&nbsp;',
                'i-visa' => '&#xe82e;&nbsp;',
                'i-youtube' => '&#xe82f;&nbsp;',
                'i-github-1' => '&#xe830;&nbsp;',
                'i-behance' => '&#xe831;&nbsp;',
                'i-glass' => '&#xe832;&nbsp;',
                'i-link' => '&#xe833;&nbsp;',
                'i-star' => '&#xe834;&nbsp;',
                'i-star-empty' => '&#xe835;&nbsp;',
                'i-attach' => '&#xe836;&nbsp;',
                'i-basket' => '&#xe837;&nbsp;',
                'i-basket-circled' => '&#xe838;&nbsp;',
                'i-info-circled' => '&#xe839;&nbsp;',
                'i-note' => '&#xe83a;&nbsp;',
                'i-align-justify' => '&#xe83b;&nbsp;',
                'i-barcode' => '&#xe83c;&nbsp;',
                'i-share' => '&#xe83d;&nbsp;',
                'i-right-open-mini' => '&#xe83e;&nbsp;',
                'i-left-open-mini' => '&#xe83f;&nbsp;',
                'i-right-open-big' => '&#xe840;&nbsp;',
                'i-left-open-big' => '&#xe841;&nbsp;',
                'i-down-open-big' => '&#xe842;&nbsp;',
                'i-up-open-big' => '&#xe843;&nbsp;',
                'i-discover' => '&#xe844;&nbsp;',
                'i-mail-circled' => '&#xe845;&nbsp;',
                'i-amex' => '&#xe846;&nbsp;',
                'i-win8' => '&#xe847;&nbsp;',
                'i-video' => '&#xe848;&nbsp;',
                'i-videocam' => '&#xe849;&nbsp;',
                'i-th-large' => '&#xe84a;&nbsp;',
                'i-th-list' => '&#xe84b;&nbsp;',
                'i-th' => '&#xe84c;&nbsp;',
                'i-view-mode' => '&#xe84d;&nbsp;',
                'i-ok' => '&#xe84e;&nbsp;',
                'i-home' => '&#xe84f;&nbsp;',
                'i-plus-circled' => '&#xe850;&nbsp;',
                'i-flag' => '&#xe851;&nbsp;',
                'i-bookmark-empty' => '&#xe852;&nbsp;',
                'i-minus-circled' => '&#xe853;&nbsp;',
                'i-bookmark' => '&#xe854;&nbsp;',
                'i-eye' => '&#xe855;&nbsp;',
                'i-bell' => '&#xe856;&nbsp;',
                'i-food' => '&#xe857;&nbsp;',
                'i-lightbulb' => '&#xe858;&nbsp;',
                'i-headphones' => '&#xe859;&nbsp;',
                'i-right-hand' => '&#xe85a;&nbsp;',
                'i-left-hand' => '&#xe85b;&nbsp;',
                'i-down-hand' => '&#xe85c;&nbsp;',
                'i-up-circled' => '&#xe85d;&nbsp;',
                'i-right-circled' => '&#xe85e;&nbsp;',
                'i-left-circled' => '&#xe85f;&nbsp;',
                'i-down-circled' => '&#xe860;&nbsp;',
                'i-up-hand' => '&#xe861;&nbsp;',
                'i-globe' => '&#xe862;&nbsp;',
                'i-cloud' => '&#xe863;&nbsp;',
                'i-signal' => '&#xe864;&nbsp;',
                'i-book' => '&#xe865;&nbsp;',
                'i-adjust' => '&#xe866;&nbsp;',
                'i-tint' => '&#xe867;&nbsp;',
                'i-glasses' => '&#xe868;&nbsp;',
                'i-address-book' => '&#xe869;&nbsp;',
                'i-smiley' => '&#xe86a;&nbsp;',
                'i-gauge' => '&#xe86b;&nbsp;',
                'i-filter' => '&#xe86c;&nbsp;',
                'i-tasks' => '&#xe86d;&nbsp;',
                'i-check-empty' => '&#xe86e;&nbsp;',
                'i-gift' => '&#xe86f;&nbsp;',
                'i-fire' => '&#xe870;&nbsp;',
                'i-magnet' => '&#xe871;&nbsp;',
                'i-chart' => '&#xe872;&nbsp;',
                'i-credit-card' => '&#xe873;&nbsp;',
                'i-megaphone' => '&#xe874;&nbsp;',
                'i-clipboard' => '&#xe875;&nbsp;',
                'i-hdd' => '&#xe876;&nbsp;',
                'i-briefcase' => '&#xe877;&nbsp;',
                'i-road' => '&#xe878;&nbsp;',
                'i-qrcode' => '&#xe879;&nbsp;',
                'i-off' => '&#xe87a;&nbsp;',
                'i-braille' => '&#xe87b;&nbsp;',
                'i-align-left' => '&#xe87c;&nbsp;',
                'i-flight' => '&#xe87d;&nbsp;',
                'i-arrows-cw' => '&#xe87e;&nbsp;',
                'i-shuffle' => '&#xe87f;&nbsp;',
                'i-record' => '&#xe880;&nbsp;',
                'i-pause' => '&#xe881;&nbsp;',
                'i-stop' => '&#xe882;&nbsp;',
                'i-adult' => '&#xe883;&nbsp;',
                'i-female' => '&#xe884;&nbsp;',
                'i-male' => '&#xe885;&nbsp;',
                'i-child' => '&#xe886;&nbsp;',
                'i-picture-1' => '&#xe887;&nbsp;',
                'i-link-1' => '&#xe888;&nbsp;',
                'i-code' => '&#xe889;&nbsp;',
                'i-feather' => '&#xe88a;&nbsp;',
                'i-pencil-1' => '&#xe88b;&nbsp;',
                'i-palette' => '&#xe88c;&nbsp;',
                'i-mobile' => '&#xe88d;&nbsp;',
                'i-monitor' => '&#xe88e;&nbsp;',
                'i-signal-1' => '&#xe88f;&nbsp;',
                'i-trophy' => '&#xe890;&nbsp;',
                'i-calendar-1' => '&#xe891;&nbsp;',
                'i-map' => '&#xe892;&nbsp;',
                'i-address' => '&#xe893;&nbsp;',
                'i-flag-1' => '&#xe894;&nbsp;',
                'i-flashlight' => '&#xe895;&nbsp;',
                'i-suitcase' => '&#xe896;&nbsp;',
                'i-briefcase-1' => '&#xe897;&nbsp;',
                'i-mouse' => '&#xe898;&nbsp;',
                'i-lifebuoy' => '&#xe899;&nbsp;',
                'i-moon' => '&#xe89a;&nbsp;',
                'i-flash' => '&#xe89b;&nbsp;',
                'i-paper-plane' => '&#xe89c;&nbsp;',
                'i-magnet-1' => '&#xe89d;&nbsp;',
                'i-brush' => '&#xe89e;&nbsp;',
                'i-chart-pie' => '&#xe89f;&nbsp;',
                'i-target' => '&#xe8a0;&nbsp;',
                'i-newspaper' => '&#xe8a1;&nbsp;',
                'i-book-open' => '&#xe8a2;&nbsp;',
                'i-coffee' => '&#xe8a3;&nbsp;',
                'i-beer' => '&#xe8a4;&nbsp;',
                'i-user-md' => '&#xe8a5;&nbsp;',
                'i-stethoscope' => '&#xe8a6;&nbsp;',
                'i-ambulance' => '&#xe8a7;&nbsp;',
                'i-medkit' => '&#xe8a8;&nbsp;',
                'i-h-sigh' => '&#xe8a9;&nbsp;',
                'i-gauge-1' => '&#xe8aa;&nbsp;',
                'i-hammer' => '&#xe8ab;&nbsp;',
                'i-sitemap' => '&#xe8ac;&nbsp;',
                'i-puzzle' => '&#xe8ad;&nbsp;',
                'i-apple' => '&#xe8ae;&nbsp;',
                'i-android' => '&#xe8af;&nbsp;',
                'i-lemon' => '&#xe8b0;&nbsp;',
                'i-key' => '&#xe8b1;&nbsp;',
                'i-moon-1' => '&#xe8b2;&nbsp;',
                'i-umbrella' => '&#xe8b3;&nbsp;',
                'i-soundcloud' => '&#xe8b4;&nbsp;',
                'i-upload-cloud' => '&#xe8b5;&nbsp;',
                'i-download-alt' => '&#xe8b6;&nbsp;',
                'i-download' => '&#xe8b7;&nbsp;',
                'i-guidedog' => '&#xe8b8;&nbsp;',
                'i-doc-text' => '&#xe8b9;&nbsp;',
                'i-doc-inv' => '&#xe8ba;&nbsp;',
                'i-doc-text-inv' => '&#xe8bb;&nbsp;',
                'i-doc' => '&#xe8bc;&nbsp;',
                'i-beaker' => '&#xe8bd;&nbsp;',
                'i-beaker-1' => '&#xe8be;&nbsp;',
                'i-emo-happy' => '&#xe8bf;&nbsp;',
                'i-emo-wink' => '&#xe8c0;&nbsp;',
                'i-emo-wink2' => '&#xe8c1;&nbsp;',
                'i-emo-unhappy' => '&#xe8c2;&nbsp;',
                'i-emo-devil' => '&#xe8c3;&nbsp;',
                'i-emo-coffee' => '&#xe8c4;&nbsp;',
                'i-mic' => '&#xe8c5;&nbsp;',
                'i-leaf' => '&#xe8c6;&nbsp;',
                'i-inbox' => '&#xe8c7;&nbsp;',
                'i-emo-sunglasses' => '&#xe8c8;&nbsp;',
                'i-emo-displeased' => '&#xe8c9;&nbsp;',
                'i-emo-beer' => '&#xe8ca;&nbsp;',
                'i-emo-thumbsup' => '&#xe8cc;&nbsp;',
                'i-emo-surprised' => '&#xe8cd;&nbsp;',
                'i-emo-tongue' => '&#xe8ce;&nbsp;',
                'i-emo-grin' => '&#xe8cf;&nbsp;',
                'i-emo-saint' => '&#xe8d0;&nbsp;',
                'i-emo-squint' => '&#xe8d2;&nbsp;',
                'i-emo-laugh' => '&#xe8d3;&nbsp;'
			)
		);

		$this->settings = array(
			'path' => apply_filters('acf/helpers/get_path', __FILE__),
			'dir' => apply_filters('acf/helpers/get_dir', __FILE__),
			'version' => '1.2'
		);


    	parent::__construct();
	}

	function create_options($field)
	{
		// defaults?
		$field = array_merge($this->defaults, $field);

		// key is needed in the field names to correctly save the data
		$key = $field['name'];


		// Create Field Options HTML
		?>
		<tr class="field_option field_option_<?php echo $this->name; ?>">
			<td class="label">
				<label><?php _e("Default Icon", 'acf'); ?></label>
			</td>
			<td>
				<div class="icon-field-wrapper">
					<div class="icon-live-preview"></div>
					<?php

					do_action('acf/create_field', array(
						'type'    =>  'select',
						'name'    =>  'fields[' . $key . '][default_value]',
						'value'   =>  $field['default_value'],
						'class'	  =>  '',
						'choices' =>  array_merge( array( 'null' => __("Select",'acf') ), $field['choices'] )
					));

					?>
				</div>
			</td>
		</tr>
		<tr class="field_option field_option_<?php echo $this->name; ?>">
			<td class="label">
				<label><?php _e("Return Value",'acf'); ?></label>
				<p class="description"><?php _e("Specify the returned value on front end", 'acf'); ?></p>
			</td>
			<td>
				<?php 
				do_action('acf/create_field', array(
					'type'	=>	'radio',
					'name'	=>	'fields['.$key.'][save_format]',
					'value'	=>	$field['save_format'],
					'choices'	=>	array(
						'element'	=>	__("Icon Element",'acf'),
						'object'	=>	__("Icon Object",'acf'),
						'class'		=>	__("Icon Class",'acf'),
					),
					'layout'	=>	'horizontal',
				));
				?>
			</td>
		</tr>

		<tr class="field_option field_option_<?php echo $this->name; ?>">
			<td class="label">
				<label><?php _e("Allow Null?",'acf'); ?></label>
			</td>
			<td>
				<?php 
				do_action('acf/create_field', array(
					'type'	=>	'radio',
					'name'	=>	'fields['.$key.'][allow_null]',
					'value'	=>	$field['allow_null'],
					'choices'	=>	array(
						1	=>	__("Yes",'acf'),
						0	=>	__("No",'acf'),
					),
					'layout'	=>	'horizontal',
				));
				?>
			</td>
		</tr>


		<?php

	}


	function create_field( $field )
	{	
		if( 'object' == $field['save_format'] )
			$field['value'] = array( $field['value']->class );

		// value must be array
		if( !is_array($field['value']) )
		{
			// perhaps this is a default value with new lines in it?
			if( strpos($field['value'], "\n") !== false )
			{
				// found multiple lines, explode it
				$field['value'] = explode("\n", $field['value']);
			}
			else
			{
				$field['value'] = array( $field['value'] );
			}
		}
		
		// trim value
		$field['value'] = array_map('trim', $field['value']);
		
		// html
		echo '<div class="icon-field-wrapper">';
		echo '<div class="icon-live-preview"></div>';
		echo '<select id="' . $field['id'] . '" class="' . $field['class'] . ' icon-select2-field" name="' . $field['name'] . '" >';
		
		// null
		if( $field['allow_null'] )
		{
			echo '<option value="null">- ' . __("Select",'acf') . ' -</option>';
		}
		
		// loop through values and add them as options
		if( is_array($field['choices']) )
		{
			foreach( $field['choices'] as $key => $value )
			{
				$selected = $this->find_selected( $key, $field['value'], $field['save_format'], $field['choices'] );
				echo '<option value="'.$key.'" '.$selected.'>'.$value.' '.$key.'</option>';
			}
		}

		echo '</select>';
		echo '</div>';
	}

	function find_selected( $needle, $haystack, $type, $choices )
	{
		switch( $type )
		{
			case 'object':
			case 'element':
				$search = array( '<i class="', '"></i>' );
				$string = str_replace( $search, '', $haystack[0] );
				break;

			case 'class':
				$string = $haystack[0];
				break;
		}

		if( $string == $needle )
			return 'selected="selected"';

		return '';
	}

	function input_admin_enqueue_scripts()
	{
		// register acf scripts
		wp_enqueue_script('acf-input-fontello-select2', $this->settings['dir'] . 'js/select2/select2.min.js', array(), $this->settings['version']);
		wp_enqueue_script('acf-input-fontello-edit-input', $this->settings['dir'] . 'js/edit_input.js', array(), $this->settings['version']);
		wp_enqueue_style('acf-input-fontello-input', $this->settings['dir'] . 'css/input.css', array(), $this->settings['version']);
		wp_enqueue_style('acf-input-fontello-select2-css', $this->settings['dir'] . 'css/select2.css', array(), $this->settings['version']);
	}


	function field_group_admin_enqueue_scripts()
	{
		// register acf scripts
		wp_enqueue_script('fontello-select2', $this->settings['dir'] . 'js/select2/select2.min.js', array(), $this->settings['version']);
		wp_enqueue_script('fontello-create-input', $this->settings['dir'] . 'js/create_input.js', array(), $this->settings['version']);
		wp_enqueue_style('acf-input-fontello-input', $this->settings['dir'] . 'css/input.css', array(), $this->settings['version']);
		wp_enqueue_style('acf-input-fontello-select2-css', $this->settings['dir'] . 'css/select2.css', array(), $this->settings['version']);
	}


	function load_value($value, $post_id, $field)
	{
		switch( $field['save_format'] )
		{
			case 'object':
				$icon_unicode = $this->defaults['choices'][ $value ];
				$value = (object) array(
						'unicode' => $icon_unicode,
						'class'	  => $value,
						'element' => '<i class="' . $value . '"></i>'
					);
				break;

			case 'unicode':
				$value = $this->defaults['choices'][ $value ];
				break;

			case 'element':
				$value = '<i class="' . $value . '"></i>';
				break;
		}

		return $value;
	}

}

new acf_field_fontello();
