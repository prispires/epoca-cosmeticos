(function($){

	$(document).on( 'change', '.field_type select', function() {

		if( $(this).val() == 'fontello' ) {
			var fontello_form = $(this).closest( '.field_form' );
			var fontello_select = $( 'select.fontawesome', fontello_form );
			setTimeout(function() {
				$( 'select.fontawesome', fontello_form ).select2({
					width : '100%'
				});
				update_preview( fontello_select, $( fontello_select ).val() );
			}, 1000);
		}

	});

	$(document).on( 'acf/field_form-open', function( e, field ) {

		element = $( 'select.fontawesome', field );

		$( element ).select2({
			width : '100%'
		});
		update_preview( element, $(element).val() );
	});

	$(document).on( 'select2-selecting', 'select.fontawesome', function( object ) {
		update_preview( this, object.val );
	});

	$(document).on( 'select2-highlight', 'select.fontawesome', function( object ) {
		update_preview( this, object.val );
	});

	$(document).on( 'select2-close', 'select.fontawesome', function( object ) {
		update_preview( this, $(this).val() );
	});

	function update_preview( element, selected ) {
		var parent = $(element).parent();
		$( '.icon-live-preview', parent ).html( '<i class=" ' + selected + '"></i>' );
	}

})(jQuery);