=== ACF: Fontello Icons ===
Contributors: themeous.com
Tags: Advanced Custom Fields, ACF, Fontello
Requires at least: 3.5
Tested up to: 3.9
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Based on 'Advanced Custom Fields: Font Awesome' by Matt Keys http://mattkeys.me/

== Description ==

Adds Fontello (http://fontello.com/) icon field type to Advanced Custom Fields.
