<?php
/*
Plugin Name: ACF: Fontello Icons
Description: Add a Fontello Icon field type to Advanced Custom Fields. Based on 'Advanced Custom Fields: Font Awesome' by Matt Keys http://mattkeys.me/
Version: 1.0
Author: Themeous
Author URI: http://themeous.com/
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/


class acf_field_fontello_plugin
{


	function __construct()
	{

		add_action('acf/register_fields', array($this, 'register_fields'));

	}


	function register_fields()
	{
		include_once('fontello.php');
	}

}

new acf_field_fontello_plugin();

?>
