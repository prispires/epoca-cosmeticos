<style type="text/css">
    #armored_website{display:none!important}
</style>

<?php get_template_part( 'inc/nav' ); ?>

<div id="footer-top" class="white hide-for-small">

    <?php get_template_part( 'sidebar', 'footer'); ?>

</div>


<div class="scroll-top">
    <div class="scroll-button colorful">
        <a href="#top">
            <i class="i-up-open-big"></i>
        </a>
    </div>
</div>

</section><!-- Wrapper End -->

<?php get_template_part( 'sidebar', 'secondary'); ?>


<a class="exit-off-canvas"></a>

</div><!-- off-canvas inner end -->
</div><!-- off-canvas end -->

<!-- Mini Search -->
<div id="mini-search" class="animated fadeInDown">
<form role="search" class="custom" method="get" id="searchform" action="<?php echo home_url('/'); ?>">

            <input type="text" value="" name="s" id="s-top" placeholder="<?php _e('Search...', 'revivaltheme'); ?>">


</form>
    <div class="info"><?php _e('type keyword and hit enter', 'revivaltheme'); ?></div>
</div>


<div class="footerTotal">
<div class="clear"></div>
<div class="cont_footer">
    <div class="footer_cima">
        
        <div class="footer_menu">
            <div class="col_footer">
                <h4>DEPARTAMENTOS</h4>
                <ul>
                    <li><a href="http://www.epocacosmeticos.com.br/perfumes">Perfumes</a></li>
                    <li><a href="http://www.epocacosmeticos.com.br/maquiagem">Maquiagem</a></li>
                    <li><a href="http://www.epocacosmeticos.com.br/cabelos">Cabelos</a></li>
                    <li><a href="http://www.epocacosmeticos.com.br/dermocosmeticos">Dermocosm&eacute;ticos</a></li>
                    <li><a href="http://www.epocacosmeticos.com.br/tratamentos">Tratamentos</a></li>
                    <li><a href="http://www.epocacosmeticos.com.br/corpo-e-banho">Corpo &amp; banho</a></li>
                    <li><a href="http://www.epocacosmeticos.com.br/solares">Solares</a></li><!--
                    <li><a href="#">Homem</a></li>
                    <li><a href="#">Ofertas</a></li> -->
                </ul>               
            </div>
            <div class="col_footer">
                <h4>INSTITUCIONAL</h4>
                <ul>
                    <li><a href="http://www.epocacosmeticos.com.br/nossaslojas">Conhe&ccedil;a a &Eacute;poca Cosm&eacute;ticos</a></li>
                    <!-- <li><a href="http://www.epocacosmeticos.com.br/nossaslojas">Nossas Lojas</a></li> -->
                    <li><a href="http://www.epocacosmeticos.com.br/centralatendimento/formas-pagamento">Formas de Pagamento</a></li>
                    <li><a href="http://www.epocacosmeticos.com.br/centralatendimento/entrega">Prazo de Entrega</a></li>
                    <li><a href="http://www.epocacosmeticos.com.br/centralatendimento/privacidade-seguranca">Privacidade e Seguran&ccedil;a</a></li>
                    <li><a href="http://www.epocacosmeticos.com.br/centralatendimento/troca-devolucao">Direito de Arrependimento </a></li>
                    <li><a href="http://epocacosmeticos.integracaoafiliados.com.br/affiliates/?utm_source=epocacosmeticos&amp;utm_medium=site" target="_blank">Programa de Afiliados</a></li>
                </ul>
            </div>
            <div class="col_footer">
                <h4>ATENDIMENTO</h4>
                <ul>
                    <li><a href="http://www.epocacosmeticos.com.br/centralatendimento">Central de Atendimento</a></li>
                    <li><a href="http://www.epocacosmeticos.com.br/login?ReturnUrl=%2faccount%2forders">Acompanhe o seu pedido</a></li>
                    <li><a href="http://www.epocacosmeticos.com.br/login?ReturnUrl=%2faccount%2forders">2&ordf; via do Boleto</a></li>
                    <li><a href="http://www.epocacosmeticos.com.br/centralatendimento/troca-devolucao">Troca e Devolu&ccedil;&atilde;o</a></li>
                    <!-- <li><a href="http://www.epocacosmeticos.com.br/nossaslojas">Alterar Senha</a></li>
                    <li><a href="http://www.epocacosmeticos.com.br/nossaslojas">Alterar E-mail</a></li>
                    <li><a href="http://www.epocacosmeticos.com.br/nossaslojas">Alterar Cadastro</a></li> -->

                </ul>
            </div>
            <div class="col_footer last">
                <h4>INFORMA&Ccedil;&Otilde;ES</h4>
                <ul>
                    <li><a href="http://www.epocacosmeticos.com.br/centralatendimento/como-comprar">Como Comprar</a></li>
                    <li><a href="http://www.epocacosmeticos.com.br/ofertas">Ofertas Imperd&iacute;veis</a></li>
                    <li><a href="http://www.epocacosmeticos.com.br/brindes">Compre e Ganhe Brindes</a></li>
                    <!-- <li><a href="http://www.epocacosmeticos.com.br/clubedocupom">Cupom de Desconto</a></li> -->
                    <li><a href="http://www.epocacosmeticos.com.br/presentes">Kits e Presentes</a></li>
                    <li><a href="http://www.epocacosmeticos.com.br/perfumes-ate99">Perfumes at&eacute; 99,</a></li>
                    <li><a href="http://www.epocacosmeticos.com.br/centralatendimento/mais-duvidas">Mais D&uacte;vidas</a></li>

                </ul>

            </div>
        </div> <!-- footer_menu -->

        <div class="footer_social">
            <h4>REDES SOCIAIS</h4>
            <ul class="redes">
                <li class="face"><a href="https://www.facebook.com/EpocaCosmeticos" target="_blank">facebook</a></li>
                <li class="twit"><a href="http://www.twitter.com/epocacosmeticos" target="_blank">twitter</a></li>
                <li class="yout"><a href="http://www.youtube.com/user/epocacosmeticosvideo" target="_blank">youtube</a></li>
                <li class="goog"><a href="http://plus.google.com/+epocacosmeticos" target="_blank">google plus</a></li>
                <li class="blog"><a href="http://www.papodebeleza.com.br" target="_blank">blog</a></li>
            </ul>

            <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FEpocaCosmeticos&amp;width=292&amp;height=62&amp;colorscheme=light&amp;show_faces=false&amp;header=false&amp;stream=false&amp;show_border=true&amp;appId=253803314733446" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:292px; height:62px;" allowTransparency="true"></iframe>
        </div> <!-- footer_social -->
    </div> <!-- footer_cima -->


    <div class="footer_baixo">
        <div class="selos">

            <a id="seloEbit" href="http://www.ebit.com.br/epoca-cosmeticos" target="_blank" onclick="redir(this.href);">Avalia&ccedil;&atilde;o de Lojas e-bit</a>
            <script type="text/javascript" id="getSelo" src="https://558701205.r.anankecdn.com.br/ebitBR/static/getSelo.js?5354" >
            </script> 
            
            <!-- Begin DigiCert site seal HTML and JavaScript -->
            <div class="digi-cert">
                <div id="DigiCertClickID_rDYbygEk" data-language="en_US">
                    <a href="http://www.digicert.com/unified-communications-ssl-tls.htm"></a>
                </div>
                <script type="text/javascript">
                    var __dcid = __dcid || [];__dcid.push(["DigiCertClickID_rDYbygEk", "7", "m", "black", "rDYbygEk"]);(function(){var cid=document.createElement("script");cid.async=true;cid.src="//seal.digicert.com/seals/cascade/seal.min.js";var s = document.getElementsByTagName("script");var ls =s[(s.length - 1)];ls.parentNode.insertBefore(cid, ls.nextSibling);}());
                </script>
            </div>
            <!-- End DigiCert site seal HTML and JavaScript --> 


            <img src="http://blog.epocacosmeticos.com.br/wp-content/themes/revival/img/siteblindado.gif" style="margin:0 5px" />
            <img src="http://blog.epocacosmeticos.com.br/wp-content/themes/revival/img/selo-ra1k.gif" style="margin:0 5px" />
            <img src="http://blog.epocacosmeticos.com.br/wp-content/themes/revival/img/adipec.jpg" style="margin:0 5px" />


            
        </div> <!-- selos -->

        <div class="forma_pagamento"></div>

        <div class="footer_baixo_baixo">
            <div class="logo_footer"><img src="http://www.epocacosmeticos.com.br/arquivos/logo_epoca_cosmeticos_footer.jpg" alt="" /></div> <!-- logo_footer -->
            
            <div class="endereco">
                &Eacute;poca Cosm&eacute;ticos Perfumaria | <a href="http://www.epocacosmeticos.com.br/centralatendimento">
                Central de Atendimento</a> | [21] 3202.7555 | www.epocacosmeticos.com.br
                 Raz&atilde;o Social: Campos Floridos Comercio de Cosmeticos Ltda | 
                 CNPJ: 01.239.313/0001-60 <br />Rua Francisco S&acute;, 23 / 907 - Copacabana - Rio de Janeiro
            </div> <!-- endereco -->

            <ul class="power">
                <li class="profite"><a href="http://www.profite.com.br" target="_blank">Profite</a></li>
            </ul> <!-- power -->

        </div> <!-- footer_baixo_baixo -->
    </div> <!-- footer_baixo -->
</div> <!-- cont_footer -->

<div style="display:none"><vtex.cmc:navigationHistory /></div>

<div class="scroll-top">
    <div class="scroll-button colorful">
        <a href="#top">
            <i class="i-up-open-big"></i>
        </a>
    </div>
</div>


<?php wp_footer(); ?>
</div>
<div class="footerEspecial">
    <style type="text/css">
    .footerMobile{float:left; background-color: #1C1C1C; width:100%}
    .halfMobile{float:left; background-color: #1C1C1C; width: 50%}
    p{margin:10px 0 10px 0!important}
    </style>
    <div class="footerMobile">
        <center><img src="http://blog.epocacosmeticos.com.br/wp-content/themes/revival/img/televendas.jpg" /></center>
    </div>
    <div class="halfMobile">
        <a href="http://www.epocacosmeticos.com.br/account/orders/">
            <p align="right"><img src="http://blog.epocacosmeticos.com.br/wp-content/themes/revival/img/meusPedidos.jpg" /></p>
        </a>
    </div>
    <div class="halfMobile">
       <a href="http://blog.epocacosmeticos.com.br?res=true">
            <p align="left"><img src="http://blog.epocacosmeticos.com.br/wp-content/themes/revival/img/versaoDesktop.jpg" /></p>
        </a>
    </div>
    <div class="footerMobile">
            <center><img src="http://blog.epocacosmeticos.com.br/wp-content/themes/revival/img/copyright.jpg" border="0" alt="" /></center>
    </div>
</div>


</body>
</html>