<?php $disable_featured_image = get_field('disable_featured_image'); ?>

<div class="entry-wrapper white row collapse">

    <?php get_template_part( 'inc/post', 'header'); ?>

    <?php if (! is_page()) { get_template_part( 'inc/share'); } ?>


    <div class="<?php if ( has_post_format( 'aside' ) || has_post_format( 'link' ) || ( $disable_featured_image && false == get_post_format()) ) { ?>large-4 columns<?php } elseif ( has_post_format( 'image' ) || has_post_format( 'audio' ) || has_post_format( 'video' ) || has_post_format( 'gallery' )) { ?>large-8 columns<?php } elseif ( has_post_format( 'quote' ) || is_page() || has_post_format( 'status' ) ) { ?>wide<?php } else { ?>large-5 columns<?php } ?> media-column">


        <?php if ( has_post_format( 'quote' ) || is_page() || has_post_format( 'status' ) ) { } else { ?>
            <div class="keynote">
                <?php if( $post->post_excerpt ) { the_excerpt(); } else { } ?>
            </div>

        <?php } ?>


        <?php { get_template_part('inc/formats'); } ?>


        <div class="row collapse">
            <div class="large-10 columns large-offset-2">
                <?php if ( has_post_format( 'quote' ) || has_post_format( 'status' ) || is_page() ) {} else { ?><?php get_template_part( 'inc/meta'); ?><?php } ?>
            </div>
        </div>

        <div class="divider-medium"></div>

    </div>


    <div class="<?php if ( has_post_format( 'quote' ) || is_page() || has_post_format( 'status' )) { ?>large-7 large-centered<?php } elseif ( has_post_format( 'link' ) || has_post_format( 'aside' ) || ( $disable_featured_image && false == get_post_format()) ) { ?>large-7<?php } elseif ( has_post_format( 'image' ) || has_post_format( 'audio' ) || has_post_format( 'video' ) || has_post_format( 'gallery' )) { ?>large-4<?php } else { ?>large-6<?php } ?> columns end">


        <div class="entry-content">

            <?php the_content(); ?>
            <?php get_template_part( 'inc/rating', 'criteria'); ?>


        </div>

        <footer class="single-bottom">

            <?php wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('Pages:&nbsp;&nbsp;', 'revivaltheme'), 'link_before' =>'&nbsp;&nbsp;', 'after' => '</p></nav>' )); ?>

        </footer>

    </div>


</div><!--enrty-wrapper end-->