<?php

// Main
include_once('lib/enqueue.php');
include_once('lib/foundation.php');
include_once ('lib/utility.php');
include_once('lib/widget-areas.php');
include_once('vendor/tgm/install-plugins.php');
include_once ('vendor/Mobile_Detect.php');

// Custom Fields
define( 'ACF_LITE' , true );
include_once('vendor/acf/acf.php' );
include_once('vendor/acf/add-ons/acf-repeater/repeater.php');
include_once('vendor/acf/add-ons/acf-gallery/gallery.php');
include_once('vendor/acf/add-ons/acf-icons/acf-icons.php');
include_once( 'lib/acf-options.php' );

// Link to widgets
include_once ('lib/widgets/widget-sidebar-tabs.php');
include_once ('lib/widgets/widget-footer-logo.php');
include_once ('lib/widgets/widget-sidebar-qr.php');
include_once ('lib/widgets/widget-sidebar-news-recent.php');
include_once ('lib/widgets/widget-sidebar-featured-excerpt.php');
include_once ('lib/widgets/widget-sidebar-featured.php');
include_once ('lib/widgets/widget-sidebar-video.php');

add_action('save_post', 'auto_rate_post');

function auto_rate_post($post_id)
{
	if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
	{
		return $post_id;
	}

	if(!isset($_POST['post_type']) || !current_user_can('edit_post', $post_id))
	{
		return $post_id;
	}

	$stars = 4;

	$total_stars = is_numeric(get_option('kksr_stars')) ? get_option('kksr_stars') : 5;

	if(!get_post_meta($post_id, '_kksr_ratings', true))
	{
		$ratings = $stars / ($total_stars/5);
		$avg = $ratings ? number_format((float)$ratings, 2, '.', '') : 0;

		update_post_meta($post_id, '_kksr_ratings', $ratings);
		update_post_meta($post_id, '_kksr_casts', 1);
		update_post_meta($post_id, '_kksr_avg', $avg);
	}

	return $post_id;
}
add_filter('the_excerpt', 'excerpt_read_more_link');

//Redirect to Theme Installer After Activation
if (is_admin() && isset($_GET['activated'] ) && $pagenow == "themes.php" ) {
    //Call action that sets
    add_action('admin_head','flytheme_option_setup');
    //Do redirect
    header( 'Location: '.admin_url().'customize.php' ) ;
}

// Add the Customize page to the WordPress admin area
function revivaltheme_customizer_menu() {
    add_theme_page( __( 'Customize', 'revivaltheme' ), __( 'Customize', 'revivaltheme' ), 'edit_theme_options', 'customize.php' );
}
include_once('lib/options.php'); // do all the cleaning and enqueue here

// Add theme supports
function revivaltheme_theme_support() {

	// language supports
	load_theme_textdomain('revivaltheme', get_template_directory() . '/lang');
    // rss
    add_theme_support('automatic-feed-links');
    //add_theme_support( 'custom-background' );
    // thumbnails
    add_theme_support( 'post-thumbnails' );

        // sizes
        add_image_size( 'small_full', 440, 0 );
        add_image_size( 'medium_full', 710, 0 );
        add_image_size( 'square', 100, 100, true );
        add_image_size( 'small', 270, 180, true );
        add_image_size( 'small_crop', 440, 245, true );
        add_image_size( 'medium_crop', 710, 395, true );
        add_image_size( 'large_crop', 1170, 650, true );
        add_image_size( 'large_full', 1170, 0 );

	// post formats support
	add_theme_support('post-formats', array('gallery', 'image', 'video', 'aside', 'link',  'quote', 'audio', 'status'));

	// menus support
	add_theme_support('menus');
	register_nav_menus(array(
		'primary' => __('Primary Navigation', 'revivaltheme'),
		'mobile' => __('Mobile Navigation', 'revivaltheme'),
		'footer' => __('Footer Navigation', 'revivaltheme')
	));

}
add_action('after_setup_theme', 'revivaltheme_theme_support'); /* end theme support */

if ( ! isset( $content_width ) ) $content_width = 1240;